// user.entity
import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, OneToMany, BaseEntity} from 'typeorm';
import {BidEntity} from "../../bid/entity/bid.entity";
import {IncomeEntity} from "../../income/enity/income.entity";
import {PurchasesEntity} from "../../purchases/entity/purchases.entity";
import { NotificationEntity } from "../../notification/entity/notification.entity";

@Entity({name:"users"})
export class UserEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    username: string;

    @Column()
    password: string;

    @Column()
    first_name: string;

    @Column()
    last_name: string

    @Column()
    email: string

    @Column({default:"client"})
    role: string

    @Column({default: true})
    active: boolean;

    @Column({type:"float", default:0})
    balance: number;

    @OneToMany(() => BidEntity, (bid) => bid.user)
    bids: BidEntity[]

    @OneToMany(()=>PurchasesEntity,(purchases) => purchases.user)
    purchases: PurchasesEntity[]

    @OneToMany(()=>NotificationEntity,(notification) => notification.user)
    notification: NotificationEntity[]

}