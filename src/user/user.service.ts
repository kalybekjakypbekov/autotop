import {ConflictException, Injectable} from '@nestjs/common';
import {CreateUserDto} from "./dto/create-user.dto";
import {UserEntity} from "./entity/user.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import * as bcrypt from 'bcrypt';

import {UserWithoutPassword} from "./dto/user-without-password.dto";
@Injectable()
export class UserService {

    constructor(@InjectRepository(UserEntity) private readonly usersEntity:Repository<UserEntity>,) {
    }
    async createUser(createUserDto: CreateUserDto): Promise<UserWithoutPassword>{

        const existingUsername = await this.usersEntity.findOne({
            where: { username: createUserDto.username },
        });

        if (existingUsername) {
            throw new ConflictException('Username is already taken.');
        }

        const existingEmail = await this.usersEntity.findOne({
            where: { email: createUserDto.email },
        });

        if (existingEmail) {
            throw new ConflictException('Email is already registered.');
        }

        const saltRounds = 10;
        const hashedPassword = await bcrypt.hash(createUserDto.password, saltRounds);

        const newUser = this.usersEntity.create(createUserDto);
        newUser.password = hashedPassword;
        const user = await this.usersEntity.save(newUser);

        const { password, ...result } = user;

        return result;
    }

    async findOne(username: string ): Promise<UserEntity | undefined> {
        const user  = await this.usersEntity.createQueryBuilder('user')
          .where('user.username = :username', { username: username })
          .orWhere('user.email = :email', { email: username })
          .getOne();
        return user
    }


}
