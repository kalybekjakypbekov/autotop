"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.VehicleModule = void 0;
var common_1 = require("@nestjs/common");
var vehicle_controller_1 = require("./vehicle.controller");
var vehicle_service_1 = require("./vehicle.service");
var typeorm_1 = require("@nestjs/typeorm");
var vehicle_entity_1 = require("./entity/vehicle.entity");
var VehicleModule = /** @class */ (function () {
    function VehicleModule() {
    }
    VehicleModule = __decorate([
        (0, common_1.Module)({
            imports: [typeorm_1.TypeOrmModule.forFeature([vehicle_entity_1.VehicleEntity])],
            controllers: [vehicle_controller_1.VehicleController],
            providers: [vehicle_service_1.VehicleService],
            exports: [vehicle_service_1.VehicleService]
        })
    ], VehicleModule);
    return VehicleModule;
}());
exports.VehicleModule = VehicleModule;
