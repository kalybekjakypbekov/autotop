import {
    AfterInsert, AfterUpdate,
    BaseEntity, BeforeInsert, BeforeUpdate,
    Column, CreateDateColumn,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn, UpdateDateColumn
} from "typeorm";
import {LotEntity} from "../../auction/entity/lot.entity";
import {MediaEntity} from "../../media/entity/media.entity";
import {ModelEntity} from "../../car/entity/model.entity";
import {MakeEntity} from "../../car/entity/make.entity";
import {PurchasesEntity} from "../../purchases/entity/purchases.entity";
import { IncomeEntity } from "../../income/enity/income.entity";
import { OrderEntity } from "../../order/entity/order.entity";
import { Expose } from "class-transformer";

enum FuelType {
    Solid_fuels="Solid_fuels",
    Gaseous_fuels="Gaseous fuels"
}


enum VehicleType {
    Sedan = "Sedan",
    Hatchback = "Hatchback",
    SUV = "SUV",
    Crossover = "Crossover",
    Coupe = "Coupe",
    Convertible = "Convertible",
    PickupTruck = "PickupTruck",
    Van = "Van",
    Minivan = "Minivan",
    Motorcycle = "Motorcycle",
    Truck = "Truck",
    Bus = "Bus",
}

@Entity({name:"vehicles"})
export class VehicleEntity  extends BaseEntity{

    @PrimaryGeneratedColumn()
    @Index()
    id: number

    @Column({nullable:true})
    @Index()
    aj_id: string

    @Column({nullable:true})
    stock: number

    @Column({type:"date"})
    year: Date;

    @Column({nullable:true})
    chassis: string;

    @Column()
    grade: string;

    @Column()
    color: string;

    @Column()
    mileage: number;

    @Column({default:""})
    fuel_type: string

    @Column()
    displace: number;

    @Column()
    trans: string;

    @Column()
    equipment : string;

    @Column()
    condition_ext: string;

    @Column()
    condition_int: string;

    @Column()
    modelId: number

    @Column({nullable:true})
    ref_number: string

    @Column({default:0})
    price: number

    @Column({default:0})
    buy_price: number


    @Column({default:0})
    sales_tax: number


    @Column({default:0})
    auc_fee: number


    @Column({default:0})
    tax_auc: number


    @Column({default:0})
    recycle: number


    @Column({default:0})
    rikusou: number


    @Column({default:0})
    auction_service_extra_fee: number


    @Column({default:0})
    shaken_fee: number


    @Column({default:0})
    road_tax_fee: number

    @Column({ default: 0 })
    total_cost: number; // Теперь это сохраняемое поле

    @Column({default: ""})
    category: string;

    @Column({default: ""})
    rate: string;

    @Column({default: ""})
    stock_source: string;

    @Column({default:""})
    auc_grade: string;

    @Column({default:0})
    other_cost: number

    @Column({default:""})
    memo: string;

    @Column({default:""})
    other: string;


    @BeforeInsert()
    @BeforeUpdate()
    updateTotalCost() {
        this.total_cost =
          this.buy_price +
          this.sales_tax +
          this.auc_fee +
          this.tax_auc +
          this.recycle +
          this.rikusou +
          this.auction_service_extra_fee +
          this.shaken_fee +
          this.road_tax_fee;
    }

    @Column()
    makeId: number

    @OneToOne(()=> PurchasesEntity, purchases=> purchases.vehicle)
    purchases: PurchasesEntity

    @OneToOne(() => LotEntity, (auction) => auction.vehicle)
    lot: LotEntity;

    @ManyToOne(()=>ModelEntity, (model) => model.vehicle)
    model: ModelEntity

    @ManyToOne(()=>MakeEntity, (make) => make.vehicle)
    make: MakeEntity

    @OneToMany(() => MediaEntity, (media) => media.vehicle)
    media: MediaEntity[]

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    created_at: Date;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', onUpdate: 'CURRENT_TIMESTAMP' })
    updated_at: Date;

    @OneToOne(()=>OrderEntity, (order) => order.product)
    order: OrderEntity;
}