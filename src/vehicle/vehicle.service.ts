import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {VehicleEntity} from "./entity/vehicle.entity";
import {MoreThanOrEqual, Repository} from "typeorm";
import { CreateVehicleDto, IMedia } from "./dto/create-vehicle.dto";
import { IPaginationOptions, paginate, Pagination } from "nestjs-typeorm-paginate";
import { FilterVehicleDto } from "./dto/filter-vehicle.dto";
import { query } from "express";
import { MediaService } from "../media/media.service";
import { pick } from 'lodash';

@Injectable()
export class VehicleService {

    constructor(@InjectRepository(VehicleEntity) private  readonly  vehicleEntity: Repository<VehicleEntity>,
                private readonly mediaService: MediaService) {
    }

    async  listVehiclesAdmin (){
        return await this.vehicleEntity.find({relations:["model","model.make"]})
    }

    async  pagination (options: IPaginationOptions, filter: FilterVehicleDto): Promise<Pagination<VehicleEntity>>{

        const queryBuilder = this.vehicleEntity.createQueryBuilder('vehicle');

        // if(){
        //     queryBuilder.innerJoinAndSelect("vehicle.lot", 'lot')
        // }


        queryBuilder.innerJoinAndSelect("vehicle.model", 'model')
        queryBuilder.innerJoinAndSelect("model.make", 'make');



        if(filter.id !== undefined){
            queryBuilder.andWhere('vehicle.id = :id', {id: filter.id})
        }

        if(filter.chassis){
            queryBuilder.andWhere('vehicle.chassis LIKE :chassis', {chassis: filter.chassis})
        }

        if(filter.color){
            queryBuilder.andWhere('vehicle.color = :color', {color: filter.color})
        }

        if(filter.makeId){
            queryBuilder.andWhere('vehicle.makeId = :makeId', {makeId: filter.makeId})
        }

        if(filter.stock){
            queryBuilder.andWhere('vehicle.stock = :stock', {stock: filter.stock})
        }

        if(filter.year){
            queryBuilder.andWhere('vehicle.year = :year', {stock: filter.year})
        }

        if(filter.displace){
            queryBuilder.andWhere('vehicle.displace = :displace', {displace: filter.displace})
        }

        if(filter.mileage){
            queryBuilder.andWhere('vehicle.mileage = :mileage', {mileage: filter.mileage})
        }

        if(filter.created_at){
            queryBuilder.andWhere('vehicle.created_at = :created_at', {created_at: filter.created_at})
        }

        if(filter.trans){
            queryBuilder.andWhere('vehicle.trans = :trans', {trans: filter.trans})
        }

        if (filter.orderBy) {
            const orderBy = filter.desc === "true" ? "DESC" : "ASC";
            queryBuilder.orderBy(`vehicle.${filter.orderBy}`, orderBy);
        }
        return paginate<VehicleEntity>(queryBuilder, options);
    }



    async getById(id: number) {
        const result  = await this.vehicleEntity.findOne({where:{id: id}, relations:['lot', 'model' , 'model.make', 'media'] })
        return result
    }


    async getByAjId(ajId: string){
        return await this.vehicleEntity.findOne({where:{aj_id:ajId}, cache: true})
    }

    async save(vehicle: CreateVehicleDto) {
        // Проверка, существует ли запись с указанным aj_id
        const existingVehicle = await this.vehicleEntity.findOne({ where: { aj_id: vehicle.aj_id } });

        if (existingVehicle) {
            // Если объект существует, обновляем его
            await this.vehicleEntity.update(existingVehicle.id, vehicle);
            return await this.vehicleEntity.findOne({ where: { id: existingVehicle.id } }); // Возвращаем обновленный объект
        } else {
            // Если объект не существует, создаем новый
            return await this.vehicleEntity.save(vehicle);
        }
    }

    async saveAdmin (vehicle: CreateVehicleDto){
        const vehicleCreate  =  await this.vehicleEntity.save(vehicle)
        const media: IMedia[] = vehicle.media
        for(let photo of media ){
            await this.mediaService.createImageVehicle(vehicleCreate.id,photo.url, photo.type, photo.id)
        }
        const findById  = await this.vehicleEntity.findOne({where: {id: vehicleCreate.id }})
        return findById
    }

    async update (id: number, newData: CreateVehicleDto){

        let vehicle = await this.vehicleEntity.findOne({where:{id: id}})
        const {model,lot,media, featuredImageId, ...updateDate } =  newData
        Object.assign(vehicle, updateDate);

        // vehicle.modelId = newData.modelId
        // vehicle.makeId = newData.makeId

        const photos: IMedia[] = newData.media
        for(let photo of photos ){
            await this.mediaService.createImageVehicle(vehicle.id,photo.url, photo.type, photo.id)
        }

        return await this.vehicleEntity.save(vehicle)
    }


    async updateVehicle(ajId: string, newData: CreateVehicleDto) {
        // Найдите существующую запись по идентификатору
        const vehicle = await this.vehicleEntity.findOne({where: {aj_id:ajId}});

        // Если запись не найдена, верните ошибку или обработайте по вашему усмотрению
        if (!vehicle) {
            throw new Error(`Vehicle with id ${ajId} not found.`);
        }

        // Обновите свойства записи новыми данными
        Object.assign(vehicle, newData);

        // Сохраните обновленную запись
        return await this.vehicleEntity.save(vehicle);
    }


}
