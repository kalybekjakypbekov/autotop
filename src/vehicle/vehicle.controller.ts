import {
    Body,
    Controller,
    Get,
    Param,
    Post,
    Put,
    Query,
    Res,
    UploadedFiles,
    UseGuards,
    UseInterceptors
} from "@nestjs/common";
import {VehicleService} from "./vehicle.service";
import {CreateVehicleDto} from "./dto/create-vehicle.dto";
import { FilterVehicleDto } from "./dto/filter-vehicle.dto";
import { JwtAuthGuard } from "../auth/guard/jwt-auth.guard";


@Controller('vehicle')
@UseGuards(JwtAuthGuard)
export class VehicleController {

    constructor(private  readonly  vehicleService:VehicleService) {
    }

    @Get('/list')
    async listVehicle(@Query('page') page: number = 1, @Query('limit') limit: number = 10, @Query() params: FilterVehicleDto){
        return this.vehicleService.pagination({page,limit},params)
    }


    @Get('/:id')
    async getById(@Param("id") id: number){
        const vehicle = await this.vehicleService.getById(id)
        return  vehicle
    }

    @Post('/')
    async createVehicles (@Body() createVehicleDto:CreateVehicleDto){
       return  this.vehicleService.saveAdmin(createVehicleDto)
    }



    @Put('/:id')
    async updateById(@Param("id") id: number, @Body() vehicle){
        return await this.vehicleService.update(id, vehicle)
    }

}
