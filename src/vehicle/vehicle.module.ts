import { Module } from '@nestjs/common';
import { VehicleController } from './vehicle.controller';
import { VehicleService } from './vehicle.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {VehicleEntity} from "./entity/vehicle.entity";
import { MediaModule } from "../media/media.module";

@Module({
  imports: [TypeOrmModule.forFeature([VehicleEntity]), MediaModule],
  controllers: [VehicleController],
  providers: [VehicleService],
  exports: [VehicleService]
})
export class VehicleModule {}
