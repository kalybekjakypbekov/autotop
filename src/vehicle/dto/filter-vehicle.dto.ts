import { IsOptional, IsString } from "class-validator";


export class FilterVehicleDto {

  @IsOptional()
  id?: number;

  @IsOptional()
  aj_id?: string;

  @IsOptional()
  stock?: null | string;

  @IsOptional()
  year?: string;

  @IsOptional()
  chassis?: string;

  @IsOptional()
  grade?: string;

  @IsOptional()
  color?: string;

  @IsOptional()
  mileage?: number;

  @IsOptional()
  fuel_type?: string;

  @IsOptional()
  displace?: number;

  @IsOptional()
  trans?: string;

  @IsOptional()
  equipment?: string;

  @IsOptional()
  condition_ext?: string;

  @IsOptional()
  condition_int?: string;

  @IsOptional()
  modelId?: number;

  @IsOptional()
  ref_number?: null | string;

  @IsOptional()
  makeId?: number;

  @IsOptional()
  created_at?: Date;

  @IsOptional()
  updated_at?: Date;

  @IsString()
  @IsOptional()
  orderBy: string;

  @IsString()
  @IsOptional()
  desc: string;

}