"use strict";
exports.__esModule = true;
exports.CreateVehicleDto = void 0;
var FuelType;
(function (FuelType) {
    FuelType["Solid_fuels"] = "Solid_fuels";
    FuelType["Gaseous_fuels"] = "Gaseous fuels";
})(FuelType || (FuelType = {}));
var CreateVehicleDto = /** @class */ (function () {
    function CreateVehicleDto() {
    }
    return CreateVehicleDto;
}());
exports.CreateVehicleDto = CreateVehicleDto;
