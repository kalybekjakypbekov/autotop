import { IsDate, IsDateString, IsNotEmpty } from "class-validator";

// enum FuelType {
//     Solid_fuels="Solid_fuels",
//     Gaseous_fuels="Gaseous fuels"
// }

export  class CreateVehicleDto {

    stock?: number

    aj_id?: string

    @IsNotEmpty()
    @IsDateString()
    year: Date;

    @IsNotEmpty()
    chassis: string;

    @IsNotEmpty()
    grade: string;

    @IsNotEmpty()
    color: string;

    @IsNotEmpty()
    mileage: number;

    fuel_type?: string

    @IsNotEmpty()
    displace: number;

    @IsNotEmpty()
    trans: string;

    @IsNotEmpty()
    equipment : string;

    @IsNotEmpty()
    condition_ext: string;

    @IsNotEmpty()
    condition_int: string;

    @IsNotEmpty()
    media?: []

    makeId?: number;

    modelId?: number;

    featuredImageId? : string

    lot? : any

    model?: any

}


export interface  IMedia {
    id: string;

    url: string;

    type: string;
}
