import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {LotMediaEntity} from "./entity/lot.media.entity";
import {Repository} from "typeorm";
import { Express } from "express";
import { v4 as uuidv4 } from 'uuid';
import { MediaEntity } from "./entity/media.entity";


@Injectable()
export class MediaService {


    constructor(@InjectRepository(LotMediaEntity) private readonly lotMediaEntity: Repository<LotMediaEntity>,
                @InjectRepository(MediaEntity) private readonly mediaEntity: Repository<MediaEntity>) {
    }


    async createImage (lot_id, image){
        return await this.lotMediaEntity.save({lot_id: lot_id, url: image })
    }


    async createImageVehicle(vehicle_id, image, type, id) {
        if (id !== 'new')
            return
        return await this.mediaEntity.save({vehicleId: vehicle_id, url: image, type : type})
    }


    async  fileUrls (files: Express.Multer.File[]){

        let resultFiles = files.map(image => {
            return {
                id: uuidv4(),
                url: image.path.split('public')[1],
                type : image.mimetype
            }
        } )

        return resultFiles;
    }


}
