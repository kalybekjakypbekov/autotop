import { Controller, Post, UploadedFiles, UseInterceptors } from "@nestjs/common";
import { AnyFilesInterceptor } from "@nestjs/platform-express";
import { diskStorage } from "multer";
import * as path from "path";
import * as fs from "fs";
import { fileFilter } from "../utils/file-filter.util";
import { MediaService } from "./media.service";

@Controller('media')
export class MediaController {

    constructor(private  readonly mediaService: MediaService) {
    }

    @Post('/')
    @UseInterceptors(AnyFilesInterceptor({
      storage: diskStorage({
        destination: (req, file, callback) => {
          const now = new Date();
          const year = now.getFullYear();
          const month = (now.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-indexed
          const day = now.getDate().toString().padStart(2, '0');
          const uploadPath = path.join(__dirname, '..', '..', 'public', 'uploads', year.toString(), month, day);

          // Ensure the directory exists
          fs.mkdirSync(uploadPath, { recursive: true });

          callback(null, uploadPath);
        }, // Путь для сохранения файлов
        filename: (req, file, callback) => {
          const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
          const fileExtName = file.originalname.split('.').pop();
          callback(null, `${file.fieldname}-${uniqueSuffix}.${fileExtName}`);
        },
      }),
      fileFilter: fileFilter,
    }))
    async upload(@UploadedFiles() files){
        return this.mediaService.fileUrls(files)
    }



}
