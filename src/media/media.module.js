"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MediaModule = void 0;
var common_1 = require("@nestjs/common");
var media_service_1 = require("./media.service");
var media_controller_1 = require("./media.controller");
var typeorm_1 = require("@nestjs/typeorm");
var media_entity_1 = require("./entity/media.entity");
var lot_media_entity_1 = require("./entity/lot.media.entity");
var MediaModule = /** @class */ (function () {
    function MediaModule() {
    }
    MediaModule = __decorate([
        (0, common_1.Module)({
            imports: [typeorm_1.TypeOrmModule.forFeature([media_entity_1.MediaEntity, lot_media_entity_1.LotMediaEntity])],
            providers: [media_service_1.MediaService],
            controllers: [media_controller_1.MediaController],
            exports: [media_service_1.MediaService]
        })
    ], MediaModule);
    return MediaModule;
}());
exports.MediaModule = MediaModule;
