import {BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {LotEntity} from "../../auction/entity/lot.entity";


@Entity({name: "lot_media"})
export  class LotMediaEntity{

    @PrimaryGeneratedColumn()
    @Index()
    id: number

    @Column()
    url: string

    @Column()
    lot_id: number;
}