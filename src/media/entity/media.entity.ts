import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { VehicleEntity } from "../../vehicle/entity/vehicle.entity";

@Entity({ name: "medias" })
export class MediaEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 255 })  // Явно указываем тип данных для поля URL
    url: string;

    @Column({ type: 'varchar', length: 50 })  // Явно указываем тип данных для поля type
    type: string;

    @Column("int", { name: "vehicleId" })  // Поле vehicleId для связи с VehicleEntity
    vehicleId: number;

    @JoinColumn([{ name: "vehicleId", referencedColumnName: "id" }])
    @ManyToOne(() => VehicleEntity, (vehicle) => vehicle.media)
    vehicle: VehicleEntity;
}
