import { Module } from '@nestjs/common';
import { MediaService } from './media.service';
import { MediaController } from './media.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {MediaEntity} from "./entity/media.entity";
import {LotMediaEntity} from "./entity/lot.media.entity";

@Module({
  imports:[TypeOrmModule.forFeature([MediaEntity,LotMediaEntity])],
  providers: [MediaService],
  controllers: [MediaController],
  exports: [MediaService]
})
export class MediaModule {}
