import { IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateIncomeDto {
  @IsNotEmpty()
  amount: number

  @IsNotEmpty()
  payment_method: string

  @IsNotEmpty()
  vehicle_id: number

  @IsNotEmpty()
  date: Date
}

export class  IncomeDto extends CreateIncomeDto {
  @IsNotEmpty()
  id: number
}

export class FilterIncomeDto {

  @IsOptional()
  id?: number;

  @IsOptional()
  amount?: number;

  @IsOptional()
  payment_method?: string;

  @IsOptional()
  vehicle_id?: number;

  @IsOptional()
  date?: Date;

  @IsString()
  @IsOptional()
  orderBy: string;

  @IsString()
  @IsOptional()
  desc: string;

}

