import { Body, Controller, Get, Param, Post, Put, Query, UseGuards } from "@nestjs/common";
import { IncomeService } from "./income.service";
import { FilterIncomeDto } from "./dto/income.dto";
import { JwtAuthGuard } from "../auth/guard/jwt-auth.guard";


@Controller('income')
@UseGuards(JwtAuthGuard)
export  class IncomeController {
  constructor(private readonly incomeService: IncomeService) {
  }


  @Get('/list')
  async getIncomes(@Query('page') page: number = 1, @Query('limit') limit: number = 100, @Query() params: FilterIncomeDto){
        return this.incomeService.pagination({page,limit},params)
  }

  @Get('/:id')
  async getById(@Param("id") id){
      return await this.incomeService.getById(id)
  }

  @Post('/')
  async create (@Body() income){
      const { id, ...incomeMutation} = income
      return await this.incomeService.createIncome(incomeMutation)
  }

  @Put('/')
  async update (@Body() incomeDto) {
    return  await this.incomeService.updateIncome(incomeDto)
  }



}