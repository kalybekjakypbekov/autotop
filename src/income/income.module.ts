import { Module } from '@nestjs/common';
import { IncomeService } from './income.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {IncomeEntity} from "./enity/income.entity";
import {UserModule} from "../user/user.module";
import { IncomeController } from "./income.controller";
import { CustomerModule } from "../customer/customer.module";

@Module({
  imports:[TypeOrmModule.forFeature([IncomeEntity]),UserModule,CustomerModule],
  providers: [IncomeService],
  controllers:[IncomeController]
})

export class IncomeModule {}
