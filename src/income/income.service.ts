import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { IncomeEntity } from "./enity/income.entity";
import { Repository } from "typeorm";
import { IPaginationOptions, paginate } from "nestjs-typeorm-paginate";
import { FilterIncomeDto } from "./dto/income.dto";
import { CustomerService } from "../customer/customer.service";

@Injectable()
export class IncomeService {

  constructor(
    @InjectRepository(IncomeEntity) private readonly incomeRepository: Repository<IncomeEntity>,
    private readonly customerService:CustomerService
  ) {
  }

  async pagination(options: IPaginationOptions, filter: FilterIncomeDto){

    const queryBuilder = await this.incomeRepository.createQueryBuilder('income')
    queryBuilder.innerJoinAndSelect("income.customer", 'customer')

    if(filter.id !== undefined){
        queryBuilder.andWhere('income.id =: id', {id: filter.id})
    }

    if(filter.amount){
      queryBuilder.andWhere('income.amount =: amount', {amount: filter.amount})
    }

    if(filter.payment_method){
      queryBuilder.andWhere('income.payment_method =: payment_method', {payment_method: filter.payment_method})
    }

    if(filter.date){
      queryBuilder.andWhere('income.date =: date', {date: filter.date})
    }

    if (filter.orderBy) {
      const orderBy = filter.desc === "true" ? "DESC" : "ASC";
      queryBuilder.orderBy(`income.${filter.orderBy}`, orderBy);
    }

    return paginate<IncomeEntity>(queryBuilder, options);
  }

  async getById(id: number) {
    return await this.incomeRepository.findOne({where: { id: id}, relations:["customer"]})
  }


  async createIncome (income) {
    const createIncome =  await this.incomeRepository.save(income);
    if(createIncome){
       await this.customerService.updateBalance(income.customer_id, income.amount)
    }
    return createIncome
  }

  async updateIncome(incomeDto){
    const incomeId = incomeDto.id
    const income =  await this.incomeRepository.findOne(incomeId)
    Object.assign(income, incomeDto);
    return await this.incomeRepository.save(income);
  }



}
