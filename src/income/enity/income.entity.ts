import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import { Customer } from "../../customer/entity/customer.entity";

@Entity({ name: "income" })
export class IncomeEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'decimal', precision: 10, scale: 2 })  // Явно указываем тип decimal для amount
    amount: number;

    @Column({ type: 'date', nullable: true })  // Указываем тип для даты
    date: Date;

    @Column({ type: 'varchar', length: 50 })  // Указываем тип varchar для payment_method
    payment_method: string;

    @Column({ type: 'int' })  // Явно указываем тип int для customer_id
    customer_id: number;

    @CreateDateColumn({ type: 'timestamp' })  // Дата создания с типом timestamp
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })  // Дата обновления с типом timestamp
    updated_at: Date;

    @ManyToOne(() => Customer, (customer) => customer.incomes)
    @JoinColumn({ name: "customer_id", referencedColumnName: "id" })
    customer: Customer;
}
