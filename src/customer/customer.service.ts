import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from './entity/customer.entity';
import { CustomerDto, AddPhoneNumberDto, UpdateCustomerDto } from './dto/customer.dto';
import { CountriesEntity } from "./entity/countries.entity";
import { TagsEntity } from "./entity/tags.entity";

@Injectable()
export class CustomerService {
  constructor(
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
    @InjectRepository(CountriesEntity)
    private  readonly countriesRepository: Repository<CountriesEntity>,

    @InjectRepository(TagsEntity)
    private readonly tagsRepository: Repository<TagsEntity>
  ) {}

  async createCustomer(createCustomerDto: CustomerDto): Promise<Customer> {
    const customer = this.customerRepository.create(createCustomerDto);
    return this.customerRepository.save(customer);
  }

  async addPhoneNumber(customerId: number, addPhoneNumberDto: AddPhoneNumberDto): Promise<Customer> {
    const customer = await this.customerRepository.findOneBy({ id: customerId });
    if (!customer) {
      throw new NotFoundException('Customer not found');
    }

    if (!customer.phoneNumbers) {
      customer.phoneNumbers = [];
    }

    customer.phoneNumbers.push(addPhoneNumberDto);
    return this.customerRepository.save(customer);
  }

  async updateCustomer(customerId: number, updateCustomerDto: UpdateCustomerDto): Promise<Customer> {
    const customer = await this.customerRepository.findOneBy({ id: customerId });
    if (!customer) {
      throw new NotFoundException('Customer not found');
    }

    const updatedCustomer = this.customerRepository.merge(customer, updateCustomerDto);
    return this.customerRepository.save(updatedCustomer);
  }

  async getCustomers(): Promise<Customer[]>{
    return  this.customerRepository.find()
  }


  async getCustomer (id: number):Promise<Customer>{
      return this.customerRepository.findOne({where:{id}, relations:["incomes"]})
  }

  async getCountries(){
      return  await this.countriesRepository.find()
  }

  async getCustomersTag(){
      return await this.tagsRepository.find()
  }


  async updateBalance(customerId: number, income: number) {
    const customer = await this.customerRepository.findOne({ where: { id: customerId } });

    if (!customer) {
      throw new NotFoundException(`Customer with ID ${customerId} not found`);
    }

    customer.balance = customer.balance + income;
    return await this.customerRepository.save(customer);
  }

}
