import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToOne, OneToMany } from "typeorm";
import { OrderEntity } from "../../order/entity/order.entity";
import { IncomeEntity } from "../../income/enity/income.entity";

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  avatar: string;

  @Column({ nullable: true, default:"https://react-material.fusetheme.com/assets/images/cards/19-640x480.jpg" })
  background: string;

  @Column()
  name: string;

  @Column('json', { nullable: true })
  emails: {
    email: string;
    label: string;
  }[];

  @Column('json', { nullable: true })
  phoneNumbers: {
    country: string;
    phoneNumber: string;
    label: string;
  }[];

  @Column({ nullable: true })
  title: string;

  @Column({ nullable: true })
  company: string;

  @Column({ nullable: true })
  birthday: string;

  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true, length: 1000 })
  notes: string;

  @Column({default: 0})
  balance: number;

  @Column('simple-array', { nullable: true })
  tags: string[];

  @OneToOne(()=>OrderEntity, order=> order.customer)
  order: OrderEntity

  @OneToMany(()=>IncomeEntity, (income)=> income.customer)
  incomes: IncomeEntity[]
}
