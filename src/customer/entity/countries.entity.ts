import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class CountriesEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  iso: string;


  @Column({unique:true})
  name: string;

  @Column()
  code: string;

  @Column()
  flagImagePos: string;
}