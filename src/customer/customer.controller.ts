import { Controller, Post, Body, Param, Put, UsePipes, ValidationPipe, Get } from "@nestjs/common";
import { CustomerService } from './customer.service';
import { CustomerDto, AddPhoneNumberDto, UpdateCustomerDto } from "./dto/customer.dto";

@Controller('customers')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  async create(@Body() createCustomerDto: CustomerDto) {
    return this.customerService.createCustomer(createCustomerDto);
  }

  @Get('/countries')
  async getCountries(){
    return this.customerService.getCountries()
  }

  @Get('/tags')
  async getCustomersTag(){
      return await  this.customerService.getCustomersTag()
  }

  @Put(':id/phoneNumbers')
  @UsePipes(new ValidationPipe({ transform: true }))
  async addPhoneNumber(
    @Param('id') id: number,
    @Body() addPhoneNumberDto: AddPhoneNumberDto
  ) {
    return this.customerService.addPhoneNumber(id, addPhoneNumberDto);
  }

  @Put(':id')
  @UsePipes(new ValidationPipe({ transform: true }))
  async updateCustomer(
    @Param('id') id: number,
    @Body() updateCustomerDto: UpdateCustomerDto
  ) {
    return this.customerService.updateCustomer(id, updateCustomerDto);
  }


  @Get()
  async getCustomers(){
      return this.customerService.getCustomers()
  }


  @Get(':id')
  async getCustomerById( @Param('id') id: number,){
    return this.customerService.getCustomer(id)
  }







}
