import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import { CustomerService } from "./customer.service";
import { CustomerController } from "./customer.controller";
import { Customer } from "./entity/customer.entity";
import { CountriesEntity } from "./entity/countries.entity";
import { TagsEntity } from "./entity/tags.entity";


@Module({
  imports: [TypeOrmModule.forFeature([Customer,CountriesEntity,TagsEntity])],
  providers: [CustomerService],
  controllers:[CustomerController],
  exports: [CustomerService]
})
export class CustomerModule {}
