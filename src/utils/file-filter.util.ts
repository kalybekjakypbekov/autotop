import { Request } from 'express';

export const fileFilter = (req: Request, file: Express.Multer.File, callback: (error: Error | null, acceptFile: boolean) => void) => {
  // Пример фильтрации только изображений
  if (!file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) {
    return callback(new Error('Unsupported file type'), false);
  }
  callback(null, true);
};
