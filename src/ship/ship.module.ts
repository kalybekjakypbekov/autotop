import { Module } from '@nestjs/common';
import { ShipService } from './ship.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {ShipEntity} from "./entity/ship.entity";

@Module({
  imports:[TypeOrmModule.forFeature([ShipEntity])],
  providers: [ShipService]
})
export class ShipModule {}
