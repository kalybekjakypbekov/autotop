import {BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {PurchasesEntity} from "../../purchases/entity/purchases.entity";



@Entity({name:"ship"})
export class ShipEntity extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    ship_name:string

    @Column()
    ship_date: Date

    @Column()
    departure_port: string

    @Column()
    voyage:string

    @Column({nullable:true})
    purchase_id: number

    @OneToOne(() => PurchasesEntity, (purchase) => purchase.ship)
    @JoinColumn({name:"purchase_id",referencedColumnName:"id"})
    purchase: PurchasesEntity

}