import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity, JoinColumn,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {PurchasesEntity} from "../../purchases/entity/purchases.entity";

@Entity({name: "document"})
export  class DocumentEntity extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number

    @Column({nullable: true})
    export_cert_eng: string

    @Column({nullable: true})
    export_cert_jpn: string

    @Column({nullable: true})
    bl_no_original: string

    @Column({nullable: true})
    bl_no: string

    @Column({nullable: true})
    invoice: string

    @Column({nullable: true})
    insurance: string

    @Column({nullable: true})
    inspection: string

    @Column({nullable: true})
    JAAI: string

    @Column({nullable: true})
    courier: string

    @Column({nullable: true})
    date: Date

    @Column({nullable: true})
    tracking_no: string

    @Column({nullable: true})
    tracking_no_local: string

    @Column({nullable: true})
    purchase_id: number

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @OneToOne(()=> PurchasesEntity, purchase => purchase.document)
    @JoinColumn({name:"purchase_id", referencedColumnName:"id"})
    purchase: PurchasesEntity

}