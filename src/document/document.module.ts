import { Module } from '@nestjs/common';
import { DocumentService } from './document.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {DocumentEntity} from "./entity/document.entity";

@Module({
  imports: [TypeOrmModule.forFeature([DocumentEntity])],
  providers: [DocumentService]
})
export class DocumentModule {}
