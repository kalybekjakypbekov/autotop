import { IsOptional, IsString } from "class-validator";


export  class FilterOrderDto {

  @IsOptional()
  id?: number;

  @IsString()
  @IsOptional()
  orderBy: string;

  @IsString()
  @IsOptional()
  desc: string;
}