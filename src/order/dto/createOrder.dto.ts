

interface  Customer {
  id: number,

  name: string;
}

interface  Vehicle {
  id: number;

  name: string;

}

interface ShipAddress {
  shipAddress: string;

}


export  class CreateOrderDto {

  customer: Customer;

  vehicle: Vehicle;

  reference: string;

  discount: number;

  shipAddress: string;

  tax: number;


}