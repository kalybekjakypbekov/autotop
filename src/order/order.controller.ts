import { Body, Controller, Get, Param, Post, Put, Query, UseGuards } from "@nestjs/common";
import { OrderService } from "./order.service";
import { JwtAuthGuard } from "../auth/guard/jwt-auth.guard";
import { FilterOrderDto } from "./dto/filterOrderDto";
import { Paginate, Paginated, PaginateQuery } from "nestjs-paginate";
import { OrderEntity } from "./entity/order.entity";
import { ShipAddressEntity } from "./entity/shipAddress.entity";
import { CreateOrderDto } from "./dto/createOrder.dto";



@Controller('order')
@UseGuards(JwtAuthGuard)
export  class OrderController {


  constructor(private  readonly orderService: OrderService) {
  }

  @Get('/list')
  public async list(@Paginate() query: PaginateQuery): Promise<Paginated<OrderEntity>> {
    return this.orderService.pagination(query)
  }

  @Get('/:id')
  async getById(@Param("id") id){
      return await this.orderService.getById(id)
  }

  @Put('/:id')
  async update(@Param("id") id, @Body() body){
    return await  this.orderService.update(id, body )
  }

  @Post('/')
  async create (@Body() order: CreateOrderDto ){
    return this.orderService.create(order)
  }

}