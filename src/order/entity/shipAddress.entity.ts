import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { OrderEntity } from "./order.entity";


@Entity()
export  class ShipAddressEntity {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  address: string;

  @Column()
  lat: number;

  @Column()
  lng : number;

  @Column()
  order_id: number

  @OneToOne(()=>OrderEntity, (order)=> order.shipAddress)
  @JoinColumn({name:"order_id", referencedColumnName:"id"})
  order: OrderEntity

}