import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { OrderEntity } from "./order.entity";

@Entity()
export class RoadMapEntity {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  location: string

  @Column()
  action: boolean

  @Column({ type: "datetime"})
  date: Date

  @Column()
  order_id: number

  @ManyToOne(() => OrderEntity, order => order.roadMap )
  @JoinColumn({name:"order_id", referencedColumnName:"id"})
  order: OrderEntity

}