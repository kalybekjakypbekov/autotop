import {
  Column,
  CreateDateColumn,
  Entity, JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { RoadMapEntity } from "./roadMap.entity";
import { OrderStatusHistoryEntity } from "./orderStatusHistory.entity";
import { Customer } from "../../customer/entity/customer.entity";
import { VehicleEntity } from "../../vehicle/entity/vehicle.entity";
import { ShipAddressEntity } from "./shipAddress.entity";
import { PaymentEntity } from "../../payment/entity/payment.entity";
import { ConsigneeEntity } from "../../consignee/entity/consignee.entity";

@Entity({name:"order"})
export class OrderEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  reference: string;

  @Column({default: 0})
  subtotal: number;

  @Column()
  tax: number;

  @Column({default: 0})
  discount: number;

  @Column({default: 0})
  total: number



  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  date: Date

  @Column({nullable:true})
  product_id: number;

  @Column({nullable:true})
  customer_id: number;

  @Column({nullable:true})
  consignee_id: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(()=>RoadMapEntity, roadMap=> roadMap.order)
  roadMap: RoadMapEntity[]

  @OneToMany(()=>OrderStatusHistoryEntity, historyStatus=> historyStatus.order)
  statusHistory: OrderStatusHistoryEntity[]

  @OneToOne(()=>Customer, customer=>customer.order)
  @JoinColumn({name:"customer_id", referencedColumnName:"id"})
  customer: Customer

  @OneToOne(()=> VehicleEntity, (product)=>product.order)
  @JoinColumn({name:"product_id", referencedColumnName:"id"})
  product: VehicleEntity

  @OneToOne(()=>ShipAddressEntity, (shipAddress) => shipAddress.order)
  shipAddress: ShipAddressEntity

  @OneToMany(()=>PaymentEntity,(payment)=> payment.order)
  payment: PaymentEntity[]

  @OneToOne(()=>ConsigneeEntity, (consignee) => consignee.order)
  @JoinColumn({name:"consignee_id", referencedColumnName:"id"})
  consignee: ConsigneeEntity
}