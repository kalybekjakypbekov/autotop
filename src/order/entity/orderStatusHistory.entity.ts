import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { OrderEntity } from "./order.entity";

@Entity()
export class OrderStatusHistoryEntity {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  status: string;

  @Column()
  color: string;

  @Column()
  order_id: number

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => OrderEntity, order => order.statusHistory)
  @JoinColumn({name:"order_id", referencedColumnName:"id"})
  order: OrderEntity

}