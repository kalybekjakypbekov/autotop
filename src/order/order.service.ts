import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { OrderEntity } from "./entity/order.entity";
import { Repository } from "typeorm";
// import { IPaginationOptions, paginate, Pagination } from "nestjs-typeorm-paginate";
import { FilterOrderDto } from "./dto/filterOrderDto";
import { VehicleEntity } from "../vehicle/entity/vehicle.entity";
import { VehicleService } from "../vehicle/vehicle.service";
import { CustomerService } from "../customer/customer.service";
import { Paginated, PaginateQuery, paginate, FilterOperator, FilterSuffix } from "nestjs-paginate";
import { ShipAddressEntity } from "./entity/shipAddress.entity";
import { CreateOrderDto } from "./dto/createOrder.dto";

@Injectable()
export class OrderService {

  constructor(@InjectRepository(OrderEntity) private  readonly orderEntity: Repository<OrderEntity>,
              @InjectRepository(ShipAddressEntity) private readonly shipAddressEntity: Repository<ShipAddressEntity>,
              private readonly vehicleService: VehicleService,
              private  readonly customerService: CustomerService ) {
  }



  async pagination(query: PaginateQuery): Promise<Paginated<OrderEntity>> {

    const paginatedResult = await paginate(query, this.orderEntity, {
      relations:['customer','payment','statusHistory','product'],
      sortableColumns: ['id','reference','customer.name','total','date'],
      searchableColumns: ['id','reference','customer.name','total','date'],
      maxLimit: 20,
      defaultLimit: 20,
      filterableColumns: {
        id: true,
        reference: true,
        "customer.name": [FilterOperator.ILIKE],
        total: [FilterOperator.BTW],
        date: [FilterOperator.BTW]
      }
    })
    return paginatedResult;
  }



  async getById (id: number ) {
     const order = await this.orderEntity.findOne({where:{id: id}, relations:['roadMap','statusHistory','customer', 'product','product.media','product.model', 'product.model.make','shipAddress','payment','consignee']})
    return order;
  }

  async update(id: number, updateData: Partial<OrderEntity>) {
    const order = await this.getById(id);

    if (!order) {
      throw new NotFoundException(`Order with ID ${id} not found`);
    }

    Object.assign(order, updateData);

    if(updateData.product_id) {
      const vehicle = await this.vehicleService.getById(updateData.product_id);

      if (!vehicle) {
        throw new NotFoundException(`Vehicle with ID ${updateData.product_id} not found`);
      }

      order.product = vehicle;
    }



    if(updateData.customer_id) {
      const customer = await this.customerService.getCustomer(updateData.customer_id);

      if (!customer) {
        throw new NotFoundException(`Vehicle with ID ${updateData.product_id} not found`);
      }

      order.customer = customer;
    }

    if(updateData.shipAddress){
       await this.updateShipAddress(order.id,updateData.shipAddress.address)
    }


    // Save the updated order
    return this.orderEntity.save(order);
  }

  async  create (orderDto: CreateOrderDto){
      const orderData = {
        reference: orderDto.reference,
        tax: orderDto.tax,
        product_id: orderDto.vehicle.id,
        customer_id: orderDto.customer.id
      }
      const order = await this.orderEntity.save(orderData)

    return order

  }

  async updateShipAddress (order_id, text){

     const shipAddress = await this.shipAddressEntity.findOne({where:{order_id: order_id}})

    if(shipAddress){
      shipAddress.address = text
      return await this.shipAddressEntity.save(shipAddress)
    }

    const shipAddressNew = {
      address: text,
      lat:0,
      lng:0,
      order_id: order_id
    }

    return  await this.shipAddressEntity.save(shipAddressNew)
  }

}
