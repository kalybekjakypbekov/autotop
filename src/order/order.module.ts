import { Module } from '@nestjs/common';
import { OrderController } from "./order.controller";
import { OrderService } from "./order.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { OrderEntity } from "./entity/order.entity";
import { OrderStatusHistoryEntity } from "./entity/orderStatusHistory.entity";
import { RoadMapEntity } from "./entity/roadMap.entity";
import { VehicleModule } from "../vehicle/vehicle.module";
import { CustomerModule } from "../customer/customer.module";
import { ShipAddressEntity } from "./entity/shipAddress.entity";
@Module({
  imports:[TypeOrmModule.forFeature([OrderEntity,OrderStatusHistoryEntity,RoadMapEntity, ShipAddressEntity]),VehicleModule,CustomerModule],
  providers: [OrderService],
  controllers:[OrderController]
})

export class OrderModule {}
