import {Body, Controller, Get, Post, Query, UseGuards} from '@nestjs/common';
import {JwtAuthGuard} from "../auth/guard/jwt-auth.guard";
import { DashboardService } from "./dashboard.service";
import  { readFileSync} from "fs"
import * as path from "path";
@UseGuards(JwtAuthGuard)
@Controller('dashboards')
export class DashboardController {

  constructor(private readonly dashboardService: DashboardService) {

  }



  @Get('/analytics/widgets')
  async analyticsWidget(){
    const  mockData  = JSON.parse(readFileSync(process.cwd()+'/public/mock/mockData.json', { encoding: 'utf8', flag: 'r' }));
    return mockData;
  }




}
