import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import { DashboardController } from "./dashboard.controller";
import { DashboardService } from "./dashboard.service";


@Module({
  imports: [],
  providers: [DashboardService],
  controllers:[DashboardController],

})
export class DashboardModule {}
