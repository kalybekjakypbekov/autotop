import {BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {LotEntity} from "./lot.entity";


@Entity({name:"auctions"})
export  class AuctionsEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string


    @OneToMany(()=>LotEntity, (lot) =>lot.auction)
    lot: LotEntity[]

}