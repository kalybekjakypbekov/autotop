import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn
} from "typeorm";
import {VehicleEntity} from "../../vehicle/entity/vehicle.entity";
import {BidEntity} from "../../bid/entity/bid.entity";
import {AuctionsEntity} from "./auctions.entity";
import {LotMediaEntity} from "../../media/entity/lot.media.entity";

export enum Status {
    BUY= "BUY",
    sold = "sold",
    "not sold" = "not sold",
    "Sold By Nego" = "Sold By Nego",
    END = "END",
    "removed"= "removed",
    Cancelled = "Cancelled",
    In_Auction ="IN AUCTION",
    DEFAULT = ""
}

@Entity({name:"lots"})
export  class LotEntity extends  BaseEntity {

    @PrimaryGeneratedColumn()
    @Index()
    id: number

    @Column({nullable:true})
    @Index()
    aj_id: string

    @Column({type:"datetime"})
    start_time: Date;

    @Column({type:"datetime"})
    end_time: Date;

    @Column()
    lot_no: string;

    @Column()
    start_price: number;

    @Column({nullable:true})
    finish_price: number

    @Column()
    price_average: number

    @Column({default:""})
    average_price_list: string

    @Column()
    rate: string

    @Column()
    status: string
    //0708363668

    @Column()
    vehicleId: number

    @Column()
    auctionId: number

    @Column({type:"text"})
    auction_list: string

    @Column({ default:0})
    actual_purchase_price: number

    @Column({type:"text"})
    images: string

    @OneToOne(() => VehicleEntity, (vehicle) => vehicle.lot)
    @JoinColumn({name:"vehicleId", referencedColumnName:"id"})
    vehicle: VehicleEntity;

    @OneToMany(() => BidEntity, (bid) =>bid.lot,{cascade:true})
    bids: BidEntity[]

    @ManyToOne(()=> AuctionsEntity, (auction) => auction.lot)
    @JoinColumn({name:"auctionId", referencedColumnName:"id"})
    auction: AuctionsEntity

}