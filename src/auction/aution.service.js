"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
exports.__esModule = true;
exports.AutionService = void 0;
var common_1 = require("@nestjs/common");
var typeorm_1 = require("@nestjs/typeorm");
var lot_entity_1 = require("./entity/lot.entity");
var typeorm_2 = require("typeorm");
var auctions_entity_1 = require("./entity/auctions.entity");
var AutionService = /** @class */ (function () {
    function AutionService(lotEntity, auctionsEntity) {
        this.lotEntity = lotEntity;
        this.auctionsEntity = auctionsEntity;
    }
    AutionService.prototype.search = function (params, currentUser, purchases) {
        return __awaiter(this, void 0, void 0, function () {
            var queryBuilder, currentUserId, currentUserId, start_millage, end_millage, start_millage, end_millage, start_year, end_year, start_year, end_year, price_max, price_min, price_max, price_min, start_condition, end_condition, start_condition, end_condition, start_displace, end_displace, start_displace, end_displace, page, limit, offset, _a, vehicles, count;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.lotEntity.createQueryBuilder("lot")
                            .leftJoinAndSelect("lot.vehicle", "vehicle")
                            .leftJoinAndSelect("lot.auction", "auction")
                            .loadRelationCountAndMap("lot.bidsCount", "lot.bids")
                            .leftJoinAndSelect("vehicle.model", "model")
                            .leftJoinAndSelect("model.make", "make")
                            .leftJoin("lot.bids", "bid")];
                    case 1:
                        queryBuilder = _b.sent();
                        if (params.make) {
                            queryBuilder.where("make.name = :make", { make: params.make });
                        }
                        if (params.model) {
                            queryBuilder.andWhere("model.name =:model ", { model: params.model });
                        }
                        if (currentUser && purchases) {
                            currentUserId = currentUser.userId;
                            queryBuilder.andWhere("bid.userId = :userId", { userId: currentUserId });
                        }
                        else if (currentUser) {
                            currentUserId = currentUser.userId;
                            queryBuilder.andWhere("bid.userId = :userId", { userId: currentUserId });
                        }
                        if (params.lotNumber) {
                            console.log(params.lotNumber);
                            queryBuilder.andWhere("lot.lot_no =:lot_no", { lot_no: params.lotNumber });
                        }
                        if (params.date) {
                            queryBuilder.andWhere("DATE(lot.start_time) = :date", { date: params.date });
                        }
                        else {
                            queryBuilder.andWhere("DATE(lot.start_time) = :date", { date: new Date().toISOString().split('T')[0] });
                        }
                        if (params.auction) {
                            queryBuilder.andWhere("auction.name = :auction", { auction: params.auction });
                        }
                        if (params.start_millage && params.end_millage) {
                            start_millage = params.start_millage;
                            end_millage = params.end_millage;
                            queryBuilder.andWhere("vehicle.mileage BETWEEN  :start_millage AND :end_millage ", { start_millage: start_millage, end_millage: end_millage });
                        }
                        else if (params.start_millage) {
                            start_millage = params.start_millage;
                            queryBuilder.andWhere("vehicle.mileage >= :start_millage", { start_millage: start_millage });
                        }
                        else if (params.end_millage) {
                            end_millage = params.end_millage;
                            queryBuilder.andWhere("vehicle.mileage <= :end_millage", { end_millage: end_millage });
                        }
                        if (params.start_year && params.end_year) {
                            start_year = params.start_year;
                            end_year = params.end_year;
                            queryBuilder.andWhere("YEAR(vehicle.year) >= :start_year AND YEAR(vehicle.year) <= :end_year ", { start_year: start_year, end_year: end_year });
                        }
                        else if (params.start_year) {
                            start_year = params.start_year;
                            queryBuilder.andWhere("YEAR(vehicle.year) >= :start_year ", { start_year: start_year });
                        }
                        else if (params.end_year) {
                            end_year = params.end_year;
                            queryBuilder.andWhere("YEAR(vehicle.year) <= :end_year ", { end_year: end_year });
                        }
                        if (params.price_max && params.price_min) {
                            price_max = params.price_max;
                            price_min = params.price_min;
                            queryBuilder.andWhere("lot.start_price <= :price_max AND lot.start_price >= :price_min ", { price_max: price_max, price_min: price_min });
                        }
                        else if (params.price_max) {
                            price_max = params.price_max;
                            queryBuilder.andWhere("lot.start_price <= :price_max ", { price_max: price_max });
                        }
                        else if (params.price_min) {
                            price_min = params.price_min;
                            queryBuilder.andWhere("lot.start_price >= :price_min ", { price_min: price_min });
                        }
                        if (params.start_condition && params.end_condition) {
                            start_condition = params.start_condition;
                            end_condition = params.end_condition;
                            queryBuilder.andWhere("vehicle.condition_ext >= :start_condition AND vehicle.condition_ext <= :end_condition ", { start_condition: start_condition, end_condition: end_condition });
                        }
                        else if (params.start_condition) {
                            start_condition = params.start_condition;
                            queryBuilder.andWhere("vehicle.condition_ext >= :start_condition ", { start_condition: start_condition });
                        }
                        else if (params.end_condition) {
                            end_condition = params.end_condition;
                            queryBuilder.andWhere("vehicle.condition_ext <= :end_condition ", { end_condition: end_condition });
                        }
                        if (params.start_displace && params.end_displace) {
                            start_displace = params.start_displace;
                            end_displace = params.end_displace;
                            queryBuilder.andWhere("vehicle.displace >= :start_displace AND vehicle.displace <= :end_displace ", { start_displace: start_displace, end_displace: end_displace });
                        }
                        else if (params.start_displace) {
                            start_displace = params.start_displace;
                            queryBuilder.andWhere("vehicle.displace >= :start_displace ", { start_displace: start_displace });
                        }
                        else if (params.end_displace) {
                            end_displace = params.end_displace;
                            queryBuilder.andWhere("vehicle.displace <= :end_displace ", { end_displace: end_displace });
                        }
                        if (!params.page) {
                            queryBuilder.limit(10);
                        }
                        page = params.page || 1;
                        limit = 10;
                        offset = (page - 1) * limit;
                        queryBuilder.skip(offset).take(limit);
                        return [4 /*yield*/, queryBuilder.getManyAndCount()];
                    case 2:
                        _a = _b.sent(), vehicles = _a[0], count = _a[1];
                        return [2 /*return*/, { lot: vehicles, count: count }];
                }
            });
        });
    };
    AutionService.prototype.list = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.auctionsEntity.find()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AutionService.prototype.inAuctionsVehicles = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate, vehicles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        currentDate = new Date();
                        return [4 /*yield*/, this.lotEntity.find({ where: { status: lot_entity_1.Status.In_Auction, end_time: (0, typeorm_2.LessThan)(currentDate) }, relations: ['vehicle'] })];
                    case 1:
                        vehicles = _a.sent();
                        return [2 /*return*/, vehicles];
                }
            });
        });
    };
    AutionService.prototype.getById = function (id, currentUser) {
        return __awaiter(this, void 0, void 0, function () {
            var result, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.lotEntity.findOne({ where: { id: id }, relations: ["vehicle", "auction", "bids", "vehicle.model", "vehicle.model.make", "vehicle.media"] })];
                    case 1:
                        result = _b.sent();
                        _a = result;
                        return [4 /*yield*/, this.bids(id, currentUser)];
                    case 2:
                        _a.bids = _b.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    AutionService.prototype.getByIdAuction = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.auctionsEntity.findOne({ where: { id: id } })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AutionService.prototype.bids = function (lotId, currentUser) {
        return __awaiter(this, void 0, void 0, function () {
            var lot, currentUserId, bids, maxBid, winnerBid, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.lotEntity.findOne({ where: { id: lotId }, relations: ['bids', 'bids.user'] })];
                    case 1:
                        lot = _a.sent();
                        currentUserId = currentUser.userId;
                        console.log(currentUserId);
                        bids = Object.assign([], lot.bids);
                        if (lot.bids.length) {
                            maxBid = lot.bids.reduce(function (prev, current) {
                                return (prev.bid_price > current.bid_price) ? prev : current;
                            });
                        }
                        winnerBid = Object.assign({}, maxBid);
                        result = bids.map(function (bid) {
                            var _a = bid.user, password = _a.password, email = _a.email, active = _a.active, id = _a.id, username = _a.username, result = __rest(_a, ["password", "email", "active", "id", "username"]);
                            result.created_at = bid.created_at;
                            if (winnerBid.userId == bid.userId && winnerBid.bid_price == bid.bid_price) {
                                result.winner = true;
                            }
                            else {
                                result.winner = false;
                            }
                            result.bid_price = bid.bid_price;
                            if (currentUserId != bid.userId) {
                                result.first_name = result.first_name.substring(0, 3) + "*****";
                                result.last_name = result.last_name.substring(0, 3) + "*****";
                            }
                            return result;
                        });
                        result.sort(function (a, b) {
                            return b.winner - a.winner;
                        });
                        return [2 /*return*/, result];
                }
            });
        });
    };
    AutionService.prototype.createAuction = function (auctionName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.auctionsEntity.save({ name: auctionName })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AutionService.prototype.findAuction = function (auctionName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.auctionsEntity.findOne({ where: { name: auctionName } })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AutionService.prototype.createLot = function (lot) {
        return __awaiter(this, void 0, void 0, function () {
            var lot_data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.lotEntity.save(lot)];
                    case 1:
                        lot_data = _a.sent();
                        return [2 /*return*/, lot_data];
                }
            });
        });
    };
    AutionService.prototype.findLotAjId = function (ajId, vehicleid, auct_date) {
        return __awaiter(this, void 0, void 0, function () {
            var lot;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.lotEntity
                            .createQueryBuilder('lot')
                            .where('lot.aj_id = :ajId', { ajId: ajId })
                            .orWhere('lot.vehicleId = :vehicleid', { vehicleid: vehicleid })
                            .andWhere('lot.start_time <= :auct_date', { auct_date: auct_date })
                            .getOne()];
                    case 1:
                        lot = _a.sent();
                        return [2 /*return*/, lot];
                }
            });
        });
    };
    AutionService.prototype.updateLot = function (ajId, newData) {
        return __awaiter(this, void 0, void 0, function () {
            var lot;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.lotEntity.findOne({ where: { aj_id: ajId } })];
                    case 1:
                        lot = _a.sent();
                        Object.assign(lot, newData);
                        return [4 /*yield*/, this.lotEntity.save(lot)];
                    case 2: 
                    // Сохраните обновленную запись
                    return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AutionService = __decorate([
        (0, common_1.Injectable)(),
        __param(0, (0, typeorm_1.InjectRepository)(lot_entity_1.LotEntity)),
        __param(1, (0, typeorm_1.InjectRepository)(auctions_entity_1.AuctionsEntity))
    ], AutionService);
    return AutionService;
}());
exports.AutionService = AutionService;
