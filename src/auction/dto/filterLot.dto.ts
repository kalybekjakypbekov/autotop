import { IsString, IsNumber, IsDateString, IsUrl, IsOptional } from 'class-validator';

export class FilterLotDto {
  @IsNumber()
  @IsOptional()
  id?: number;

  @IsString()
  @IsOptional()
  aj_id?: string;

  @IsDateString()
  @IsOptional()
  start_time?: string;

  @IsDateString()
  @IsOptional()
  end_time?: string;

  @IsString()
  @IsOptional()
  lot_no?: string;

  @IsNumber()
  @IsOptional()
  start_price?: number;

  @IsNumber()
  @IsOptional()
  finish_price?: number;

  @IsNumber()
  @IsOptional()
  price_average?: number;

  @IsString()
  @IsOptional()
  average_price_list?: string;

  @IsString()
  @IsOptional()
  rate?: string;

  @IsOptional()
  @IsString()
  status?: string;

  @IsNumber()
  @IsOptional()
  vehicleId?: number;

  @IsNumber()
  @IsOptional()
  auctionId?: number;

  @IsUrl()
  @IsOptional()
  auction_list?: string;

  @IsNumber()
  @IsOptional()
  actual_purchase_price?: number;

  @IsString()
  @IsOptional()
  images?: string;

  @IsString()
  @IsOptional()
  orderBy: string;

  @IsString()
  @IsOptional()
  desc: string;
}
