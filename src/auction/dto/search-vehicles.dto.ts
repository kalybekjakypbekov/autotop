
export class SearchVehiclesDto {

    make: string;

    model: string;

    start_year: string;

    end_year: string;

    lot_no: string;

    lotNumber: string;

    price_min: string;

    price_max: string;

    finish_price_min: string;

    finish_price_max: string;

    start_millage: string;

    end_millage: string;

    auction: string;

    date: string;

    condition: string;
    start_displace : number

    end_displace : number

    page: number

    limit: number;

    color: string;


}