import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {LotEntity, Status} from "./entity/lot.entity";
import {LessThan, MoreThanOrEqual, Repository} from "typeorm";
import {SearchVehiclesDto} from "./dto/search-vehicles.dto";
import {AuctionsEntity} from "./entity/auctions.entity";
import {CreateVehicleDto} from "../vehicle/dto/create-vehicle.dto";
import {paginate ,IPaginationOptions, Pagination } from "nestjs-typeorm-paginate";
import { FilterLotDto } from "./dto/filterLot.dto";
import { UserService } from "../user/user.service";
import axios from 'axios';
import { CreateLotDto } from "./dto/create-lot.dto";
import { arrayMaxSize } from "class-validator";
const FormData = require('form-data');
const dayjs = require('dayjs');
const cheerio = require('cheerio');
const moment = require('moment-timezone');

@Injectable()
export class AutionService {

    constructor(
        @InjectRepository(LotEntity) private  readonly lotEntity: Repository<LotEntity>,
        @InjectRepository(AuctionsEntity) private readonly auctionsEntity: Repository<AuctionsEntity>,
        private  readonly  userService: UserService,


    ) {
    }

    async search (params:SearchVehiclesDto, currentUser?, purchases?){


        const queryBuilder = await this.lotEntity.createQueryBuilder("lot")
            .leftJoinAndSelect("lot.vehicle", "vehicle")
            .leftJoinAndSelect("lot.auction", "auction")
            .loadRelationCountAndMap("lot.bidsCount", "lot.bids")
            .leftJoinAndSelect("vehicle.model", "model")
            .leftJoinAndSelect("model.make", "make")
            .leftJoin("lot.bids", "bid")


        if(params.make){
            queryBuilder.where("make.name = :make", { make: params.make })
        }

        if(params.model){
            queryBuilder.andWhere("model.name =:model ",{model: params.model})
        }


        if(currentUser && purchases) {
            const currentUserId = currentUser.userId
            queryBuilder.andWhere("bid.userId = :userId", { userId: currentUserId })
        } else if (currentUser) {
            const currentUserId = currentUser.userId
            queryBuilder.andWhere("bid.userId = :userId", { userId: currentUserId })
        }


        if(params.lotNumber){
            queryBuilder.andWhere("lot.lot_no =:lot_no",{lot_no : params.lotNumber})
        }

        if(params.date){
            queryBuilder.andWhere("DATE(lot.start_time) = :date", { date: params.date });
        }else{
             // queryBuilder.andWhere("DATE(lot.start_time) = :date", { date: new Date().toISOString().split('T')[0] });
        }


        if(params.auction){
            queryBuilder.andWhere("auction.name = :auction", {auction: params.auction} )
        }

        if(params.start_millage && params.end_millage){
            let start_millage = params.start_millage
            let end_millage = params.end_millage
            queryBuilder.andWhere("vehicle.mileage BETWEEN  :start_millage AND :end_millage ",{start_millage,end_millage})
        }else if(params.start_millage){
            let start_millage = params.start_millage
            queryBuilder.andWhere("vehicle.mileage >= :start_millage",{start_millage})
        }else if(params.end_millage){
            let end_millage = params.end_millage
            queryBuilder.andWhere("vehicle.mileage <= :end_millage",{end_millage})
        }

        if(params.start_year && params.end_year){
            let start_year =  params.start_year
            let end_year = params.end_year
            queryBuilder.andWhere("YEAR(vehicle.year) >= :start_year AND YEAR(vehicle.year) <= :end_year ",{start_year,end_year})
        }else if(params.start_year){
            let start_year =  params.start_year
            queryBuilder.andWhere("YEAR(vehicle.year) >= :start_year ",{start_year})
        }else if(params.end_year) {
            let end_year = params.end_year
            queryBuilder.andWhere("YEAR(vehicle.year) <= :end_year ",{end_year})
        }

        if(params.price_max && params.price_min){
            let price_max =  params.price_max
            let price_min = params.price_min
            queryBuilder.andWhere("lot.start_price <= :price_max AND lot.start_price >= :price_min ",{price_max,price_min})
        }else if(params.price_max){
            let price_max =  params.price_max
            queryBuilder.andWhere("lot.start_price <= :price_max ",{price_max})
        }else if(params.price_min) {
            let price_min = params.price_min
            queryBuilder.andWhere("lot.start_price >= :price_min ",{price_min})
        }

        // if(params.start_condition && params.end_condition){
        //     let start_condition =  params.start_condition
        //     let end_condition = params.end_condition
        //     queryBuilder.andWhere("vehicle.condition_ext >= :start_condition AND vehicle.condition_ext <= :end_condition ",{start_condition,end_condition})
        // }else if(params.start_condition){
        //     let start_condition =  params.start_condition
        //     queryBuilder.andWhere("vehicle.condition_ext >= :start_condition ",{start_condition})
        // }else if(params.end_condition) {
        //     let end_condition = params.end_condition
        //     queryBuilder.andWhere("vehicle.condition_ext <= :end_condition ",{end_condition})
        // }

        if(params.start_displace && params.end_displace){
            let start_displace =  params.start_displace
            let end_displace = params.end_displace
            queryBuilder.andWhere("vehicle.displace >= :start_displace AND vehicle.displace <= :end_displace ",{start_displace,end_displace})
        }else if(params.start_displace){
            let start_displace =  params.start_displace
            queryBuilder.andWhere("vehicle.displace >= :start_displace ",{start_displace})
        }else if(params.end_displace) {
            let end_displace = params.end_displace
            queryBuilder.andWhere("vehicle.displace <= :end_displace ",{end_displace})
        }



        // if(!params.page){
        //     queryBuilder.limit(10)
        // }


        if(!currentUser) {
            return await this.searchCarvector(params)
        }


        const page = params.page  || 1;

        const limit = 10;
        const offset = (page - 1) * limit;
        queryBuilder.skip(offset).take(limit)
        const [vehicles, count] = await queryBuilder.getManyAndCount();
        return {lot:  vehicles, count };
    }


    async searchCarvector(params){
        let carsOfCarvector  =  await this.getDateCarvector(
          params.page,
          params.make,
          params.model,
          params.auction,
          params.date,
          params.lotNumber,
          params.start_millage,
          params.end_millage,
          params.start_year,
          params.end_year,
          params.price_min,
          params.price_max,
          params.finish_price_min,
          params.finish_price_max,
          params.condition,
          params.color
        )


        let lots = {
            count: carsOfCarvector ?  carsOfCarvector.navi.rows : 0,
            lot: []
        }

        if (!carsOfCarvector){
            return lots
        }

        const makeAndModel  = carsOfCarvector.navi.md

        for(let lot of carsOfCarvector.body){

            let dateArray = lot.e.split(".").reverse().join("-");
            let [hours, minutes] = lot.f.replaceAll(/\[|\]/g, '').split(':');
            const parsedDate = dayjs(dateArray).add(hours, "hour").add(minutes,"minute");

            let lotNewStruct  =  {
                id: lot.a,
                auction: {
                    name: lot.d
                },
                images: `https://14.ajes.com/imgs/${lot.x}?w=320#https://14.ajes.com/imgs/${lot.y}?w=320#https://14.ajes.com/imgs/${lot.z}?w=320`,
                bidsCount:0,
                end_time: parsedDate,
                start_price: lot.s,
                finish_price: lot.t,
                status: lot.v,
                lot_no: lot.c,
                rate: lot.r,
                vehicle: {
                    chassis: lot.j,
                    displace: lot.q,
                    trans: lot.k,
                    condition_ext: lot.r,
                    mileage:lot.q,
                    model: {
                        name: lot.b == "" ? makeAndModel : lot.b,
                        make: {
                            name: ""
                        }
                    }

                }
            }

            lots.lot.push(lotNewStruct)
        }

        return  lots
    }


    async list (){
        return await this.auctionsEntity.find()
    }

    async lotsList(){
        return await this.lotEntity.find({take:100})
    }

    async paginate(options: IPaginationOptions, filter: FilterLotDto): Promise<Pagination<LotEntity>> {


        const queryBuilder = this.lotEntity.createQueryBuilder('lot');

        if (filter.id !== undefined) {
            queryBuilder.andWhere('lot.id = :id', { id: filter.id });
        }

        if (filter.aj_id) {
            queryBuilder.andWhere('lot.aj_id LIKE :aj_id', { aj_id: `${filter.aj_id}%` });
        }

        if (filter.start_time) {
            queryBuilder.andWhere('lot.start_time >= :start_time', { start_time: filter.start_time });
        }

        if (filter.end_time) {
            queryBuilder.andWhere('lot.end_time <= :end_time', { end_time: filter.end_time });
        }

        if (filter.lot_no) {
            queryBuilder.andWhere('lot.lot_no LIKE :lot_no', { lot_no: `${filter.lot_no}%` });
        }

        if (filter.start_price !== undefined) {
            queryBuilder.andWhere('lot.start_price >= :start_price', { start_price: filter.start_price });
        }

        if (filter.finish_price !== undefined) {
            queryBuilder.andWhere('lot.finish_price <= :finish_price', { finish_price: filter.finish_price });
        }

        if (filter.price_average !== undefined) {
            queryBuilder.andWhere('lot.price_average = :price_average', { price_average: filter.price_average });
        }

        if (filter.average_price_list) {
            queryBuilder.andWhere('lot.average_price_list LIKE :average_price_list', { average_price_list: `${filter.average_price_list}%` });
        }

        if (filter.rate) {
            queryBuilder.andWhere('lot.rate LIKE :rate', { rate: `${filter.rate}%` });
        }

        if (filter.status) {
            queryBuilder.andWhere('lot.status LIKE :status', { status: `${filter.status}%` });
        }

        if (filter.vehicleId !== undefined) {
            queryBuilder.andWhere('lot.vehicleId = :vehicleId', { vehicleId: filter.vehicleId });
        }

        if (filter.auctionId !== undefined) {
            queryBuilder.andWhere('lot.auctionId = :auctionId', { auctionId: filter.auctionId });
        }

        if (filter.auction_list) {
            queryBuilder.andWhere('lot.auction_list LIKE :auction_list', { auction_list: `${filter.auction_list}%` });
        }

        if (filter.actual_purchase_price !== undefined) {
            queryBuilder.andWhere('lot.actual_purchase_price = :actual_purchase_price', { actual_purchase_price: filter.actual_purchase_price });
        }

        if (filter.images) {
            queryBuilder.andWhere('lot.images LIKE :images', { images: `${filter.images}%` });
        }

        if(filter.orderBy){
            const orderBy  = filter.desc == "true" ? "DESC": "ASC"
            queryBuilder.orderBy(filter.orderBy,orderBy)
        }

        // Добавьте другие условия фильтрации по мере необходимости

        return paginate<LotEntity>(queryBuilder, options);
    }

    async inAuctionsVehicles(){
        const currentDate = new Date();

        // const vehicles = await this.lotEntity.find({where: {finish_price: 0},relations:['vehicle']})
        // return vehicles
    }

    async getById (id , currentUser ,lot: Record<string, string> = {}  ) {

        let html = await this.getDateCarvector(
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          true,
          id
        );





        let lotInfo = await this.getDateCarvector(
          undefined,
          undefined,
          undefined,
          lot.auction,
          undefined,
          lot.lot_no,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
        );


        if(!lotInfo){
            lotInfo = await this.getDateCarvector(
              undefined,
              undefined,
              undefined,
              lot.auction,
              undefined,
              lot.lot_no,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              'st'
            );


            html = await this.getDateCarvector(
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              true,
              id,
              "st"
            );
        }


        // if (!lotInfo){
        //     let result = await this.lotEntity.findOne({where:{ id: id}, relations:["vehicle","auction","bids","vehicle.model", "vehicle.model.make"]})
        //     return  result;
        // }

        let lotImages = await this.parsingHtmlLotDetails(html)

        let lotBody  = lotInfo.body[0]

        let dateArray = lotBody.e.split(".").reverse().join("-");

        let [hours, minutes] = lotBody.f.replaceAll(/\[|\]/g, '').split(':');


        const parsedDate = moment.tz(dateArray, 'Asia/Tokyo'); // Устанавливаем временную зону Токио



        if (lotBody.t) {
            parsedDate.add(hours, 'hours').add(minutes, 'minutes');
        }

        let lotDetails = {
            id: lotBody.a,
            aj_id: lotBody.a,
            vehicleId:0,
            start_time: parsedDate.toISOString(),
            end_time:  parsedDate.toISOString(),
            japan_time: `${dateArray} [${hours}:${minutes}]`,
            lot_no: lotBody.c,
            start_price: lotBody.s,
            finish_price: lotBody.t,
            price_average: lotBody.u,
            rate: lotBody.r,
            status: lotBody.v,
            auction_list: lotImages.auctionList,
            images: lotImages.vehicleImages.join("#"),
            vehicle: {
                aj_id: lotBody.a,
             year: lotBody.g,
             chassis: lotBody.j,
                makeId:0,
             model: {
                 name: lotBody?.b.split(" ")[1],
                 make:{name: lotBody?.b.split(" ")[0]}
             },
             grade: lotBody.m,
             color: lotBody.w,
             mileage: lotBody.q,
             displace: lotBody.h,
             trans: lotBody.k,
                equipment:lotBody.m,
                condition_ext:"",
                condition_int:""

            },
            auction: {
                name: lotBody.d
            },
            bids:[]
        }

        let lotId = await this.getLotId(lotDetails.aj_id)
        if(lotId){
            lotDetails.bids  = await this.bids(lotId,currentUser)
        }

        return lotDetails
    }

    async getLotId (aj_id){
            let lot = await this.lotEntity.findOne({where:{aj_id}})
            if(lot){
                return lot.id
            }
            return  false
    }

    async  parsingHtmlLotDetails(html) {
        const $ = cheerio.load(html); // Загружаем HTML в cheerio

        const  vehicleImages = []
        for(let img of $('img[border="0"]')){
           const src = $(img).attr("src")
            if(src.startsWith('https://')){
                vehicleImages.push(src)
            }
        }

        // Извлечение основного изображения аукционного списка
        const auctionList = $('#table_main2 img.table_main2_500').attr('src') || null;

        // Возвращаем объект с данными о лоте и изображениями
        return {
            vehicleImages,
            auctionList
        };

    }



    async getByIdAuction(id: number) {
        return await this.auctionsEntity.findOne({where:{id:id}})
    }

    async getByNameAuction(name: string) {
        return await this.auctionsEntity.findOne({where:{name:name}})
    }


    async bids(lotId: number, currentUser){
        const lot = await this.lotEntity.findOne({where:{id:lotId},relations:['bids','bids.user']})
        const currentUserId = currentUser.userId
        const { role }  = await this.userService.findOne(currentUser.username)


        const bids = Object.assign([], lot.bids);
        let maxBid;

        if (lot.bids.length){
            maxBid = lot.bids.reduce((prev, current) => {
                return (prev.bid_price > current.bid_price) ? prev : current;
            });
        }

        let winnerBid = Object.assign({},maxBid)

        const result  = bids.map(bid => {
            const {password,email,active,username, ...result} = bid.user

            result.created_at = bid.created_at
            if(winnerBid.userId  == bid.userId && winnerBid.bid_price == bid.bid_price){
                result.winner = true
            }else {
                result.winner = false
            }

            result.bid_price = bid.bid_price

            if(currentUserId != bid.userId && role !== "admin"){
                result.first_name = result.first_name.substring(0, 3)+"*****"
                result.last_name = result.last_name.substring(0, 3)+"*****"
            }


            return result
        })

        result.sort((a, b) => {
            return b.winner - a.winner;
        });

        return result
    }


    async createAuction (auctionName: string){
        return await this.auctionsEntity.save({name: auctionName})
    }

    async findAuction(auctionName: string) {
        return await this.auctionsEntity.findOne({where:{name:auctionName}})
    }

    async createOrUpdateLot(lot) {

        let autionId = await this.getByNameAuction(lot.auction.name)
        lot.auction.id = autionId.id

        delete lot.id;
        delete lot.bids;

        // Ищем существующий лот по aj_id
        let existingLot = await this.lotEntity.findOne({ where: { aj_id: lot.aj_id } });

        if (existingLot) {
            // Обновляем существующий лот

            Object.assign(existingLot, lot);
            const updatedLot = await this.lotEntity.save(existingLot);
            let getLot = await this.lotEntity.findOne({ where: { aj_id: lot.aj_id },relations:["vehicle", "bids"] });
            return getLot;

        } else {
            const newLot = await this.lotEntity.save(lot);
            let getLot = await this.lotEntity.findOne({ where: { aj_id: lot.aj_id },relations:["vehicle", "bids"] });
            return getLot;
        }
    }

    async findLotAjId(ajId: string, vehicleid: number, auct_date){
        const lot = await this.lotEntity
            .createQueryBuilder('lot')
            .where('lot.aj_id = :ajId', { ajId })
            .orWhere('lot.vehicleId = :vehicleid', { vehicleid })
            .andWhere('lot.start_time <= :auct_date', { auct_date })
            .getOne();
        return lot;
    }

    async updateLot(ajId: string, newData) {
        // Найдите существующую запись по идентификатору
        const lot = await this.lotEntity.findOne({where: {aj_id:ajId}});// Обновите свойства записи новыми данными
        Object.assign(lot, newData);
        // Сохраните обновленную запись
        return await this.lotEntity.save(lot);
    }


    async getDateCarvector
    (page = 1,
     make = "1",
     model = "",
     auction = "" ,
     date = "",
     lot_number = "",
     start_millage = "",
     end_millage = "",
     start_year = "",
     end_year= "",
     start_price = "",
     end_price = "",
     finish_price_min = "",
     finish_price_max = "",
     condition = "",
     color= "",
     getLotDetails = false,
     url = "",
     type= "aj"
    ) {





        const form = new FormData();

        if(type == "aj"){
            form.append('url_loader', 'aj_neo?file=loader&Q=');
        }else if(type == "st"){
            form.append('url_loader', 'st?file=loader&Q=');
        }


        form.append('page', page);
        form.append('sort_ord', '');
        form.append('url_luboy', '');
        form.append('url_lubaya', '');
        form.append('lose_time_here_buT_not_buy_servlce_for_100_usd_monthly_here_http_avto_jp', 'http://avto.jp/specification.html');
        form.append('tpl', '');
        form.append('edit_post', '');
        form.append('is_stat', '');
        form.append('vendor', make);
        form.append('model', model);
        form.append('bid', lot_number);
        form.append('kuzov', '');
        form.append('rate', '');
        form.append('status', '');
        form.append('kpp_add', '');
        form.append('colour', color);
        form.append('auct_name', auction);
        form.append('_day', date && Number(date.split('-')[2]));
        form.append('_rate', condition);
        form.append('_status', '');
        form.append('_kpp_add', '');
        form.append('_auct_name', '');
        form.append('list_size', '1');
        form.append('_list_size', '1');
        form.append('lhw', '');
        form.append('eqqp', '');
        form.append('stDt1', '');
        form.append('stDt2', '');
        form.append('year', start_year);
        form.append('year2', end_year);
        form.append('probeg', start_millage);
        form.append('probeg2', end_millage);
        form.append('eng_v', '');
        form.append('eng_v2', '');
        form.append('price_start', start_price);
        form.append('price_start2', end_price);
        form.append('price_finish', finish_price_min);
        form.append('price_finish2', finish_price_max);
        form.append('_year', '');
        form.append('_year2', '');
        form.append('_probeg', '');
        form.append('_probeg2', '');
        form.append('_eng_v', '');
        form.append('_eng_v2', '');
        form.append('_price_start', '');
        form.append('_price_start2', '');
        form.append('_price_finish', '');
        form.append('_price_finish2', '');

        let urlCarvector = getLotDetails ? `https://auc.carvector.com/${type}-${url}.html` : `https://auc.carvector.com/${type == "aj" ? "aj_neo":"st"}`;

        if(!getLotDetails) {

            const response = await axios.post(
              urlCarvector,
              form,
              {

                  params:type == "aj" ? {
                      'file': 'loader',
                      'Q': '',
                      'ajx': '17259781405415-form'
                  }: {
                      'file': 'loader',
                      'Q': '',
                      'ajx': '17275379332903-form'
                  } ,

                  headers: {
                      ...form.getHeaders(),
                      'authority': 'auc.carvector.com',
                      'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                      'accept-language': 'ru-KG,ru;q=0.9,ky-KG;q=0.8,ky;q=0.7,ru-RU;q=0.6,en-US;q=0.5,en;q=0.4',
                      'cache-control': 'max-age=0',
                      'content-type': 'multipart/form-data; boundary=----WebKitFormBoundarykFR68MD84J1o9A2r',
                      'cookie': '_gcl_au=1.1.251415057.1722814315; _ym_uid=1722814316911937817; _ym_d=1722814316; _fbp=fb.1.1722814315883.234915349128241383; aj_lang=ru; aj_geo=ru; aj_geo2=Bishkek; aj_geo3=kg; aj_next=:Uz0UXUopRnGwkK:71446IJ7DmOE3df:2ftCqEi3AXVbdQt:TW4bhqKerBHBTm:cJoaqHtu4OKg97A:tDlF7zLHMBJsWmR:3LIy4sG4FGi3kA:StacLTDo36vfnq:IFk63aC9JZcdVo:2fvSTOSeVprpDya:ZxfKc7tVSG1yNv:Jrqnqz9ZiAl11E:onV8Rv6wLYlDG:0x1QiBaIpvxZE96:Q91jGcA19DYm3s:cJmO0ckaglPIGhz:TKezBAlZoioI3Z:mQZDCVobVgRecDD:8sdjZwwABQA0aP1:2eI3y52RFE1iw3U; _gid=GA1.2.462620620.1726665472; _ym_isad=2; _ym_visorc=w; ajuser=a2FseWJla2pha3lwYmVrb3ZAZ21haWwuY29tOjVhZmE3NThjYjBiNGZlMjliZGMyYjMyMzg5ODFmMDllOjMwMTUwMDY6Og%3D%3D; ajuser_isSigned2.0=1; _cvr_uid=56709; _ga_2FVST1T41S=GS1.2.1726665569.14.1.1726665573.56.0.0; _ga_MND4RP79WH=GS1.1.1726665470.21.1.1726665577.0.0.0; _ga=GA1.1.478201195.1722814316; _ga_KY00ZWYPC8=GS1.1.1726665470.23.1.1726666281.0.0.0',
                      'origin': 'https://auc.carvector.com',
                      'referer': 'https://auc.carvector.com/?lang=ru',
                      'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
                      'sec-ch-ua-mobile': '?0',
                      'sec-ch-ua-platform': '"macOS"',
                      'sec-fetch-dest': 'iframe',
                      'sec-fetch-mode': 'navigate',
                      'sec-fetch-site': 'same-origin',
                      'sec-fetch-user': '?1',
                      'upgrade-insecure-requests': '1',
                      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'
                  }
              }
            );



            let match = response.data.match(/var data = ({.*});/);
            if (match && match[1]) {
                let extractedObject = match[1];
                let carJson = await this.fixJsonString(extractedObject)

                return JSON.parse(carJson)

            } else {

                 return  false
            }
        }else{
            const response = await axios.get(
              urlCarvector,
              {
                  headers: {
                      'authority': 'auc.carvector.com',
                      'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                      'accept-language': 'ru-KG,ru;q=0.9,ky-KG;q=0.8,ky;q=0.7,ru-RU;q=0.6,en-US;q=0.5,en;q=0.4',
                      'cache-control': 'max-age=0',
                      'content-type': 'multipart/form-data; boundary=----WebKitFormBoundarykFR68MD84J1o9A2r',
                      'cookie': '_gcl_au=1.1.251415057.1722814315; _ym_uid=1722814316911937817; _ym_d=1722814316; _fbp=fb.1.1722814315883.234915349128241383; aj_lang=ru; aj_geo=ru; aj_geo2=Bishkek; aj_geo3=kg; aj_next=:Uz0UXUopRnGwkK:71446IJ7DmOE3df:2ftCqEi3AXVbdQt:TW4bhqKerBHBTm:cJoaqHtu4OKg97A:tDlF7zLHMBJsWmR:3LIy4sG4FGi3kA:StacLTDo36vfnq:IFk63aC9JZcdVo:2fvSTOSeVprpDya:ZxfKc7tVSG1yNv:Jrqnqz9ZiAl11E:onV8Rv6wLYlDG:0x1QiBaIpvxZE96:Q91jGcA19DYm3s:cJmO0ckaglPIGhz:TKezBAlZoioI3Z:mQZDCVobVgRecDD:8sdjZwwABQA0aP1:2eI3y52RFE1iw3U; _gid=GA1.2.462620620.1726665472; _ym_isad=2; _ym_visorc=w; ajuser=a2FseWJla2pha3lwYmVrb3ZAZ21haWwuY29tOjVhZmE3NThjYjBiNGZlMjliZGMyYjMyMzg5ODFmMDllOjMwMTUwMDY6Og%3D%3D; ajuser_isSigned2.0=1; _cvr_uid=56709; _ga_2FVST1T41S=GS1.2.1726665569.14.1.1726665573.56.0.0; _ga_MND4RP79WH=GS1.1.1726665470.21.1.1726665577.0.0.0; _ga=GA1.1.478201195.1722814316; _ga_KY00ZWYPC8=GS1.1.1726665470.23.1.1726666281.0.0.0',
                      'origin': 'https://auc.carvector.com',
                      'referer': 'https://auc.carvector.com/?lang=ru',
                      'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
                      'sec-ch-ua-mobile': '?0',
                      'sec-ch-ua-platform': '"macOS"',
                      'sec-fetch-dest': 'iframe',
                      'sec-fetch-mode': 'navigate',
                      'sec-fetch-site': 'same-origin',
                      'sec-fetch-user': '?1',
                      'upgrade-insecure-requests': '1',
                      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'
                  }
              }
            );


            return response.data
        }

    }



    async fixJsonString(str) {
        // Заменяем одинарные кавычки на двойные (если они неправильно используются)
        str = str.replace(/'/g, '"');

        // Исправляем неправильные символы (например, �������)
        str = str.replace(/�������/g, 'unknown');

        // Заменяем неправильные кавычки типа \"
        str = str.replace(/\\"/g, '"');

        // Оборачиваем ключи JSON в двойные кавычки (если это необходимо)
        str = str.replace(/([{,])\s*([a-zA-Z0-9_]+)\s*:/g, '$1"$2":');

        return str;
    }





}
