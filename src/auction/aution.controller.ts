import { Body, Controller, Get, Param, ParseArrayPipe, Post, Query, Res, UseGuards } from "@nestjs/common";
import {AutionService} from "./aution.service";
import {SearchVehiclesDto} from "./dto/search-vehicles.dto";
import {Response} from "express";
import {JwtAuthGuard} from "../auth/guard/jwt-auth.guard";
import {CurrentUser} from "../auth/decorators/current-user.decorator";
import { LotEntity } from "./entity/lot.entity";
import { Pagination } from "nestjs-typeorm-paginate";
import { FilterLotDto } from "./dto/filterLot.dto";


@UseGuards(JwtAuthGuard)
@Controller('auction')
export class AutionController {

    constructor(private  readonly  auctionService: AutionService) {
    }

    // @Get('/test')
    // async test(){
    //     return  await  this.auctionService.getDateCarvector()
    // }

    @Get('/search')
    async search(@Query() params: SearchVehiclesDto){
        return await this.auctionService.search(params)
    }
    @Get('/list')
    async list(){
        return await this.auctionService.list();
    }

    @Get("/lots")
    async lotsList(@Query('page') page: number = 1, @Query('limit') limit: number = 10, @Query() params: FilterLotDto): Promise<Pagination<LotEntity>>{
        return this.auctionService.paginate({
            page,
            limit,
        },params)
    }


    @Get("/lot/:id")
    async getById(@Param("id") id, @CurrentUser() currentUser, @Query() lot ){

        return await this.auctionService.getById(id, currentUser, lot)
    }


    @Get('/')
    async listAuction (@Res() res: Response){
        const auctions  = await this.auctionService.list()
        res.set('Content-Range', `cars 0-${auctions.length}/${auctions.length}`);
        res.send(auctions);
    }

    @Get('/:id')
    async getByIdAuction (@Param("id") id){
        return await this.auctionService.getByIdAuction(id)
    }

    @Get('/lot/:id/bids')
    async bids (@Param("id") id, @CurrentUser() currentUser){
        return await this.auctionService.bids(id, currentUser)
    }



    @Post("/lot/create")
    async createLot(@Body() lot){
        return this.auctionService.createOrUpdateLot(lot)
    }



}
