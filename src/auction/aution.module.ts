import { Module } from '@nestjs/common';
import { AutionService } from './aution.service';
import { AutionController } from './aution.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {LotEntity} from "./entity/lot.entity";
import {AuctionsEntity} from "./entity/auctions.entity";
import { UserModule } from "../user/user.module";



@Module({
  imports:[TypeOrmModule.forFeature([LotEntity,AuctionsEntity]),UserModule],
  providers: [AutionService],
  controllers: [AutionController],
  exports: [AutionService]
})
export class AutionModule {}
