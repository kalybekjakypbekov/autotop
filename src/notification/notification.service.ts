import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { NotificationEntity } from "./entity/notification.entity";
import { Repository } from "typeorm";


@Injectable()
export  class NotificationService{

  constructor(@InjectRepository(NotificationEntity) private readonly notificationEntity: Repository<NotificationEntity>) {
  }
  async getList (currentUser){
      const notifications = await this.notificationEntity.find({where:{user_id: currentUser.userId}})
      return notifications
  }

}