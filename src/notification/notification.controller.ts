import { Controller, Get, UseGuards } from "@nestjs/common";
import { NotificationService } from "./notification.service";
import { CurrentUser } from "../auth/decorators/current-user.decorator";
import { JwtAuthGuard } from "../auth/guard/jwt-auth.guard";


@Controller('notification')
@UseGuards(JwtAuthGuard)
export class NotificationController {

  constructor(private readonly notificationService: NotificationService) {
  }


  @Get('/')
  async getList(@CurrentUser() user){
        return await this.notificationService.getList(user)
  }


}