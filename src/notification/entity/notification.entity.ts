import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "../../user/entity/user.entity";

@Entity({name:"notification"})
export class NotificationEntity {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  icon: string

  @Column()
  title: string

  @Column()
  description: string

  @Column({type: "datetime"})
  time: string

  @Column()
  ready: boolean

  @Column()
  link: string

  @Column()
  useRouter: string

  @Column()
  user_id: number

  @ManyToOne(()=>UserEntity, (user) => user.notification)
  @JoinColumn({name:"user_id", referencedColumnName:"id"})
  user: UserEntity

}