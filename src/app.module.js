"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.AppModule = void 0;
var common_1 = require("@nestjs/common");
var aution_module_1 = require("./auction/aution.module");
var car_module_1 = require("./car/car.module");
var bid_module_1 = require("./bid/bid.module");
var role_module_1 = require("./role/role.module");
var user_module_1 = require("./user/user.module");
var auth_module_1 = require("./auth/auth.module");
var config_1 = require("@nestjs/config");
var path_1 = require("path");
var typeorm_1 = require("@nestjs/typeorm");
var vehicle_module_1 = require("./vehicle/vehicle.module");
var media_module_1 = require("./media/media.module");
var config_service_1 = require("./config/config.service");
var config_controller_1 = require("./config/config.controller");
var platform_express_1 = require("@nestjs/platform-express");
var income_module_1 = require("./income/income.module");
var favorite_module_1 = require("./favorite/favorite.module");
var purchases_module_1 = require("./purchases/purchases.module");
var tasks_module_1 = require("./tasks/tasks.module");
var parser_module_1 = require("./parser/parser.module");
var ship_module_1 = require("./ship/ship.module");
var consignee_module_1 = require("./consignee/consignee.module");
var document_module_1 = require("./document/document.module");
var DEFAULT_ADMIN = {
    email: 'admin@example.com',
    password: 'password'
};
var authenticate = function (email, password) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        if (email === DEFAULT_ADMIN.email && password === DEFAULT_ADMIN.password) {
            return [2 /*return*/, Promise.resolve(DEFAULT_ADMIN)];
        }
        return [2 /*return*/, null];
    });
}); };
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        (0, common_1.Module)({
            imports: [aution_module_1.AutionModule, car_module_1.CarModule, bid_module_1.BidModule, role_module_1.RoleModule, user_module_1.UserModule, auth_module_1.AuthModule, config_1.ConfigModule.forRoot(), typeorm_1.TypeOrmModule.forRoot({
                    type: process.env.DB_TYPE,
                    host: process.env.DB_HOST,
                    port: parseInt(process.env.DB_PORT, 10),
                    username: process.env.DB_USERNAME,
                    password: process.env.DB_PASSWORD,
                    database: process.env.DB_DATABASE,
                    entities: [(0, path_1.join)(__dirname, '**/*.entity{.ts,.js}')],
                    synchronize: true,
                    timezone: 'Asia/Tokyo',
                    extra: {
                        max_allowed_packet: 128 * 1024 * 1024
                    }
                }), vehicle_module_1.VehicleModule, media_module_1.MediaModule,
                platform_express_1.MulterModule.register({
                    dest: "../public/image"
                }),
                // AdminModule.createAdmin({...adminOptions}),
                income_module_1.IncomeModule,
                favorite_module_1.FavoriteModule,
                purchases_module_1.PurchasesModule,
                tasks_module_1.TasksModule,
                parser_module_1.ParserModule,
                ship_module_1.ShipModule,
                consignee_module_1.ConsigneeModule,
                document_module_1.DocumentModule,
            ],
            controllers: [config_controller_1.ConfigController],
            providers: [config_service_1.ConfigService]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
