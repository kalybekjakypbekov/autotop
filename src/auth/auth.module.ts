import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import {JwtModule} from "@nestjs/jwt";
import {jwtConstants} from "./config/constants";
import {UserModule} from "../user/user.module";
import {LocalStrategy} from "./strategy/local.strategy";
import {JwtStrategy} from "./strategy/jwt.strategy";
import {PassportModule} from "@nestjs/passport";

@Module({
  imports: [
      JwtModule.register({ secret: jwtConstants.secret, signOptions: { expiresIn: '3600s' }}),
      PassportModule.register({session:true}),
      UserModule
  ],
  providers: [AuthService , LocalStrategy, JwtStrategy],
  controllers: [AuthController]
})
export class AuthModule {}
