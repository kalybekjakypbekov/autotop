import { Body, Controller, HttpCode, HttpStatus, Post, UseGuards, Request, Get } from "@nestjs/common";
import {AuthService} from "./auth.service";
import {UserService} from "../user/user.service";
import {LoginDto} from "./dto/login.dto";
import {CreateUserDto} from "../user/dto/create-user.dto";
import {LocalAuthGuard} from "./guard/local-auth.guard";
import { CurrentUser } from "./decorators/current-user.decorator";
import { JwtAuthGuard } from "./guard/jwt-auth.guard";

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService,private readonly userService:UserService) {}

    @HttpCode(HttpStatus.OK)
    @Post('login')
    @UseGuards(LocalAuthGuard)
    async login(@Request() req,@Body() loginDto: LoginDto) {
        return await this.authService.login(req.user)
    }

    @Post('register')
    async register(@Body() createUserDto:CreateUserDto){
        let user = await this.userService.createUser(createUserDto);
        return user;
    }

    @UseGuards(JwtAuthGuard)
    @Get('user')
    async getUserByJWT(@CurrentUser() user,@Request() req){
        const token = req.headers.authorization.replace('Bearer ', '');
        return  await this.authService.getUserByToken(token);
    }


}
