import { Injectable } from '@nestjs/common';
import {UserService} from "../user/user.service";
import {JwtService} from "@nestjs/jwt";
import * as bcrypt from 'bcrypt';
import { jwtConstants } from "./config/constants";
@Injectable()
export class AuthService {
    constructor(
        private userService: UserService,
        private jwtService: JwtService
    ) {}




    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.userService.findOne(username);

        if(!user)
            return null

        let comparePassword = await bcrypt.compare(pass, user.password);

        if (user && comparePassword) {

            //
            // if(!user.isVerifyEmail){
            //     throw new ConflictException('Email is not activated');
            // }
            if(user.role == "admin"){
                const userAdmin  = {
                    uid: user.id,
                    role: user.role,
                    username: user.username,
                    from:"",
                    data: {
                        displayName:  user.first_name + " " + user.last_name,
                        photoURL:"",
                        email: user.email,
                        shortcuts:[],

                        settings: {
                            "layout": { },
                            "theme": { }
                        },
                        loginRedirectUrl:""
                    }
                }
                return  {user : userAdmin}
            }


            const { password, ...result } = user;
            return {user: result};
        }

        return null;
    }

    async login({user}: any) {
        let payload ;
        let {password, ...result} = user
        if(user.role === "admin"){
            result = {user: result}
            payload = { username: user.username, sub: user.uid };
        }else{
            payload = { username: user.username, sub: user.id };
        }
        return {
            ...result,
            access_token: this.jwtService.sign(payload),
        };

    }


    async getUserByToken (token: string){
        const decodedToken = await this.jwtService.verify(token,{secret: jwtConstants.secret});
        const username = decodedToken.username;
        const user  = await  this.userService.findOne(username);

        const userAdmin  = {
            uid: user.id,
            role: user.role,
            username: user.username,
            from:"",
            data: {
                displayName:  user.first_name + " " + user.last_name,
                photoURL:"",
                email: user.email,
                shortcuts:[],

                settings: {
                    "layout": { },
                    "theme": { }
                },
                loginRedirectUrl:""
            },
            access_token: token
        }

        return userAdmin;
    }


}