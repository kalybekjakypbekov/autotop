import { Module } from '@nestjs/common';
import { AutionModule } from './auction/aution.module';
import { CarModule } from './car/car.module';
import { BidModule } from './bid/bid.module';
import { RoleModule } from './role/role.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from "@nestjs/config";
import { join } from 'path';
import { VehicleModule } from './vehicle/vehicle.module';
import { MediaModule } from './media/media.module';
import { ConfigService } from './config/config.service';
import { ConfigController } from './config/config.controller';
import { MulterModule } from "@nestjs/platform-express";
import { IncomeModule } from './income/income.module';
import { FavoriteModule } from './favorite/favorite.module';
import { PurchasesModule } from './purchases/purchases.module';
import { TasksModule } from './tasks/tasks.module';
import { ParserModule } from './parser/parser.module';
import { ShipModule } from './ship/ship.module';
import { ConsigneeModule } from './consignee/consignee.module';
import { DocumentModule } from './document/document.module';
import {TypeOrmModule} from "@nestjs/typeorm";
import { DashboardModule } from "./dashboard/dashboard.module";
import { NotificationModule } from "./notification/notification.module";
import { CustomerModule } from "./customer/customer.module";
import { OrderModule } from "./order/order.module";
import { PaymentModule } from "./payment/payment.module";

@Module({
    imports: [AutionModule, CarModule, BidModule, RoleModule, UserModule, AuthModule, ConfigModule.forRoot({isGlobal:true}), TypeOrmModule.forRoot({
        type: "mysql",
        host: process.env.DB_HOST,
        port: parseInt(process.env.DB_PORT, 10),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        entities: [join(__dirname, '**/*.entity{ .ts,.js}')],
        synchronize: true,
        migrations: ['dist/migrations/*.js'],
        timezone:"Asia/Tokyo"
    }), VehicleModule, MediaModule,
        MulterModule.register({
            dest: "../public/image",
        }),
        IncomeModule,
        FavoriteModule,
        PurchasesModule,
        TasksModule,
        ParserModule,
        ShipModule,
        ConsigneeModule,
        DocumentModule,
        DashboardModule,
        NotificationModule,
        CustomerModule,
        OrderModule,
        PaymentModule
    ],
    controllers: [ConfigController],
    providers: [ConfigService],

})
export class AppModule {


}
