import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {ValidationPipe} from "@nestjs/common";
import {join} from "path";
import {NestExpressApplication} from "@nestjs/platform-express";
import * as dotenv from 'dotenv';
import 'reflect-metadata';


async function bootstrap() {

  dotenv.config();
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.useGlobalPipes(new ValidationPipe({
    forbidUnknownValues: false
  }));

  app.enableCors({
    exposedHeaders: ['Content-Range'],
  });

  app.useStaticAssets(join(__dirname, '..', 'public'));
  await app.listen(5001);
}
bootstrap();
