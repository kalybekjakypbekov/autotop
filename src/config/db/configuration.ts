
export default () => ({
    database: {
        host: process.env.DB_HOST,
        type: process.env.DB_TYPE,
        port: parseInt(process.env.DB_PORT, 10),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        entities: [],
        synchronize: true,
    },
    // Add more configuration options as needed
});