import {Body, Controller, Get, Param, Put, Query, Res, UseGuards} from '@nestjs/common';
import {CarService} from "./car.service";
import { Response } from 'express';

@Controller('car')
export class CarController {

    constructor(private readonly carService:CarService) {
    }

    @Get('/')
    async listCarsAdmin(@Res() res: Response){
        const cars  =  await this.carService.listCarsAdmin()
        res.set('Content-Range', `cars 0-${cars.length}/${cars.length}`);
        res.send(cars);
    }


    @Get('/list')
    async listCars(){
       return await this.carService.listCars()
    }

    @Get('/make/list')
    async listMake(){
        return await this.carService.listMake()
    }

    @Get('/model/list')
    async listModel(){
        return await this.carService.listModel()
    }


    @Get('/:id')
    async getById(@Param("id") id: number){
        const car = await this.carService.getById(id)
        return  car
    }

    @Put('/:id')
    async Update(@Param("id") id: number, @Body() carData){
        const car = await this.carService.update(id, carData)
        return  car
    }




}
