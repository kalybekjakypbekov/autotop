"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CarModule = void 0;
var common_1 = require("@nestjs/common");
var car_controller_1 = require("./car.controller");
var car_service_1 = require("./car.service");
var typeorm_1 = require("@nestjs/typeorm");
var make_entity_1 = require("./entity/make.entity");
var model_entity_1 = require("./entity/model.entity");
var CarModule = /** @class */ (function () {
    function CarModule() {
    }
    CarModule = __decorate([
        (0, common_1.Module)({
            imports: [typeorm_1.TypeOrmModule.forFeature([make_entity_1.MakeEntity, model_entity_1.ModelEntity])],
            controllers: [car_controller_1.CarController],
            providers: [car_service_1.CarService],
            exports: [car_service_1.CarService]
        })
    ], CarModule);
    return CarModule;
}());
exports.CarModule = CarModule;
