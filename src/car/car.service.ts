import {ConflictException, Injectable, ParseIntPipe} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {ModelEntity} from "./entity/model.entity";
import {Repository} from "typeorm";
import {MakeEntity} from "./entity/make.entity";


@Injectable()
export class CarService {
    constructor(@InjectRepository(MakeEntity) private readonly makeEntity: Repository<MakeEntity>,
                @InjectRepository(ModelEntity) private  readonly modelEntity: Repository<ModelEntity>
                ) {
    }

    // front ui

    async  listCars(){
        return await this.makeEntity.find({relations:['model']})
    }

    async  listMake(){
        return await this.makeEntity.find()
    }

    async  listModel(){
        return await this.modelEntity.find()
    }
    // admin
    async  listCarsAdmin (){
        return await this.makeEntity.find({select:['id', "name"]})
    }

    async getById(id: number) {
        const result  = await this.makeEntity.findOne({where:{id: id}})
        return result
    }

    async update(id: number ,carData){
        const car  = await  this.makeEntity.findOne({where: {id: id}})
        if(!car) {
            throw new ConflictException('Car is not');
        }
        const updateCar  =  {...car, ...carData}
        return await this.makeEntity.save(updateCar)
    }

    async getByMakeName (name: string){
        return await  this.makeEntity.findOne({where:{name: name}})
    }



    async createMake (name){
        return await this.makeEntity.save({name: name})
    }

    async createModel (name: string, makeId: number){
        return await this.modelEntity.save({name: name, makeId: makeId})
    }

    async getByModelName (name: string , makeId: number){
        return await this.modelEntity.findOne({where:{name: name, makeId: makeId}})
    }

    async getByModel (name: string){
        return await this.modelEntity.findOne({where:{name: name}, relations:['make']})
    }

}
