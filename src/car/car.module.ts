import { Module } from '@nestjs/common';
import { CarController } from './car.controller';
import { CarService } from './car.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {MakeEntity} from "./entity/make.entity";
import {ModelEntity} from "./entity/model.entity";

@Module({
  imports: [TypeOrmModule.forFeature([MakeEntity,ModelEntity])],
  controllers: [CarController],
  providers: [CarService],
  exports: [CarService]
})
export class CarModule {}
