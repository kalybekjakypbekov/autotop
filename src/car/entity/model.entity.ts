import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn
} from "typeorm";
import {MakeEntity} from "./make.entity";
import {VehicleEntity} from "../../vehicle/entity/vehicle.entity";

@Entity({name:"models"})
export  class ModelEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Index()
    id: number;

    @Column()
    name: string;

    @Column()
    makeId: number

    @ManyToOne(() => MakeEntity, (make) => make.model)
    @JoinColumn({name: "makeId", referencedColumnName: "id"})
    make: MakeEntity;

    @OneToMany(() => VehicleEntity, (vehicle )=> vehicle.model)
    vehicle: VehicleEntity[]

}