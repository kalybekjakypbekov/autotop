import {BaseEntity, Column, Entity, Index, OneToMany, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {ModelEntity} from "./model.entity";
import {VehicleEntity} from "../../vehicle/entity/vehicle.entity";

@Entity({name: "makes"})
export  class MakeEntity  extends  BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({unique:true})
    name: string;

    @Column({default: 0})
    carvector_make_id: number;

    @OneToMany(()=>ModelEntity, (model) => model.make)
    model : ModelEntity[];

    @OneToMany(() => VehicleEntity, (vehicle) => vehicle.make)
    vehicle: VehicleEntity

}