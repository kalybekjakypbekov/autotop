import { Seeder } from 'typeorm-seeding';

import { AppDataSource } from '../../data-source';
import { ModelEntity } from "../car/entity/model.entity"; // Ensure the correct path

export class CreateModelsSeed implements Seeder {
  public async run(): Promise<void> {
    const modelRepository = AppDataSource.getRepository(ModelEntity);

    const getModel =  await modelRepository.find()
    if(getModel.length){
      return undefined
    }

    const models = [
      {
        "id": 1,
        "name": "147",
        "makeId": 1
      },
      {
        "id": 2,
        "name": "156",
        "makeId": 1
      },
      {
        "id": 3,
        "name": "159",
        "makeId": 1
      },
      {
        "id": 4,
        "name": "159 SPORTWAGON",
        "makeId": 1
      },
      {
        "id": 5,
        "name": "4C",
        "makeId": 1
      },
      {
        "id": 6,
        "name": "75",
        "makeId": 1
      },
      {
        "id": 7,
        "name": "BRERA",
        "makeId": 1
      },
      {
        "id": 8,
        "name": "GIULIA",
        "makeId": 1
      },
      {
        "id": 9,
        "name": "GIULIETTA",
        "makeId": 1
      },
      {
        "id": 10,
        "name": "GT",
        "makeId": 1
      },
      {
        "id": 11,
        "name": "MITO",
        "makeId": 1
      },
      {
        "id": 12,
        "name": "SPIDER",
        "makeId": 1
      },
      {
        "id": 13,
        "name": "SPORTWAGON",
        "makeId": 1
      },
      {
        "id": 14,
        "name": "STELVIO",
        "makeId": 1
      },
      {
        "id": 15,
        "name": "DBX",
        "makeId": 2
      },
      {
        "id": 16,
        "name": "V8VANCP",
        "makeId": 2
      },
      {
        "id": 17,
        "name": "80 SERIES",
        "makeId": 3
      },
      {
        "id": 18,
        "name": "A1",
        "makeId": 3
      },
      {
        "id": 19,
        "name": "A1 SPORTBACK",
        "makeId": 3
      },
      {
        "id": 20,
        "name": "A3",
        "makeId": 3
      },
      {
        "id": 21,
        "name": "A3 SEDAN",
        "makeId": 3
      },
      {
        "id": 22,
        "name": "A3 SPORTBACK E-TRON",
        "makeId": 3
      },
      {
        "id": 23,
        "name": "A4",
        "makeId": 3
      },
      {
        "id": 24,
        "name": "A4 ALL ROAD QUATTRO",
        "makeId": 3
      },
      {
        "id": 25,
        "name": "A4 AVANTE",
        "makeId": 3
      },
      {
        "id": 26,
        "name": "A5",
        "makeId": 3
      },
      {
        "id": 27,
        "name": "A5 CABRIO",
        "makeId": 3
      },
      {
        "id": 28,
        "name": "A5 SPORTBACK",
        "makeId": 3
      },
      {
        "id": 29,
        "name": "A6",
        "makeId": 3
      },
      {
        "id": 30,
        "name": "A6 AVANTE",
        "makeId": 3
      },
      {
        "id": 31,
        "name": "A7",
        "makeId": 3
      },
      {
        "id": 32,
        "name": "A7 SPORTBACK",
        "makeId": 3
      },
      {
        "id": 33,
        "name": "A8",
        "makeId": 3
      },
      {
        "id": 34,
        "name": "ALLROAD",
        "makeId": 3
      },
      {
        "id": 35,
        "name": "Q2",
        "makeId": 3
      },
      {
        "id": 36,
        "name": "Q3",
        "makeId": 3
      },
      {
        "id": 37,
        "name": "Q4 E-TRON",
        "makeId": 3
      },
      {
        "id": 38,
        "name": "Q5",
        "makeId": 3
      },
      {
        "id": 39,
        "name": "Q7",
        "makeId": 3
      },
      {
        "id": 40,
        "name": "RS Q3",
        "makeId": 3
      },
      {
        "id": 41,
        "name": "S1",
        "makeId": 3
      },
      {
        "id": 42,
        "name": "S3",
        "makeId": 3
      },
      {
        "id": 43,
        "name": "S3 SEDAN",
        "makeId": 3
      },
      {
        "id": 44,
        "name": "S4",
        "makeId": 3
      },
      {
        "id": 45,
        "name": "S4 AVANTE",
        "makeId": 3
      },
      {
        "id": 46,
        "name": "S5",
        "makeId": 3
      },
      {
        "id": 47,
        "name": "S5 SPORTBACK",
        "makeId": 3
      },
      {
        "id": 48,
        "name": "S7",
        "makeId": 3
      },
      {
        "id": 49,
        "name": "S8",
        "makeId": 3
      },
      {
        "id": 50,
        "name": "SQ5",
        "makeId": 3
      },
      {
        "id": 51,
        "name": "TT",
        "makeId": 3
      },
      {
        "id": 52,
        "name": "TTS COUPE",
        "makeId": 3
      },
      {
        "id": 53,
        "name": "ARNAGE",
        "makeId": 4
      },
      {
        "id": 54,
        "name": "NAVY BLUE CHICP4W",
        "makeId": 4
      },
      {
        "id": 55,
        "name": "OTHER",
        "makeId": 4
      },
      {
        "id": 56,
        "name": "1 SERIES",
        "makeId": 5
      },
      {
        "id": 57,
        "name": "2 SERIES",
        "makeId": 5
      },
      {
        "id": 58,
        "name": "3 SERIES",
        "makeId": 5
      },
      {
        "id": 59,
        "name": "3 SERIES 2D",
        "makeId": 5
      },
      {
        "id": 60,
        "name": "4 SERIES",
        "makeId": 5
      },
      {
        "id": 61,
        "name": "5 SERIES",
        "makeId": 5
      },
      {
        "id": 62,
        "name": "6 SERIES",
        "makeId": 5
      },
      {
        "id": 63,
        "name": "7 SERIES",
        "makeId": 5
      },
      {
        "id": 64,
        "name": "8 SERIES",
        "makeId": 5
      },
      {
        "id": 65,
        "name": "I7",
        "makeId": 5
      },
      {
        "id": 66,
        "name": "I8",
        "makeId": 5
      },
      {
        "id": 67,
        "name": "IX",
        "makeId": 5
      },
      {
        "id": 68,
        "name": "IX1",
        "makeId": 5
      },
      {
        "id": 69,
        "name": "M2",
        "makeId": 5
      },
      {
        "id": 70,
        "name": "M3",
        "makeId": 5
      },
      {
        "id": 71,
        "name": "M4",
        "makeId": 5
      },
      {
        "id": 72,
        "name": "M5",
        "makeId": 5
      },
      {
        "id": 73,
        "name": "MINI",
        "makeId": 5
      },
      {
        "id": 74,
        "name": "OTHER",
        "makeId": 5
      },
      {
        "id": 75,
        "name": "X1",
        "makeId": 5
      },
      {
        "id": 76,
        "name": "X2",
        "makeId": 5
      },
      {
        "id": 77,
        "name": "X3",
        "makeId": 5
      },
      {
        "id": 78,
        "name": "X4",
        "makeId": 5
      },
      {
        "id": 79,
        "name": "X5 SERIES",
        "makeId": 5
      },
      {
        "id": 80,
        "name": "X6",
        "makeId": 5
      },
      {
        "id": 81,
        "name": "X6 M",
        "makeId": 5
      },
      {
        "id": 82,
        "name": "Z3 ROADSTER",
        "makeId": 5
      },
      {
        "id": 83,
        "name": "Z4",
        "makeId": 5
      },
      {
        "id": 84,
        "name": "i3",
        "makeId": 5
      },
      {
        "id": 85,
        "name": "B3",
        "makeId": 6
      },
      {
        "id": 86,
        "name": "CADE ESCALADE",
        "makeId": 7
      },
      {
        "id": 87,
        "name": "CADE ESKAEXT4W",
        "makeId": 7
      },
      {
        "id": 88,
        "name": "CTS",
        "makeId": 7
      },
      {
        "id": 89,
        "name": "SRX CROSSOVER",
        "makeId": 7
      },
      {
        "id": 90,
        "name": "OTHER",
        "makeId": 8
      },
      {
        "id": 91,
        "name": "CORVETTE",
        "makeId": 9
      },
      {
        "id": 92,
        "name": "CRUZE",
        "makeId": 9
      },
      {
        "id": 93,
        "name": "CVL ASTRO",
        "makeId": 9
      },
      {
        "id": 94,
        "name": "CVL BLAZER 4W",
        "makeId": 9
      },
      {
        "id": 95,
        "name": "CVL CORVETTE CP",
        "makeId": 9
      },
      {
        "id": 96,
        "name": "CVL EXPRESS",
        "makeId": 9
      },
      {
        "id": 97,
        "name": "CVLKOROLADO",
        "makeId": 9
      },
      {
        "id": 98,
        "name": "CVLSHEBI VAN",
        "makeId": 9
      },
      {
        "id": 99,
        "name": "300",
        "makeId": 10
      },
      {
        "id": 100,
        "name": "300C",
        "makeId": 10
      },
      {
        "id": 101,
        "name": "GRAND VOYAGE",
        "makeId": 10
      },
      {
        "id": 102,
        "name": "GRAND VOYAGER",
        "makeId": 10
      },
      {
        "id": 103,
        "name": "JEEP CHEROKEE",
        "makeId": 10
      },
      {
        "id": 104,
        "name": "JEEP COMMANDER",
        "makeId": 10
      },
      {
        "id": 105,
        "name": "JEEP COMPASS",
        "makeId": 10
      },
      {
        "id": 106,
        "name": "JEEP GRAND CHEROKEE",
        "makeId": 10
      },
      {
        "id": 107,
        "name": "JEEP PATRIOT",
        "makeId": 10
      },
      {
        "id": 108,
        "name": "JEEP RENEGADE",
        "makeId": 10
      },
      {
        "id": 109,
        "name": "JEEP WRANGLER",
        "makeId": 10
      },
      {
        "id": 110,
        "name": "PT CRUISER",
        "makeId": 10
      },
      {
        "id": 111,
        "name": "VOYAGER",
        "makeId": 10
      },
      {
        "id": 112,
        "name": "YPSILON",
        "makeId": 10
      },
      {
        "id": 113,
        "name": "BERLINGO",
        "makeId": 11
      },
      {
        "id": 114,
        "name": "C3",
        "makeId": 11
      },
      {
        "id": 115,
        "name": "C3 AIRCROSS",
        "makeId": 11
      },
      {
        "id": 116,
        "name": "C4",
        "makeId": 11
      },
      {
        "id": 117,
        "name": "C4 CACTUS",
        "makeId": 11
      },
      {
        "id": 118,
        "name": "C4 PICASSO",
        "makeId": 11
      },
      {
        "id": 119,
        "name": "C5",
        "makeId": 11
      },
      {
        "id": 120,
        "name": "DS3",
        "makeId": 11
      },
      {
        "id": 121,
        "name": "DS3 CROSSBACK",
        "makeId": 11
      },
      {
        "id": 122,
        "name": "DS4",
        "makeId": 11
      },
      {
        "id": 123,
        "name": "DS5",
        "makeId": 11
      },
      {
        "id": 124,
        "name": "GRAND C4 PICASSO",
        "makeId": 11
      },
      {
        "id": 125,
        "name": "XM",
        "makeId": 11
      },
      {
        "id": 126,
        "name": "ALTIS",
        "makeId": 12
      },
      {
        "id": 127,
        "name": "ATRAI",
        "makeId": 12
      },
      {
        "id": 128,
        "name": "ATRAI 7",
        "makeId": 12
      },
      {
        "id": 129,
        "name": "ATRAI SEVEN",
        "makeId": 12
      },
      {
        "id": 130,
        "name": "ATRAI VAN",
        "makeId": 12
      },
      {
        "id": 131,
        "name": "ATRAI WAGON",
        "makeId": 12
      },
      {
        "id": 132,
        "name": "BEGO",
        "makeId": 12
      },
      {
        "id": 133,
        "name": "BOON",
        "makeId": 12
      },
      {
        "id": 134,
        "name": "CAST",
        "makeId": 12
      },
      {
        "id": 135,
        "name": "COO",
        "makeId": 12
      },
      {
        "id": 136,
        "name": "COPEN",
        "makeId": 12
      },
      {
        "id": 137,
        "name": "ESSE",
        "makeId": 12
      },
      {
        "id": 138,
        "name": "HIJET",
        "makeId": 12
      },
      {
        "id": 139,
        "name": "HIJET ?ADDY",
        "makeId": 12
      },
      {
        "id": 140,
        "name": "HIJET CADDIE",
        "makeId": 12
      },
      {
        "id": 141,
        "name": "HIJET TRUCK",
        "makeId": 12
      },
      {
        "id": 142,
        "name": "HIJET TRUCK 4W",
        "makeId": 12
      },
      {
        "id": 143,
        "name": "HIJET VAN",
        "makeId": 12
      },
      {
        "id": 144,
        "name": "LEEZA",
        "makeId": 12
      },
      {
        "id": 145,
        "name": "MAX",
        "makeId": 12
      },
      {
        "id": 146,
        "name": "MEBIUS",
        "makeId": 12
      },
      {
        "id": 147,
        "name": "MIDGET II",
        "makeId": 12
      },
      {
        "id": 148,
        "name": "MIDJET II",
        "makeId": 12
      },
      {
        "id": 149,
        "name": "MIRA",
        "makeId": 12
      },
      {
        "id": 150,
        "name": "MIRA E S",
        "makeId": 12
      },
      {
        "id": 151,
        "name": "MIRA TOCOT",
        "makeId": 12
      },
      {
        "id": 152,
        "name": "MOVE",
        "makeId": 12
      },
      {
        "id": 153,
        "name": "MOVE CANBUS",
        "makeId": 12
      },
      {
        "id": 154,
        "name": "MOVE CONTE",
        "makeId": 12
      },
      {
        "id": 155,
        "name": "MOVE LATTE",
        "makeId": 12
      },
      {
        "id": 156,
        "name": "NAKED",
        "makeId": 12
      },
      {
        "id": 157,
        "name": "OPTI",
        "makeId": 12
      },
      {
        "id": 158,
        "name": "ROCKY",
        "makeId": 12
      },
      {
        "id": 159,
        "name": "SONICA",
        "makeId": 12
      },
      {
        "id": 160,
        "name": "TAFT",
        "makeId": 12
      },
      {
        "id": 161,
        "name": "TANTO",
        "makeId": 12
      },
      {
        "id": 162,
        "name": "TANTO EXE",
        "makeId": 12
      },
      {
        "id": 163,
        "name": "TANTO FUN CROSS",
        "makeId": 12
      },
      {
        "id": 164,
        "name": "TERIOS",
        "makeId": 12
      },
      {
        "id": 165,
        "name": "TERIOS KID",
        "makeId": 12
      },
      {
        "id": 166,
        "name": "THOR",
        "makeId": 12
      },
      {
        "id": 167,
        "name": "WAKE",
        "makeId": 12
      },
      {
        "id": 168,
        "name": "DAIMLER",
        "makeId": 13
      },
      {
        "id": 169,
        "name": "CALIBER",
        "makeId": 14
      },
      {
        "id": 170,
        "name": "CHARGER",
        "makeId": 14
      },
      {
        "id": 171,
        "name": "MAGNUM",
        "makeId": 14
      },
      {
        "id": 172,
        "name": "NITRO",
        "makeId": 14
      },
      {
        "id": 173,
        "name": "OTHER",
        "makeId": 14
      },
      {
        "id": 174,
        "name": "F430",
        "makeId": 15
      },
      {
        "id": 175,
        "name": "500",
        "makeId": 16
      },
      {
        "id": 176,
        "name": "500X",
        "makeId": 16
      },
      {
        "id": 177,
        "name": "ABARTH  595C",
        "makeId": 16
      },
      {
        "id": 178,
        "name": "ABARTH 500C",
        "makeId": 16
      },
      {
        "id": 179,
        "name": "ABARTH 595",
        "makeId": 16
      },
      {
        "id": 180,
        "name": "NEW PANDA",
        "makeId": 16
      },
      {
        "id": 181,
        "name": "OTHER",
        "makeId": 16
      },
      {
        "id": 182,
        "name": "PANDA",
        "makeId": 16
      },
      {
        "id": 183,
        "name": "PUNTO",
        "makeId": 16
      },
      {
        "id": 184,
        "name": "BCVA- NON",
        "makeId": 17
      },
      {
        "id": 185,
        "name": "ESCAPE",
        "makeId": 17
      },
      {
        "id": 186,
        "name": "EXPLORER",
        "makeId": 17
      },
      {
        "id": 187,
        "name": "FESTIVA MINI WAGON",
        "makeId": 17
      },
      {
        "id": 188,
        "name": "FIESTA",
        "makeId": 17
      },
      {
        "id": 189,
        "name": "FO EX PRO 5D4W",
        "makeId": 17
      },
      {
        "id": 190,
        "name": "FO F150",
        "makeId": 17
      },
      {
        "id": 191,
        "name": "FO MUSTANG",
        "makeId": 17
      },
      {
        "id": 192,
        "name": "FRIDA",
        "makeId": 17
      },
      {
        "id": 193,
        "name": "KUGA",
        "makeId": 17
      },
      {
        "id": 194,
        "name": "MUSTANG",
        "makeId": 17
      },
      {
        "id": 195,
        "name": "OTHER",
        "makeId": 17
      },
      {
        "id": 196,
        "name": "CADILLAC ATS",
        "makeId": 18
      },
      {
        "id": 197,
        "name": "CADILLAC CONCOURS",
        "makeId": 18
      },
      {
        "id": 198,
        "name": "BRU RIBBON CITY 2D",
        "makeId": 19
      },
      {
        "id": 199,
        "name": "DUTRO",
        "makeId": 19
      },
      {
        "id": 200,
        "name": "LIESSE",
        "makeId": 19
      },
      {
        "id": 201,
        "name": "OTHER",
        "makeId": 19
      },
      {
        "id": 202,
        "name": "PROFIA",
        "makeId": 19
      },
      {
        "id": 203,
        "name": "RANGER",
        "makeId": 19
      },
      {
        "id": 204,
        "name": "TRUCK",
        "makeId": 19
      },
      {
        "id": 205,
        "name": "OTHER",
        "makeId": 20
      },
      {
        "id": 206,
        "name": "ACCORD",
        "makeId": 21
      },
      {
        "id": 207,
        "name": "ACCORD INSPIRE",
        "makeId": 21
      },
      {
        "id": 208,
        "name": "ACCORD TOURER",
        "makeId": 21
      },
      {
        "id": 209,
        "name": "ACCORD WAGON",
        "makeId": 21
      },
      {
        "id": 210,
        "name": "ACTY",
        "makeId": 21
      },
      {
        "id": 211,
        "name": "ACTY TRUCK",
        "makeId": 21
      },
      {
        "id": 212,
        "name": "ACTY VAN",
        "makeId": 21
      },
      {
        "id": 213,
        "name": "AIRWAVE",
        "makeId": 21
      },
      {
        "id": 214,
        "name": "BEAT",
        "makeId": 21
      },
      {
        "id": 215,
        "name": "CAPA",
        "makeId": 21
      },
      {
        "id": 216,
        "name": "CIVIC",
        "makeId": 21
      },
      {
        "id": 217,
        "name": "CIVIC HYBRID",
        "makeId": 21
      },
      {
        "id": 218,
        "name": "CR-V",
        "makeId": 21
      },
      {
        "id": 219,
        "name": "CR-X",
        "makeId": 21
      },
      {
        "id": 220,
        "name": "CR-Z",
        "makeId": 21
      },
      {
        "id": 221,
        "name": "CROSSROAD",
        "makeId": 21
      },
      {
        "id": 222,
        "name": "EDIX",
        "makeId": 21
      },
      {
        "id": 223,
        "name": "ELEMENT",
        "makeId": 21
      },
      {
        "id": 224,
        "name": "ELYSION",
        "makeId": 21
      },
      {
        "id": 225,
        "name": "FIT",
        "makeId": 21
      },
      {
        "id": 226,
        "name": "FIT SHUTTLE",
        "makeId": 21
      },
      {
        "id": 227,
        "name": "FREED",
        "makeId": 21
      },
      {
        "id": 228,
        "name": "FREED + HYBRID",
        "makeId": 21
      },
      {
        "id": 229,
        "name": "FREED HYBRID",
        "makeId": 21
      },
      {
        "id": 230,
        "name": "FREED SPIKE",
        "makeId": 21
      },
      {
        "id": 231,
        "name": "FREED SPIKE HYBRID",
        "makeId": 21
      },
      {
        "id": 232,
        "name": "FREED SPY KHV",
        "makeId": 21
      },
      {
        "id": 233,
        "name": "GRACE",
        "makeId": 21
      },
      {
        "id": 234,
        "name": "HR-V",
        "makeId": 21
      },
      {
        "id": 235,
        "name": "INSIGHT",
        "makeId": 21
      },
      {
        "id": 236,
        "name": "INSIGHT EXCLUSIVE",
        "makeId": 21
      },
      {
        "id": 237,
        "name": "INSPIRE",
        "makeId": 21
      },
      {
        "id": 238,
        "name": "INTEGRA",
        "makeId": 21
      },
      {
        "id": 239,
        "name": "JADE",
        "makeId": 21
      },
      {
        "id": 240,
        "name": "LAGREAT",
        "makeId": 21
      },
      {
        "id": 241,
        "name": "LEGEND",
        "makeId": 21
      },
      {
        "id": 242,
        "name": "LIFE",
        "makeId": 21
      },
      {
        "id": 243,
        "name": "LIFE DUNK",
        "makeId": 21
      },
      {
        "id": 244,
        "name": "LIFE STEP WAGON",
        "makeId": 21
      },
      {
        "id": 245,
        "name": "LIVE DIVA",
        "makeId": 21
      },
      {
        "id": 246,
        "name": "MDX",
        "makeId": 21
      },
      {
        "id": 247,
        "name": "MOBILIO",
        "makeId": 21
      },
      {
        "id": 248,
        "name": "MOBILIO SPIKE",
        "makeId": 21
      },
      {
        "id": 249,
        "name": "N BOX",
        "makeId": 21
      },
      {
        "id": 250,
        "name": "N BOX CUSTOM",
        "makeId": 21
      },
      {
        "id": 251,
        "name": "N BOX PLUS",
        "makeId": 21
      },
      {
        "id": 252,
        "name": "N BOX SLASH",
        "makeId": 21
      },
      {
        "id": 253,
        "name": "N ONE",
        "makeId": 21
      },
      {
        "id": 254,
        "name": "N VAN",
        "makeId": 21
      },
      {
        "id": 255,
        "name": "N WGN",
        "makeId": 21
      },
      {
        "id": 256,
        "name": "N-VAN",
        "makeId": 21
      },
      {
        "id": 257,
        "name": "NSX",
        "makeId": 21
      },
      {
        "id": 258,
        "name": "ODYSSEY",
        "makeId": 21
      },
      {
        "id": 259,
        "name": "ODYSSEY HYBRID",
        "makeId": 21
      },
      {
        "id": 260,
        "name": "OTHER",
        "makeId": 21
      },
      {
        "id": 261,
        "name": "PARTNER",
        "makeId": 21
      },
      {
        "id": 262,
        "name": "PRELUDE",
        "makeId": 21
      },
      {
        "id": 263,
        "name": "S-MX",
        "makeId": 21
      },
      {
        "id": 264,
        "name": "S2000",
        "makeId": 21
      },
      {
        "id": 265,
        "name": "S660",
        "makeId": 21
      },
      {
        "id": 266,
        "name": "SABER",
        "makeId": 21
      },
      {
        "id": 267,
        "name": "SHUTTLE",
        "makeId": 21
      },
      {
        "id": 268,
        "name": "SPIKE",
        "makeId": 21
      },
      {
        "id": 269,
        "name": "STEP WAGON",
        "makeId": 21
      },
      {
        "id": 270,
        "name": "STREAM",
        "makeId": 21
      },
      {
        "id": 271,
        "name": "THATS",
        "makeId": 21
      },
      {
        "id": 272,
        "name": "TODAY",
        "makeId": 21
      },
      {
        "id": 273,
        "name": "TORNEO",
        "makeId": 21
      },
      {
        "id": 274,
        "name": "VAMOS",
        "makeId": 21
      },
      {
        "id": 275,
        "name": "VAMOS HOBIO P",
        "makeId": 21
      },
      {
        "id": 276,
        "name": "VEZEL",
        "makeId": 21
      },
      {
        "id": 277,
        "name": "Z",
        "makeId": 21
      },
      {
        "id": 278,
        "name": "ZEST",
        "makeId": 21
      },
      {
        "id": 279,
        "name": "ZR-V",
        "makeId": 21
      },
      {
        "id": 280,
        "name": "H2",
        "makeId": 22
      },
      {
        "id": 281,
        "name": "H3",
        "makeId": 22
      },
      {
        "id": 282,
        "name": "IN FINIQX56",
        "makeId": 23
      },
      {
        "id": 283,
        "name": "OTHER",
        "makeId": 24
      },
      {
        "id": 284,
        "name": "117COUPE",
        "makeId": 25
      },
      {
        "id": 285,
        "name": "BIGHORN",
        "makeId": 25
      },
      {
        "id": 286,
        "name": "COMO",
        "makeId": 25
      },
      {
        "id": 287,
        "name": "ELF",
        "makeId": 25
      },
      {
        "id": 288,
        "name": "ELF TRUCK",
        "makeId": 25
      },
      {
        "id": 289,
        "name": "FORWARD",
        "makeId": 25
      },
      {
        "id": 290,
        "name": "FOSTER RODEO",
        "makeId": 25
      },
      {
        "id": 291,
        "name": "GALA MIO",
        "makeId": 25
      },
      {
        "id": 292,
        "name": "GEMINI",
        "makeId": 25
      },
      {
        "id": 293,
        "name": "GIGA",
        "makeId": 25
      },
      {
        "id": 294,
        "name": "GIGA MAX",
        "makeId": 25
      },
      {
        "id": 295,
        "name": "JUSTON",
        "makeId": 25
      },
      {
        "id": 296,
        "name": "OTHER",
        "makeId": 25
      },
      {
        "id": 297,
        "name": "TRUCK",
        "makeId": 25
      },
      {
        "id": 298,
        "name": "WIZARD",
        "makeId": 25
      },
      {
        "id": 299,
        "name": "E-PACE",
        "makeId": 26
      },
      {
        "id": 300,
        "name": "F-PACE",
        "makeId": 26
      },
      {
        "id": 301,
        "name": "F-TYPE",
        "makeId": 26
      },
      {
        "id": 302,
        "name": "I-PACE",
        "makeId": 26
      },
      {
        "id": 303,
        "name": "S TYPE",
        "makeId": 26
      },
      {
        "id": 304,
        "name": "X TYPE",
        "makeId": 26
      },
      {
        "id": 305,
        "name": "XE",
        "makeId": 26
      },
      {
        "id": 306,
        "name": "XF SERIES",
        "makeId": 26
      },
      {
        "id": 307,
        "name": "XJ SERIES",
        "makeId": 26
      },
      {
        "id": 308,
        "name": "XJ6",
        "makeId": 26
      },
      {
        "id": 309,
        "name": "XK SERIES",
        "makeId": 26
      },
      {
        "id": 310,
        "name": "CHEROKEE",
        "makeId": 27
      },
      {
        "id": 311,
        "name": "COMPASS",
        "makeId": 27
      },
      {
        "id": 312,
        "name": "GRAND CHEROKEE",
        "makeId": 27
      },
      {
        "id": 313,
        "name": "PATRIOT",
        "makeId": 27
      },
      {
        "id": 314,
        "name": "RENEGADE",
        "makeId": 27
      },
      {
        "id": 315,
        "name": "WRANGLER",
        "makeId": 27
      },
      {
        "id": 316,
        "name": "WRANGLER UNLIMITED",
        "makeId": 27
      },
      {
        "id": 317,
        "name": "KAVASAKI",
        "makeId": 28
      },
      {
        "id": 318,
        "name": "OTHER",
        "makeId": 28
      },
      {
        "id": 319,
        "name": "OTHER",
        "makeId": 29
      },
      {
        "id": 320,
        "name": "FORKLIFT",
        "makeId": 30
      },
      {
        "id": 321,
        "name": "OTHER",
        "makeId": 30
      },
      {
        "id": 322,
        "name": "OTHER",
        "makeId": 31
      },
      {
        "id": 323,
        "name": "DEFENDER",
        "makeId": 32
      },
      {
        "id": 324,
        "name": "DISCOVERY",
        "makeId": 32
      },
      {
        "id": 325,
        "name": "DISCOVERY 4",
        "makeId": 32
      },
      {
        "id": 326,
        "name": "DISCOVERY SPORT",
        "makeId": 32
      },
      {
        "id": 327,
        "name": "FREELANDER",
        "makeId": 32
      },
      {
        "id": 328,
        "name": "RANGE ROVER",
        "makeId": 32
      },
      {
        "id": 329,
        "name": "RANGE ROVER EVOQUE",
        "makeId": 32
      },
      {
        "id": 330,
        "name": "RANGE ROVER SPORT",
        "makeId": 32
      },
      {
        "id": 331,
        "name": "RANGE ROVER VELAR",
        "makeId": 32
      },
      {
        "id": 332,
        "name": "CT",
        "makeId": 33
      },
      {
        "id": 333,
        "name": "ES",
        "makeId": 33
      },
      {
        "id": 334,
        "name": "GS",
        "makeId": 33
      },
      {
        "id": 335,
        "name": "GS F",
        "makeId": 33
      },
      {
        "id": 336,
        "name": "HS",
        "makeId": 33
      },
      {
        "id": 337,
        "name": "IS",
        "makeId": 33
      },
      {
        "id": 338,
        "name": "IS F",
        "makeId": 33
      },
      {
        "id": 339,
        "name": "LC",
        "makeId": 33
      },
      {
        "id": 340,
        "name": "LS",
        "makeId": 33
      },
      {
        "id": 341,
        "name": "LX",
        "makeId": 33
      },
      {
        "id": 342,
        "name": "NX",
        "makeId": 33
      },
      {
        "id": 343,
        "name": "RC",
        "makeId": 33
      },
      {
        "id": 344,
        "name": "RC F",
        "makeId": 33
      },
      {
        "id": 345,
        "name": "RX",
        "makeId": 33
      },
      {
        "id": 346,
        "name": "SC",
        "makeId": 33
      },
      {
        "id": 347,
        "name": "UX",
        "makeId": 33
      },
      {
        "id": 348,
        "name": "MKX",
        "makeId": 34
      },
      {
        "id": 349,
        "name": "NAVIGATION GE4W",
        "makeId": 34
      },
      {
        "id": 350,
        "name": "NAVIGATOR",
        "makeId": 34
      },
      {
        "id": 351,
        "name": "EVORA",
        "makeId": 35
      },
      {
        "id": 352,
        "name": "GHIBLI",
        "makeId": 36
      },
      {
        "id": 353,
        "name": "GRANCABRIO",
        "makeId": 36
      },
      {
        "id": 354,
        "name": "GTULIZMO",
        "makeId": 36
      },
      {
        "id": 355,
        "name": "LEVANTE",
        "makeId": 36
      },
      {
        "id": 356,
        "name": "QUATTROPORTE",
        "makeId": 36
      },
      {
        "id": 357,
        "name": "6",
        "makeId": 37
      },
      {
        "id": 358,
        "name": "ATENZA",
        "makeId": 37
      },
      {
        "id": 359,
        "name": "ATENZA SEDAN",
        "makeId": 37
      },
      {
        "id": 360,
        "name": "ATENZA SPORT",
        "makeId": 37
      },
      {
        "id": 361,
        "name": "ATENZA WAGON",
        "makeId": 37
      },
      {
        "id": 362,
        "name": "AUTOZAM AZ-1",
        "makeId": 37
      },
      {
        "id": 363,
        "name": "AXELA",
        "makeId": 37
      },
      {
        "id": 364,
        "name": "AZ WAGON",
        "makeId": 37
      },
      {
        "id": 365,
        "name": "AZ-OFFROAD",
        "makeId": 37
      },
      {
        "id": 366,
        "name": "BIANTE",
        "makeId": 37
      },
      {
        "id": 367,
        "name": "BONGO",
        "makeId": 37
      },
      {
        "id": 368,
        "name": "BONGO BRAWNY AFT",
        "makeId": 37
      },
      {
        "id": 369,
        "name": "BONGO BRAWNY TRUCK",
        "makeId": 37
      },
      {
        "id": 370,
        "name": "BONGO BRAWNY VAN",
        "makeId": 37
      },
      {
        "id": 371,
        "name": "BONGO FRIENDEE",
        "makeId": 37
      },
      {
        "id": 372,
        "name": "BONGO VAN",
        "makeId": 37
      },
      {
        "id": 373,
        "name": "BONGO VAN 4D4W",
        "makeId": 37
      },
      {
        "id": 374,
        "name": "BONGO VAN 5D4W",
        "makeId": 37
      },
      {
        "id": 375,
        "name": "CAPELLA",
        "makeId": 37
      },
      {
        "id": 376,
        "name": "CAROL",
        "makeId": 37
      },
      {
        "id": 377,
        "name": "CAROL ECO",
        "makeId": 37
      },
      {
        "id": 378,
        "name": "CLEF",
        "makeId": 37
      },
      {
        "id": 379,
        "name": "CX-3",
        "makeId": 37
      },
      {
        "id": 380,
        "name": "CX-30",
        "makeId": 37
      },
      {
        "id": 381,
        "name": "CX-5",
        "makeId": 37
      },
      {
        "id": 382,
        "name": "CX-60",
        "makeId": 37
      },
      {
        "id": 383,
        "name": "CX-7",
        "makeId": 37
      },
      {
        "id": 384,
        "name": "CX-8",
        "makeId": 37
      },
      {
        "id": 385,
        "name": "DEMIO",
        "makeId": 37
      },
      {
        "id": 386,
        "name": "EFINI RX-7",
        "makeId": 37
      },
      {
        "id": 387,
        "name": "EUNOS ROADSTER",
        "makeId": 37
      },
      {
        "id": 388,
        "name": "FAMILIA",
        "makeId": 37
      },
      {
        "id": 389,
        "name": "FAMILIA S WAGON",
        "makeId": 37
      },
      {
        "id": 390,
        "name": "FAMILIA VAN",
        "makeId": 37
      },
      {
        "id": 391,
        "name": "FLAIR",
        "makeId": 37
      },
      {
        "id": 392,
        "name": "FLAIR CROSSOVER",
        "makeId": 37
      },
      {
        "id": 393,
        "name": "FLAIR WAGON",
        "makeId": 37
      },
      {
        "id": 394,
        "name": "FLAIR WAGON CUSTOM STYLE",
        "makeId": 37
      },
      {
        "id": 395,
        "name": "LAPUTA",
        "makeId": 37
      },
      {
        "id": 396,
        "name": "MAZDA2",
        "makeId": 37
      },
      {
        "id": 397,
        "name": "MAZDA3",
        "makeId": 37
      },
      {
        "id": 398,
        "name": "MAZDA3 FASTBACK",
        "makeId": 37
      },
      {
        "id": 399,
        "name": "MILLENIA",
        "makeId": 37
      },
      {
        "id": 400,
        "name": "MPV",
        "makeId": 37
      },
      {
        "id": 401,
        "name": "MX-30",
        "makeId": 37
      },
      {
        "id": 402,
        "name": "PREMACY",
        "makeId": 37
      },
      {
        "id": 403,
        "name": "PROCEED",
        "makeId": 37
      },
      {
        "id": 404,
        "name": "PROCEED LEVANTE",
        "makeId": 37
      },
      {
        "id": 405,
        "name": "REVUE",
        "makeId": 37
      },
      {
        "id": 406,
        "name": "ROADSTER",
        "makeId": 37
      },
      {
        "id": 407,
        "name": "ROADSTER RF",
        "makeId": 37
      },
      {
        "id": 408,
        "name": "RX-7",
        "makeId": 37
      },
      {
        "id": 409,
        "name": "RX-8",
        "makeId": 37
      },
      {
        "id": 410,
        "name": "SCRUM",
        "makeId": 37
      },
      {
        "id": 411,
        "name": "SCRUM TRUCK",
        "makeId": 37
      },
      {
        "id": 412,
        "name": "SENTIA",
        "makeId": 37
      },
      {
        "id": 413,
        "name": "SPIANO",
        "makeId": 37
      },
      {
        "id": 414,
        "name": "TITAN",
        "makeId": 37
      },
      {
        "id": 415,
        "name": "TRIBUTE",
        "makeId": 37
      },
      {
        "id": 416,
        "name": "VERISA",
        "makeId": 37
      },
      {
        "id": 417,
        "name": "190 SERIES",
        "makeId": 38
      },
      {
        "id": 418,
        "name": "500S",
        "makeId": 38
      },
      {
        "id": 419,
        "name": "A CLASS",
        "makeId": 38
      },
      {
        "id": 420,
        "name": "AMG",
        "makeId": 38
      },
      {
        "id": 421,
        "name": "AMG C",
        "makeId": 38
      },
      {
        "id": 422,
        "name": "AMG C CLASS",
        "makeId": 38
      },
      {
        "id": 423,
        "name": "AMG CLA",
        "makeId": 38
      },
      {
        "id": 424,
        "name": "AMG CLA SHOOTING BRAKE",
        "makeId": 38
      },
      {
        "id": 425,
        "name": "AMG CLS",
        "makeId": 38
      },
      {
        "id": 426,
        "name": "AMG CLS SHOOTING BRAKE",
        "makeId": 38
      },
      {
        "id": 427,
        "name": "AMG GLA",
        "makeId": 38
      },
      {
        "id": 428,
        "name": "AMG GLC",
        "makeId": 38
      },
      {
        "id": 429,
        "name": "AMG GLE",
        "makeId": 38
      },
      {
        "id": 430,
        "name": "AMG S CLASS",
        "makeId": 38
      },
      {
        "id": 431,
        "name": "B CLASS",
        "makeId": 38
      },
      {
        "id": 432,
        "name": "C CLASS",
        "makeId": 38
      },
      {
        "id": 433,
        "name": "C CLASS ALL-TERRAIN",
        "makeId": 38
      },
      {
        "id": 434,
        "name": "C CLASS STATION WAGON",
        "makeId": 38
      },
      {
        "id": 435,
        "name": "C CLASS WAGON",
        "makeId": 38
      },
      {
        "id": 436,
        "name": "CL CLASS",
        "makeId": 38
      },
      {
        "id": 437,
        "name": "CLA CLASS",
        "makeId": 38
      },
      {
        "id": 438,
        "name": "CLA SHOOTING BRAKE",
        "makeId": 38
      },
      {
        "id": 439,
        "name": "CLK CLASS",
        "makeId": 38
      },
      {
        "id": 440,
        "name": "CLS",
        "makeId": 38
      },
      {
        "id": 441,
        "name": "CLS CLASS",
        "makeId": 38
      },
      {
        "id": 442,
        "name": "E CLASS",
        "makeId": 38
      },
      {
        "id": 443,
        "name": "E CLASS STATION WAGON",
        "makeId": 38
      },
      {
        "id": 444,
        "name": "E CLASS WAGON",
        "makeId": 38
      },
      {
        "id": 445,
        "name": "EQA",
        "makeId": 38
      },
      {
        "id": 446,
        "name": "EQB",
        "makeId": 38
      },
      {
        "id": 447,
        "name": "EQE",
        "makeId": 38
      },
      {
        "id": 448,
        "name": "G CLASS",
        "makeId": 38
      },
      {
        "id": 449,
        "name": "GL CLASS",
        "makeId": 38
      },
      {
        "id": 450,
        "name": "GLA CLASS",
        "makeId": 38
      },
      {
        "id": 451,
        "name": "GLB",
        "makeId": 38
      },
      {
        "id": 452,
        "name": "GLC CLASS",
        "makeId": 38
      },
      {
        "id": 453,
        "name": "GLE CLASS",
        "makeId": 38
      },
      {
        "id": 454,
        "name": "GLK CLASS",
        "makeId": 38
      },
      {
        "id": 455,
        "name": "GLS",
        "makeId": 38
      },
      {
        "id": 456,
        "name": "M CLASS",
        "makeId": 38
      },
      {
        "id": 457,
        "name": "MB GELAENDE 5D4W",
        "makeId": 38
      },
      {
        "id": 458,
        "name": "MEDIUM CLASS",
        "makeId": 38
      },
      {
        "id": 459,
        "name": "ML CLASS",
        "makeId": 38
      },
      {
        "id": 460,
        "name": "R CLASS",
        "makeId": 38
      },
      {
        "id": 461,
        "name": "S CLASS",
        "makeId": 38
      },
      {
        "id": 462,
        "name": "SL SERIES",
        "makeId": 38
      },
      {
        "id": 463,
        "name": "SLK CLASS",
        "makeId": 38
      },
      {
        "id": 464,
        "name": "V CLASS",
        "makeId": 38
      },
      {
        "id": 465,
        "name": "VIANO",
        "makeId": 38
      },
      {
        "id": 466,
        "name": "OTHER",
        "makeId": 39
      },
      {
        "id": 467,
        "name": "AERO MIDI 1D",
        "makeId": 40
      },
      {
        "id": 468,
        "name": "AIRTREK",
        "makeId": 40
      },
      {
        "id": 469,
        "name": "BUS",
        "makeId": 40
      },
      {
        "id": 470,
        "name": "CANTER",
        "makeId": 40
      },
      {
        "id": 471,
        "name": "CHARIOT GRANDIS",
        "makeId": 40
      },
      {
        "id": 472,
        "name": "COLT",
        "makeId": 40
      },
      {
        "id": 473,
        "name": "COLT PLUS",
        "makeId": 40
      },
      {
        "id": 474,
        "name": "DELICA",
        "makeId": 40
      },
      {
        "id": 475,
        "name": "DELICA D2",
        "makeId": 40
      },
      {
        "id": 476,
        "name": "DELICA D5",
        "makeId": 40
      },
      {
        "id": 477,
        "name": "DELICA TRUCK",
        "makeId": 40
      },
      {
        "id": 478,
        "name": "DELICA WAGON",
        "makeId": 40
      },
      {
        "id": 479,
        "name": "DIAMANTE",
        "makeId": 40
      },
      {
        "id": 480,
        "name": "DINGO",
        "makeId": 40
      },
      {
        "id": 481,
        "name": "DION",
        "makeId": 40
      },
      {
        "id": 482,
        "name": "ECLIPSE",
        "makeId": 40
      },
      {
        "id": 483,
        "name": "ECLIPSE CROSS",
        "makeId": 40
      },
      {
        "id": 484,
        "name": "ECLIPSE SPIDER",
        "makeId": 40
      },
      {
        "id": 485,
        "name": "EK ACTIVE",
        "makeId": 40
      },
      {
        "id": 486,
        "name": "EK CUSTOM",
        "makeId": 40
      },
      {
        "id": 487,
        "name": "EK SPACE",
        "makeId": 40
      },
      {
        "id": 488,
        "name": "EK SPACE CUSTOM",
        "makeId": 40
      },
      {
        "id": 489,
        "name": "EK SPORTS",
        "makeId": 40
      },
      {
        "id": 490,
        "name": "EK WAGON",
        "makeId": 40
      },
      {
        "id": 491,
        "name": "EK X",
        "makeId": 40
      },
      {
        "id": 492,
        "name": "EK X SPACE",
        "makeId": 40
      },
      {
        "id": 493,
        "name": "FORKLIFT",
        "makeId": 40
      },
      {
        "id": 494,
        "name": "FTO",
        "makeId": 40
      },
      {
        "id": 495,
        "name": "FUSO",
        "makeId": 40
      },
      {
        "id": 496,
        "name": "FUSO CANTER GUTS",
        "makeId": 40
      },
      {
        "id": 497,
        "name": "FUSO FIGHTER",
        "makeId": 40
      },
      {
        "id": 498,
        "name": "FUSO FIGHTER MIGNON",
        "makeId": 40
      },
      {
        "id": 499,
        "name": "FUSO ROSA",
        "makeId": 40
      },
      {
        "id": 500,
        "name": "FUSO SUPER GREAT",
        "makeId": 40
      },
      {
        "id": 501,
        "name": "FUSO THE GREAT",
        "makeId": 40
      },
      {
        "id": 502,
        "name": "FUSO TRUCK",
        "makeId": 40
      },
      {
        "id": 503,
        "name": "GALANT",
        "makeId": 40
      },
      {
        "id": 504,
        "name": "GALANT FORTIS",
        "makeId": 40
      },
      {
        "id": 505,
        "name": "GALANT FORTIS SPORTBACK",
        "makeId": 40
      },
      {
        "id": 506,
        "name": "GRANDIS",
        "makeId": 40
      },
      {
        "id": 507,
        "name": "GTO",
        "makeId": 40
      },
      {
        "id": 508,
        "name": "I",
        "makeId": 40
      },
      {
        "id": 509,
        "name": "I-MIEV",
        "makeId": 40
      },
      {
        "id": 510,
        "name": "JEEP",
        "makeId": 40
      },
      {
        "id": 511,
        "name": "LANCER",
        "makeId": 40
      },
      {
        "id": 512,
        "name": "LANCER VAN",
        "makeId": 40
      },
      {
        "id": 513,
        "name": "MINICA",
        "makeId": 40
      },
      {
        "id": 514,
        "name": "MINICAB",
        "makeId": 40
      },
      {
        "id": 515,
        "name": "MINICAB MIEV",
        "makeId": 40
      },
      {
        "id": 516,
        "name": "MINICAB TRUCK",
        "makeId": 40
      },
      {
        "id": 517,
        "name": "MINICAB VAN",
        "makeId": 40
      },
      {
        "id": 518,
        "name": "MINIKA TOPPO VAN",
        "makeId": 40
      },
      {
        "id": 519,
        "name": "MIRAGE",
        "makeId": 40
      },
      {
        "id": 520,
        "name": "OTHER",
        "makeId": 40
      },
      {
        "id": 521,
        "name": "OUTLANDER",
        "makeId": 40
      },
      {
        "id": 522,
        "name": "OUTLANDER PHEV",
        "makeId": 40
      },
      {
        "id": 523,
        "name": "PAJERO",
        "makeId": 40
      },
      {
        "id": 524,
        "name": "PAJERO IO",
        "makeId": 40
      },
      {
        "id": 525,
        "name": "PAJERO MINI",
        "makeId": 40
      },
      {
        "id": 526,
        "name": "PROUDIA",
        "makeId": 40
      },
      {
        "id": 527,
        "name": "ROSA",
        "makeId": 40
      },
      {
        "id": 528,
        "name": "RVR",
        "makeId": 40
      },
      {
        "id": 529,
        "name": "STARION",
        "makeId": 40
      },
      {
        "id": 530,
        "name": "SUPER GREAT",
        "makeId": 40
      },
      {
        "id": 531,
        "name": "TOPPO",
        "makeId": 40
      },
      {
        "id": 532,
        "name": "TOPPO BJ",
        "makeId": 40
      },
      {
        "id": 533,
        "name": "TOWNBOX",
        "makeId": 40
      },
      {
        "id": 534,
        "name": "TRITON",
        "makeId": 40
      },
      {
        "id": 535,
        "name": "GALUE",
        "makeId": 41
      },
      {
        "id": 536,
        "name": "GALUE-2",
        "makeId": 41
      },
      {
        "id": 537,
        "name": "NOUERA",
        "makeId": 41
      },
      {
        "id": 538,
        "name": "RYOGA",
        "makeId": 41
      },
      {
        "id": 539,
        "name": "VIEWT",
        "makeId": 41
      },
      {
        "id": 540,
        "name": "180 SX",
        "makeId": 42
      },
      {
        "id": 541,
        "name": "AD",
        "makeId": 42
      },
      {
        "id": 542,
        "name": "ARIYA",
        "makeId": 42
      },
      {
        "id": 543,
        "name": "ATLAS",
        "makeId": 42
      },
      {
        "id": 544,
        "name": "AURA",
        "makeId": 42
      },
      {
        "id": 545,
        "name": "AVENIR",
        "makeId": 42
      },
      {
        "id": 546,
        "name": "AVENIR CARGO",
        "makeId": 42
      },
      {
        "id": 547,
        "name": "BE-1",
        "makeId": 42
      },
      {
        "id": 548,
        "name": "BLUEBIRD",
        "makeId": 42
      },
      {
        "id": 549,
        "name": "BLUEBIRD SYLPHY",
        "makeId": 42
      },
      {
        "id": 550,
        "name": "BUS",
        "makeId": 42
      },
      {
        "id": 551,
        "name": "CARAVAN",
        "makeId": 42
      },
      {
        "id": 552,
        "name": "CARAVAN BUS",
        "makeId": 42
      },
      {
        "id": 553,
        "name": "CARAVAN VAN",
        "makeId": 42
      },
      {
        "id": 554,
        "name": "CARAVAN WAGON",
        "makeId": 42
      },
      {
        "id": 555,
        "name": "CEDRIC",
        "makeId": 42
      },
      {
        "id": 556,
        "name": "CEFIRO",
        "makeId": 42
      },
      {
        "id": 557,
        "name": "CIMA",
        "makeId": 42
      },
      {
        "id": 558,
        "name": "CIVILIAN",
        "makeId": 42
      },
      {
        "id": 559,
        "name": "CLIPPER RIO",
        "makeId": 42
      },
      {
        "id": 560,
        "name": "CLIPPER TRUCK",
        "makeId": 42
      },
      {
        "id": 561,
        "name": "CLIPPER VAN",
        "makeId": 42
      },
      {
        "id": 562,
        "name": "CONDOR",
        "makeId": 42
      },
      {
        "id": 563,
        "name": "CUBE",
        "makeId": 42
      },
      {
        "id": 564,
        "name": "CUBECUBIC",
        "makeId": 42
      },
      {
        "id": 565,
        "name": "DATSUN",
        "makeId": 42
      },
      {
        "id": 566,
        "name": "DATSUN FAIRLADY",
        "makeId": 42
      },
      {
        "id": 567,
        "name": "DATSUN PICKUP",
        "makeId": 42
      },
      {
        "id": 568,
        "name": "DAYZ",
        "makeId": 42
      },
      {
        "id": 569,
        "name": "DAYZ ROOX",
        "makeId": 42
      },
      {
        "id": 570,
        "name": "DUALIS",
        "makeId": 42
      },
      {
        "id": 571,
        "name": "E-NV200 VAN",
        "makeId": 42
      },
      {
        "id": 572,
        "name": "E-NV200 WAGON",
        "makeId": 42
      },
      {
        "id": 573,
        "name": "ELGRAND",
        "makeId": 42
      },
      {
        "id": 574,
        "name": "EXPERT",
        "makeId": 42
      },
      {
        "id": 575,
        "name": "FAIRLADYZ",
        "makeId": 42
      },
      {
        "id": 576,
        "name": "FIGARO",
        "makeId": 42
      },
      {
        "id": 577,
        "name": "FORKLIFT",
        "makeId": 42
      },
      {
        "id": 578,
        "name": "FUGA",
        "makeId": 42
      },
      {
        "id": 579,
        "name": "FUGA HYBRID",
        "makeId": 42
      },
      {
        "id": 580,
        "name": "GLORIA",
        "makeId": 42
      },
      {
        "id": 581,
        "name": "GLORIA WAGON",
        "makeId": 42
      },
      {
        "id": 582,
        "name": "GT-R",
        "makeId": 42
      },
      {
        "id": 583,
        "name": "JUKE",
        "makeId": 42
      },
      {
        "id": 584,
        "name": "KA Z",
        "makeId": 42
      },
      {
        "id": 585,
        "name": "KICKS",
        "makeId": 42
      },
      {
        "id": 586,
        "name": "KIX",
        "makeId": 42
      },
      {
        "id": 587,
        "name": "LAFESTA",
        "makeId": 42
      },
      {
        "id": 588,
        "name": "LARGO",
        "makeId": 42
      },
      {
        "id": 589,
        "name": "LATIO",
        "makeId": 42
      },
      {
        "id": 590,
        "name": "LAUREL",
        "makeId": 42
      },
      {
        "id": 591,
        "name": "LEAF",
        "makeId": 42
      },
      {
        "id": 592,
        "name": "LEOPARD",
        "makeId": 42
      },
      {
        "id": 593,
        "name": "LIBERTY",
        "makeId": 42
      },
      {
        "id": 594,
        "name": "MARCH",
        "makeId": 42
      },
      {
        "id": 595,
        "name": "MAXIMA",
        "makeId": 42
      },
      {
        "id": 596,
        "name": "MICRA",
        "makeId": 42
      },
      {
        "id": 597,
        "name": "MOCO",
        "makeId": 42
      },
      {
        "id": 598,
        "name": "MURANO",
        "makeId": 42
      },
      {
        "id": 599,
        "name": "NOTE",
        "makeId": 42
      },
      {
        "id": 600,
        "name": "NT100 CLIPPER",
        "makeId": 42
      },
      {
        "id": 601,
        "name": "NV CARAVAN MIKE B",
        "makeId": 42
      },
      {
        "id": 602,
        "name": "NV100 CLIPPER",
        "makeId": 42
      },
      {
        "id": 603,
        "name": "NV150 AD",
        "makeId": 42
      },
      {
        "id": 604,
        "name": "NV200",
        "makeId": 42
      },
      {
        "id": 605,
        "name": "NV350 CARAVAN",
        "makeId": 42
      },
      {
        "id": 606,
        "name": "OTHER",
        "makeId": 42
      },
      {
        "id": 607,
        "name": "OTTI",
        "makeId": 42
      },
      {
        "id": 608,
        "name": "PAO",
        "makeId": 42
      },
      {
        "id": 609,
        "name": "PINO",
        "makeId": 42
      },
      {
        "id": 610,
        "name": "PRESAGE",
        "makeId": 42
      },
      {
        "id": 611,
        "name": "PRESIDENT",
        "makeId": 42
      },
      {
        "id": 612,
        "name": "PRIMERA",
        "makeId": 42
      },
      {
        "id": 613,
        "name": "PRIMERA WAGON",
        "makeId": 42
      },
      {
        "id": 614,
        "name": "PULSAR",
        "makeId": 42
      },
      {
        "id": 615,
        "name": "QUON",
        "makeId": 42
      },
      {
        "id": 616,
        "name": "RASHEEN",
        "makeId": 42
      },
      {
        "id": 617,
        "name": "ROOX",
        "makeId": 42
      },
      {
        "id": 618,
        "name": "SAFARI",
        "makeId": 42
      },
      {
        "id": 619,
        "name": "SAKURA",
        "makeId": 42
      },
      {
        "id": 620,
        "name": "SERENA",
        "makeId": 42
      },
      {
        "id": 621,
        "name": "SERENA CARGO",
        "makeId": 42
      },
      {
        "id": 622,
        "name": "SILVIA",
        "makeId": 42
      },
      {
        "id": 623,
        "name": "SKYLINE",
        "makeId": 42
      },
      {
        "id": 624,
        "name": "SKYLINE CROSSOVER",
        "makeId": 42
      },
      {
        "id": 625,
        "name": "STAGEA",
        "makeId": 42
      },
      {
        "id": 626,
        "name": "SUNNY",
        "makeId": 42
      },
      {
        "id": 627,
        "name": "SUNNY TRUCK",
        "makeId": 42
      },
      {
        "id": 628,
        "name": "SYLPHY",
        "makeId": 42
      },
      {
        "id": 629,
        "name": "TEANA",
        "makeId": 42
      },
      {
        "id": 630,
        "name": "TERRANO",
        "makeId": 42
      },
      {
        "id": 631,
        "name": "TERRANO REGULUS",
        "makeId": 42
      },
      {
        "id": 632,
        "name": "TIIDA",
        "makeId": 42
      },
      {
        "id": 633,
        "name": "TIIDA LATIO",
        "makeId": 42
      },
      {
        "id": 634,
        "name": "TINO",
        "makeId": 42
      },
      {
        "id": 635,
        "name": "UD SERIES",
        "makeId": 42
      },
      {
        "id": 636,
        "name": "VANETTE",
        "makeId": 42
      },
      {
        "id": 637,
        "name": "VANETTE TRUCK",
        "makeId": 42
      },
      {
        "id": 638,
        "name": "VANETTE VAN",
        "makeId": 42
      },
      {
        "id": 639,
        "name": "WINGROAD",
        "makeId": 42
      },
      {
        "id": 640,
        "name": "X-TRAIL",
        "makeId": 42
      },
      {
        "id": 641,
        "name": "VECTRA",
        "makeId": 43
      },
      {
        "id": 642,
        "name": "A TEC S",
        "makeId": 44
      },
      {
        "id": 643,
        "name": "ACROSS",
        "makeId": 44
      },
      {
        "id": 644,
        "name": "AIRMAN",
        "makeId": 44
      },
      {
        "id": 645,
        "name": "BIG THUMB",
        "makeId": 44
      },
      {
        "id": 646,
        "name": "CAT",
        "makeId": 44
      },
      {
        "id": 647,
        "name": "CHIK ACID YELLOW .NIKOM",
        "makeId": 44
      },
      {
        "id": 648,
        "name": "COMS",
        "makeId": 44
      },
      {
        "id": 649,
        "name": "CONDOR",
        "makeId": 44
      },
      {
        "id": 650,
        "name": "DENYO",
        "makeId": 44
      },
      {
        "id": 651,
        "name": "DS DS9",
        "makeId": 44
      },
      {
        "id": 652,
        "name": "EARTH FUJI",
        "makeId": 44
      },
      {
        "id": 653,
        "name": "FORKLIFT",
        "makeId": 44
      },
      {
        "id": 654,
        "name": "FORZA Z",
        "makeId": 44
      },
      {
        "id": 655,
        "name": "FRUEHAUF",
        "makeId": 44
      },
      {
        "id": 656,
        "name": "FURUKAWA",
        "makeId": 44
      },
      {
        "id": 657,
        "name": "GT",
        "makeId": 44
      },
      {
        "id": 658,
        "name": "HITACHI",
        "makeId": 44
      },
      {
        "id": 659,
        "name": "HO KSIN",
        "makeId": 44
      },
      {
        "id": 660,
        "name": "IHI",
        "makeId": 44
      },
      {
        "id": 661,
        "name": "IIDA",
        "makeId": 44
      },
      {
        "id": 662,
        "name": "IYASAKA",
        "makeId": 44
      },
      {
        "id": 663,
        "name": "JUUKOU",
        "makeId": 44
      },
      {
        "id": 664,
        "name": "KAMA-A GRIS",
        "makeId": 44
      },
      {
        "id": 665,
        "name": "KATO",
        "makeId": 44
      },
      {
        "id": 666,
        "name": "LAPI-DO",
        "makeId": 44
      },
      {
        "id": 667,
        "name": "LEATHER JUU TICKET YELLOW",
        "makeId": 44
      },
      {
        "id": 668,
        "name": "LITTLE WA",
        "makeId": 44
      },
      {
        "id": 669,
        "name": "MEAMG A",
        "makeId": 44
      },
      {
        "id": 670,
        "name": "MEAMG C STEREO",
        "makeId": 44
      },
      {
        "id": 671,
        "name": "MEAMG CLA",
        "makeId": 44
      },
      {
        "id": 672,
        "name": "MEAMG CLS",
        "makeId": 44
      },
      {
        "id": 673,
        "name": "MEAMG E STEREO",
        "makeId": 44
      },
      {
        "id": 674,
        "name": "MEAMG G CLASS",
        "makeId": 44
      },
      {
        "id": 675,
        "name": "MEAMG GLB",
        "makeId": 44
      },
      {
        "id": 676,
        "name": "MEAMG GT",
        "makeId": 44
      },
      {
        "id": 677,
        "name": "MEAMG GT 5D4W",
        "makeId": 44
      },
      {
        "id": 678,
        "name": "MEAMG SL",
        "makeId": 44
      },
      {
        "id": 679,
        "name": "MP LAFER",
        "makeId": 44
      },
      {
        "id": 680,
        "name": "NI SEAT",
        "makeId": 44
      },
      {
        "id": 681,
        "name": "NICHIYU",
        "makeId": 44
      },
      {
        "id": 682,
        "name": "NICHIYU FORKLIFT",
        "makeId": 44
      },
      {
        "id": 683,
        "name": "NUM SEI",
        "makeId": 44
      },
      {
        "id": 684,
        "name": "OTHER",
        "makeId": 44
      },
      {
        "id": 685,
        "name": "OTHER JAPAN",
        "makeId": 44
      },
      {
        "id": 686,
        "name": "QUON",
        "makeId": 44
      },
      {
        "id": 687,
        "name": "SAKAI",
        "makeId": 44
      },
      {
        "id": 688,
        "name": "SEA DOO",
        "makeId": 44
      },
      {
        "id": 689,
        "name": "SHIBAURA",
        "makeId": 44
      },
      {
        "id": 690,
        "name": "SHOVEL",
        "makeId": 44
      },
      {
        "id": 691,
        "name": "SIMEIWA",
        "makeId": 44
      },
      {
        "id": 692,
        "name": "SNOW LA",
        "makeId": 44
      },
      {
        "id": 693,
        "name": "STARLEVER",
        "makeId": 44
      },
      {
        "id": 694,
        "name": "SUMITOMO",
        "makeId": 44
      },
      {
        "id": 695,
        "name": "TANKER",
        "makeId": 44
      },
      {
        "id": 696,
        "name": "TIRE CHANGER",
        "makeId": 44
      },
      {
        "id": 697,
        "name": "TOKYU",
        "makeId": 44
      },
      {
        "id": 698,
        "name": "TRAILMOBILE",
        "makeId": 44
      },
      {
        "id": 699,
        "name": "TRANSPORT",
        "makeId": 44
      },
      {
        "id": 700,
        "name": "TREX",
        "makeId": 44
      },
      {
        "id": 701,
        "name": "UD",
        "makeId": 44
      },
      {
        "id": 702,
        "name": "UNIQUE",
        "makeId": 44
      },
      {
        "id": 703,
        "name": "WA CAR",
        "makeId": 44
      },
      {
        "id": 704,
        "name": "WADO",
        "makeId": 44
      },
      {
        "id": 705,
        "name": "YUNI CARRIER",
        "makeId": 44
      },
      {
        "id": 706,
        "name": "ZENOAH",
        "makeId": 44
      },
      {
        "id": 707,
        "name": "2008",
        "makeId": 45
      },
      {
        "id": 708,
        "name": "206",
        "makeId": 45
      },
      {
        "id": 709,
        "name": "207",
        "makeId": 45
      },
      {
        "id": 710,
        "name": "208",
        "makeId": 45
      },
      {
        "id": 711,
        "name": "3008",
        "makeId": 45
      },
      {
        "id": 712,
        "name": "306",
        "makeId": 45
      },
      {
        "id": 713,
        "name": "307",
        "makeId": 45
      },
      {
        "id": 714,
        "name": "308",
        "makeId": 45
      },
      {
        "id": 715,
        "name": "406",
        "makeId": 45
      },
      {
        "id": 716,
        "name": "5008",
        "makeId": 45
      },
      {
        "id": 717,
        "name": "508",
        "makeId": 45
      },
      {
        "id": 718,
        "name": "E-2008",
        "makeId": 45
      },
      {
        "id": 719,
        "name": "RCZ",
        "makeId": 45
      },
      {
        "id": 720,
        "name": "911",
        "makeId": 46
      },
      {
        "id": 721,
        "name": "944",
        "makeId": 46
      },
      {
        "id": 722,
        "name": "CAYENNE",
        "makeId": 46
      },
      {
        "id": 723,
        "name": "CAYENNE 4W",
        "makeId": 46
      },
      {
        "id": 724,
        "name": "CAYMAN",
        "makeId": 46
      },
      {
        "id": 725,
        "name": "KAI KPE4W",
        "makeId": 46
      },
      {
        "id": 726,
        "name": "MACAN",
        "makeId": 46
      },
      {
        "id": 727,
        "name": "PANAMERA",
        "makeId": 46
      },
      {
        "id": 728,
        "name": "PORU PANASONIC",
        "makeId": 46
      },
      {
        "id": 729,
        "name": "PORU PANASONIC ME5D4W",
        "makeId": 46
      },
      {
        "id": 730,
        "name": "KANGOO",
        "makeId": 47
      },
      {
        "id": 731,
        "name": "LUTECIA",
        "makeId": 47
      },
      {
        "id": 732,
        "name": "MEGANE",
        "makeId": 47
      },
      {
        "id": 733,
        "name": "MEGANE ESTATE",
        "makeId": 47
      },
      {
        "id": 734,
        "name": "TWINGO",
        "makeId": 47
      },
      {
        "id": 735,
        "name": "DISCOVERY",
        "makeId": 48
      },
      {
        "id": 736,
        "name": "FREELANDER2",
        "makeId": 48
      },
      {
        "id": 737,
        "name": "MINI",
        "makeId": 48
      },
      {
        "id": 738,
        "name": "RANGE ROVER",
        "makeId": 48
      },
      {
        "id": 739,
        "name": "9-3 SERIES",
        "makeId": 49
      },
      {
        "id": 740,
        "name": "FORFOUR",
        "makeId": 50
      },
      {
        "id": 741,
        "name": "FORTWO",
        "makeId": 50
      },
      {
        "id": 742,
        "name": "FORTWO CABRIO",
        "makeId": 50
      },
      {
        "id": 743,
        "name": "FOURFOUR",
        "makeId": 50
      },
      {
        "id": 744,
        "name": "OTHER",
        "makeId": 50
      },
      {
        "id": 745,
        "name": "BRZ",
        "makeId": 51
      },
      {
        "id": 746,
        "name": "CHIFFON",
        "makeId": 51
      },
      {
        "id": 747,
        "name": "CROSSTREK",
        "makeId": 51
      },
      {
        "id": 748,
        "name": "DEX",
        "makeId": 51
      },
      {
        "id": 749,
        "name": "DIAS WAGON",
        "makeId": 51
      },
      {
        "id": 750,
        "name": "DOMINGO",
        "makeId": 51
      },
      {
        "id": 751,
        "name": "ELF",
        "makeId": 51
      },
      {
        "id": 752,
        "name": "EXIGA",
        "makeId": 51
      },
      {
        "id": 753,
        "name": "EXIGA CROSSOVER 7",
        "makeId": 51
      },
      {
        "id": 754,
        "name": "FORESTER",
        "makeId": 51
      },
      {
        "id": 755,
        "name": "GOLF ALLTRACK",
        "makeId": 51
      },
      {
        "id": 756,
        "name": "IMPREZA",
        "makeId": 51
      },
      {
        "id": 757,
        "name": "IMPREZA ANESIS",
        "makeId": 51
      },
      {
        "id": 758,
        "name": "IMPREZA G4",
        "makeId": 51
      },
      {
        "id": 759,
        "name": "IMPREZA SPOILER 4W",
        "makeId": 51
      },
      {
        "id": 760,
        "name": "IMPREZA SPORT",
        "makeId": 51
      },
      {
        "id": 761,
        "name": "IMPREZA SPORT WAGON",
        "makeId": 51
      },
      {
        "id": 762,
        "name": "IMPREZA WRX4W",
        "makeId": 51
      },
      {
        "id": 763,
        "name": "JUSTY",
        "makeId": 51
      },
      {
        "id": 764,
        "name": "LEGACY",
        "makeId": 51
      },
      {
        "id": 765,
        "name": "LEGACY B4",
        "makeId": 51
      },
      {
        "id": 766,
        "name": "LEGACY OUTBACK",
        "makeId": 51
      },
      {
        "id": 767,
        "name": "LEVORG",
        "makeId": 51
      },
      {
        "id": 768,
        "name": "LUCRA",
        "makeId": 51
      },
      {
        "id": 769,
        "name": "OUTBACK",
        "makeId": 51
      },
      {
        "id": 770,
        "name": "PLEO",
        "makeId": 51
      },
      {
        "id": 771,
        "name": "PLEO PLUS",
        "makeId": 51
      },
      {
        "id": 772,
        "name": "R1",
        "makeId": 51
      },
      {
        "id": 773,
        "name": "R2",
        "makeId": 51
      },
      {
        "id": 774,
        "name": "SAMBAR",
        "makeId": 51
      },
      {
        "id": 775,
        "name": "STELLA",
        "makeId": 51
      },
      {
        "id": 776,
        "name": "TRAVIQ",
        "makeId": 51
      },
      {
        "id": 777,
        "name": "TREZIA",
        "makeId": 51
      },
      {
        "id": 778,
        "name": "VIVIO",
        "makeId": 51
      },
      {
        "id": 779,
        "name": "WRX",
        "makeId": 51
      },
      {
        "id": 780,
        "name": "WRX S4",
        "makeId": 51
      },
      {
        "id": 781,
        "name": "WRX STI",
        "makeId": 51
      },
      {
        "id": 782,
        "name": "XV",
        "makeId": 51
      },
      {
        "id": 783,
        "name": "FORKLIFT",
        "makeId": 52
      },
      {
        "id": 784,
        "name": "AERIO SEDAN",
        "makeId": 53
      },
      {
        "id": 785,
        "name": "ALTO",
        "makeId": 53
      },
      {
        "id": 786,
        "name": "ALTO ECO",
        "makeId": 53
      },
      {
        "id": 787,
        "name": "ALTO HUSTLE",
        "makeId": 53
      },
      {
        "id": 788,
        "name": "ALTO LAPIN",
        "makeId": 53
      },
      {
        "id": 789,
        "name": "ALTO LAPIN CHOCOLATE",
        "makeId": 53
      },
      {
        "id": 790,
        "name": "ALTO TURBO RS",
        "makeId": 53
      },
      {
        "id": 791,
        "name": "ALTO VAN",
        "makeId": 53
      },
      {
        "id": 792,
        "name": "BALENO",
        "makeId": 53
      },
      {
        "id": 793,
        "name": "CAPPUCCINO",
        "makeId": 53
      },
      {
        "id": 794,
        "name": "CAPTINO",
        "makeId": 53
      },
      {
        "id": 795,
        "name": "CARRY",
        "makeId": 53
      },
      {
        "id": 796,
        "name": "CARRY TRUCK",
        "makeId": 53
      },
      {
        "id": 797,
        "name": "CARRY VAN",
        "makeId": 53
      },
      {
        "id": 798,
        "name": "CERVO",
        "makeId": 53
      },
      {
        "id": 799,
        "name": "CERVO CLASSIC",
        "makeId": 53
      },
      {
        "id": 800,
        "name": "CHEVROLET CRUZE",
        "makeId": 53
      },
      {
        "id": 801,
        "name": "CHEVROLET MW",
        "makeId": 53
      },
      {
        "id": 802,
        "name": "ESCUDO",
        "makeId": 53
      },
      {
        "id": 803,
        "name": "EVERY",
        "makeId": 53
      },
      {
        "id": 804,
        "name": "EVERY VAN",
        "makeId": 53
      },
      {
        "id": 805,
        "name": "EVERY WAGON",
        "makeId": 53
      },
      {
        "id": 806,
        "name": "FRONTE",
        "makeId": 53
      },
      {
        "id": 807,
        "name": "HUSTLER",
        "makeId": 53
      },
      {
        "id": 808,
        "name": "IGNIS",
        "makeId": 53
      },
      {
        "id": 809,
        "name": "JIMNY",
        "makeId": 53
      },
      {
        "id": 810,
        "name": "JIMNY SIERRA",
        "makeId": 53
      },
      {
        "id": 811,
        "name": "JIMNY WIDE",
        "makeId": 53
      },
      {
        "id": 812,
        "name": "KEI",
        "makeId": 53
      },
      {
        "id": 813,
        "name": "KIZASHI",
        "makeId": 53
      },
      {
        "id": 814,
        "name": "LANDY",
        "makeId": 53
      },
      {
        "id": 815,
        "name": "LAPIN LC",
        "makeId": 53
      },
      {
        "id": 816,
        "name": "MRWAGON",
        "makeId": 53
      },
      {
        "id": 817,
        "name": "MW",
        "makeId": 53
      },
      {
        "id": 818,
        "name": "OTHER",
        "makeId": 53
      },
      {
        "id": 819,
        "name": "PALETTE",
        "makeId": 53
      },
      {
        "id": 820,
        "name": "SOLIO",
        "makeId": 53
      },
      {
        "id": 821,
        "name": "SOLIO BANDIT",
        "makeId": 53
      },
      {
        "id": 822,
        "name": "SPACIA",
        "makeId": 53
      },
      {
        "id": 823,
        "name": "SPACIA BACE",
        "makeId": 53
      },
      {
        "id": 824,
        "name": "SPACIA BASE",
        "makeId": 53
      },
      {
        "id": 825,
        "name": "SPACIA CUSTOM",
        "makeId": 53
      },
      {
        "id": 826,
        "name": "SPACIA GEAR",
        "makeId": 53
      },
      {
        "id": 827,
        "name": "SPLASH",
        "makeId": 53
      },
      {
        "id": 828,
        "name": "SWIFT",
        "makeId": 53
      },
      {
        "id": 829,
        "name": "SWIFT SPORTS",
        "makeId": 53
      },
      {
        "id": 830,
        "name": "SX-4",
        "makeId": 53
      },
      {
        "id": 831,
        "name": "SX4 S-CROSS",
        "makeId": 53
      },
      {
        "id": 832,
        "name": "TWIN",
        "makeId": 53
      },
      {
        "id": 833,
        "name": "WAGON R",
        "makeId": 53
      },
      {
        "id": 834,
        "name": "WAGON R CUSTOM Z",
        "makeId": 53
      },
      {
        "id": 835,
        "name": "WAGON R SMILE",
        "makeId": 53
      },
      {
        "id": 836,
        "name": "XBEE",
        "makeId": 53
      },
      {
        "id": 837,
        "name": "FORKLIFT",
        "makeId": 54
      },
      {
        "id": 838,
        "name": "OTHER",
        "makeId": 54
      },
      {
        "id": 839,
        "name": "MODEL 3",
        "makeId": 55
      },
      {
        "id": 840,
        "name": "MODEL S",
        "makeId": 55
      },
      {
        "id": 841,
        "name": "MODEL X",
        "makeId": 55
      },
      {
        "id": 842,
        "name": "TESLA",
        "makeId": 55
      },
      {
        "id": 843,
        "name": "4RUNNER",
        "makeId": 56
      },
      {
        "id": 844,
        "name": "86",
        "makeId": 56
      },
      {
        "id": 845,
        "name": "ALLEX",
        "makeId": 56
      },
      {
        "id": 846,
        "name": "ALLION",
        "makeId": 56
      },
      {
        "id": 847,
        "name": "ALPHARD",
        "makeId": 56
      },
      {
        "id": 848,
        "name": "ALTEZZA",
        "makeId": 56
      },
      {
        "id": 849,
        "name": "ALTEZZA WAGON",
        "makeId": 56
      },
      {
        "id": 850,
        "name": "AQUA",
        "makeId": 56
      },
      {
        "id": 851,
        "name": "ARISTO",
        "makeId": 56
      },
      {
        "id": 852,
        "name": "AURIS",
        "makeId": 56
      },
      {
        "id": 853,
        "name": "AVENSIS SEDAN",
        "makeId": 56
      },
      {
        "id": 854,
        "name": "AVENSIS WAGON",
        "makeId": 56
      },
      {
        "id": 855,
        "name": "BB",
        "makeId": 56
      },
      {
        "id": 856,
        "name": "BELTA",
        "makeId": 56
      },
      {
        "id": 857,
        "name": "BLADE",
        "makeId": 56
      },
      {
        "id": 858,
        "name": "BREVIS",
        "makeId": 56
      },
      {
        "id": 859,
        "name": "C-HR",
        "makeId": 56
      },
      {
        "id": 860,
        "name": "CALDINA",
        "makeId": 56
      },
      {
        "id": 861,
        "name": "CALDINA VAN",
        "makeId": 56
      },
      {
        "id": 862,
        "name": "CAMI",
        "makeId": 56
      },
      {
        "id": 863,
        "name": "CAMROAD",
        "makeId": 56
      },
      {
        "id": 864,
        "name": "CAMRY",
        "makeId": 56
      },
      {
        "id": 865,
        "name": "CARINA",
        "makeId": 56
      },
      {
        "id": 866,
        "name": "CELICA",
        "makeId": 56
      },
      {
        "id": 867,
        "name": "CELSIOR",
        "makeId": 56
      },
      {
        "id": 868,
        "name": "CENTURY",
        "makeId": 56
      },
      {
        "id": 869,
        "name": "CHASER",
        "makeId": 56
      },
      {
        "id": 870,
        "name": "COASTER",
        "makeId": 56
      },
      {
        "id": 871,
        "name": "COMS",
        "makeId": 56
      },
      {
        "id": 872,
        "name": "COPEN",
        "makeId": 56
      },
      {
        "id": 873,
        "name": "COROLLA",
        "makeId": 56
      },
      {
        "id": 874,
        "name": "COROLLA AXIO",
        "makeId": 56
      },
      {
        "id": 875,
        "name": "COROLLA CERES",
        "makeId": 56
      },
      {
        "id": 876,
        "name": "COROLLA CROSS",
        "makeId": 56
      },
      {
        "id": 877,
        "name": "COROLLA FIELDER",
        "makeId": 56
      },
      {
        "id": 878,
        "name": "COROLLA LEVIN",
        "makeId": 56
      },
      {
        "id": 879,
        "name": "COROLLA RUMION",
        "makeId": 56
      },
      {
        "id": 880,
        "name": "COROLLA RUNX",
        "makeId": 56
      },
      {
        "id": 881,
        "name": "COROLLA SPACIO",
        "makeId": 56
      },
      {
        "id": 882,
        "name": "COROLLA SPORT",
        "makeId": 56
      },
      {
        "id": 883,
        "name": "COROLLA TOURING",
        "makeId": 56
      },
      {
        "id": 884,
        "name": "COROLLA TOURING WAGON",
        "makeId": 56
      },
      {
        "id": 885,
        "name": "COROLLA VAN",
        "makeId": 56
      },
      {
        "id": 886,
        "name": "COROLLA2",
        "makeId": 56
      },
      {
        "id": 887,
        "name": "CORONA",
        "makeId": 56
      },
      {
        "id": 888,
        "name": "CORONA EXIV",
        "makeId": 56
      },
      {
        "id": 889,
        "name": "CORONA PREMIO",
        "makeId": 56
      },
      {
        "id": 890,
        "name": "CORSA",
        "makeId": 56
      },
      {
        "id": 891,
        "name": "CRESTA",
        "makeId": 56
      },
      {
        "id": 892,
        "name": "CROWN",
        "makeId": 56
      },
      {
        "id": 893,
        "name": "CROWN COMFORT",
        "makeId": 56
      },
      {
        "id": 894,
        "name": "CROWN ESTATE",
        "makeId": 56
      },
      {
        "id": 895,
        "name": "CROWN SPORT",
        "makeId": 56
      },
      {
        "id": 896,
        "name": "CROWN SPORT HV",
        "makeId": 56
      },
      {
        "id": 897,
        "name": "CROWN WAGON",
        "makeId": 56
      },
      {
        "id": 898,
        "name": "DELIBOY",
        "makeId": 56
      },
      {
        "id": 899,
        "name": "DUET",
        "makeId": 56
      },
      {
        "id": 900,
        "name": "DYNA",
        "makeId": 56
      },
      {
        "id": 901,
        "name": "DYNA DUMP",
        "makeId": 56
      },
      {
        "id": 902,
        "name": "ESQUIRE",
        "makeId": 56
      },
      {
        "id": 903,
        "name": "ESTIMA",
        "makeId": 56
      },
      {
        "id": 904,
        "name": "ESTIMA HYBRID",
        "makeId": 56
      },
      {
        "id": 905,
        "name": "FJ CRUISER",
        "makeId": 56
      },
      {
        "id": 906,
        "name": "FORKLIFT",
        "makeId": 56
      },
      {
        "id": 907,
        "name": "FUNCARGO",
        "makeId": 56
      },
      {
        "id": 908,
        "name": "GAIA",
        "makeId": 56
      },
      {
        "id": 909,
        "name": "GR YARIS",
        "makeId": 56
      },
      {
        "id": 910,
        "name": "GR86",
        "makeId": 56
      },
      {
        "id": 911,
        "name": "GRANACE",
        "makeId": 56
      },
      {
        "id": 912,
        "name": "GRANVIA",
        "makeId": 56
      },
      {
        "id": 913,
        "name": "HARRIER",
        "makeId": 56
      },
      {
        "id": 914,
        "name": "HIACE",
        "makeId": 56
      },
      {
        "id": 915,
        "name": "HIACE REGIUS",
        "makeId": 56
      },
      {
        "id": 916,
        "name": "HIACE TRUCK",
        "makeId": 56
      },
      {
        "id": 917,
        "name": "HIACE VAN",
        "makeId": 56
      },
      {
        "id": 918,
        "name": "HIACE VAN 4D4W",
        "makeId": 56
      },
      {
        "id": 919,
        "name": "HIACE VAN 5D4W",
        "makeId": 56
      },
      {
        "id": 920,
        "name": "HILUX",
        "makeId": 56
      },
      {
        "id": 921,
        "name": "HILUX SURF",
        "makeId": 56
      },
      {
        "id": 922,
        "name": "IPSUM",
        "makeId": 56
      },
      {
        "id": 923,
        "name": "IQ",
        "makeId": 56
      },
      {
        "id": 924,
        "name": "ISIS",
        "makeId": 56
      },
      {
        "id": 925,
        "name": "IST",
        "makeId": 56
      },
      {
        "id": 926,
        "name": "JPN TAXI",
        "makeId": 56
      },
      {
        "id": 927,
        "name": "KLUGER",
        "makeId": 56
      },
      {
        "id": 928,
        "name": "LAND CRUISER",
        "makeId": 56
      },
      {
        "id": 929,
        "name": "LAND CRUISER PRADO",
        "makeId": 56
      },
      {
        "id": 930,
        "name": "LITE ACE",
        "makeId": 56
      },
      {
        "id": 931,
        "name": "LITE ACE NOAH",
        "makeId": 56
      },
      {
        "id": 932,
        "name": "LITE ACE TRUCK",
        "makeId": 56
      },
      {
        "id": 933,
        "name": "LITE ACE VAN",
        "makeId": 56
      },
      {
        "id": 934,
        "name": "MARK II",
        "makeId": 56
      },
      {
        "id": 935,
        "name": "MARK II BLIT",
        "makeId": 56
      },
      {
        "id": 936,
        "name": "MARK II WAGON",
        "makeId": 56
      },
      {
        "id": 937,
        "name": "MARK X",
        "makeId": 56
      },
      {
        "id": 938,
        "name": "MARK X ZIO",
        "makeId": 56
      },
      {
        "id": 939,
        "name": "MARK2 BLIT",
        "makeId": 56
      },
      {
        "id": 940,
        "name": "MARK2 QUALIS",
        "makeId": 56
      },
      {
        "id": 941,
        "name": "MARK2 VAN",
        "makeId": 56
      },
      {
        "id": 942,
        "name": "MIRAI",
        "makeId": 56
      },
      {
        "id": 943,
        "name": "MR-S",
        "makeId": 56
      },
      {
        "id": 944,
        "name": "MR2",
        "makeId": 56
      },
      {
        "id": 945,
        "name": "NDOLA4D4W",
        "makeId": 56
      },
      {
        "id": 946,
        "name": "NOAH",
        "makeId": 56
      },
      {
        "id": 947,
        "name": "OPA",
        "makeId": 56
      },
      {
        "id": 948,
        "name": "ORIGIN",
        "makeId": 56
      },
      {
        "id": 949,
        "name": "OTHER",
        "makeId": 56
      },
      {
        "id": 950,
        "name": "PASSO",
        "makeId": 56
      },
      {
        "id": 951,
        "name": "PASSO SETTE",
        "makeId": 56
      },
      {
        "id": 952,
        "name": "PIXIS EPOCH",
        "makeId": 56
      },
      {
        "id": 953,
        "name": "PIXIS JOY",
        "makeId": 56
      },
      {
        "id": 954,
        "name": "PIXIS MEGA",
        "makeId": 56
      },
      {
        "id": 955,
        "name": "PIXIS SPACE",
        "makeId": 56
      },
      {
        "id": 956,
        "name": "PIXIS TRUCK",
        "makeId": 56
      },
      {
        "id": 957,
        "name": "PIXIS VAN",
        "makeId": 56
      },
      {
        "id": 958,
        "name": "PLATZ",
        "makeId": 56
      },
      {
        "id": 959,
        "name": "PORTE",
        "makeId": 56
      },
      {
        "id": 960,
        "name": "PREMIO",
        "makeId": 56
      },
      {
        "id": 961,
        "name": "PRIUS",
        "makeId": 56
      },
      {
        "id": 962,
        "name": "PRIUS ALPHA",
        "makeId": 56
      },
      {
        "id": 963,
        "name": "PRIUS PHEV",
        "makeId": 56
      },
      {
        "id": 964,
        "name": "PRIUS PHV",
        "makeId": 56
      },
      {
        "id": 965,
        "name": "PROBOX",
        "makeId": 56
      },
      {
        "id": 966,
        "name": "PROGRES",
        "makeId": 56
      },
      {
        "id": 967,
        "name": "PRONARD",
        "makeId": 56
      },
      {
        "id": 968,
        "name": "RACTIS",
        "makeId": 56
      },
      {
        "id": 969,
        "name": "RAIZE",
        "makeId": 56
      },
      {
        "id": 970,
        "name": "RAUM",
        "makeId": 56
      },
      {
        "id": 971,
        "name": "RAV4",
        "makeId": 56
      },
      {
        "id": 972,
        "name": "RAV4 J",
        "makeId": 56
      },
      {
        "id": 973,
        "name": "RAV4 L",
        "makeId": 56
      },
      {
        "id": 974,
        "name": "REGIUS",
        "makeId": 56
      },
      {
        "id": 975,
        "name": "REGIUS ACE VAN",
        "makeId": 56
      },
      {
        "id": 976,
        "name": "REGIUS ESV4D4W",
        "makeId": 56
      },
      {
        "id": 977,
        "name": "REGIUS ESV5D4W",
        "makeId": 56
      },
      {
        "id": 978,
        "name": "REGIUS VAN",
        "makeId": 56
      },
      {
        "id": 979,
        "name": "ROOMY",
        "makeId": 56
      },
      {
        "id": 980,
        "name": "RUSH",
        "makeId": 56
      },
      {
        "id": 981,
        "name": "SAI",
        "makeId": 56
      },
      {
        "id": 982,
        "name": "SCEPTER WAGON",
        "makeId": 56
      },
      {
        "id": 983,
        "name": "SIENNA",
        "makeId": 56
      },
      {
        "id": 984,
        "name": "SIENTA",
        "makeId": 56
      },
      {
        "id": 985,
        "name": "SOARER",
        "makeId": 56
      },
      {
        "id": 986,
        "name": "SPADE",
        "makeId": 56
      },
      {
        "id": 987,
        "name": "SPARKY",
        "makeId": 56
      },
      {
        "id": 988,
        "name": "SPRINTER",
        "makeId": 56
      },
      {
        "id": 989,
        "name": "SPRINTER CARIB",
        "makeId": 56
      },
      {
        "id": 990,
        "name": "SPRINTER MARINO",
        "makeId": 56
      },
      {
        "id": 991,
        "name": "SPRINTER VAN",
        "makeId": 56
      },
      {
        "id": 992,
        "name": "SPRINTER WAGON",
        "makeId": 56
      },
      {
        "id": 993,
        "name": "STARLET",
        "makeId": 56
      },
      {
        "id": 994,
        "name": "SUCCEED",
        "makeId": 56
      },
      {
        "id": 995,
        "name": "SUPRA",
        "makeId": 56
      },
      {
        "id": 996,
        "name": "TANK",
        "makeId": 56
      },
      {
        "id": 997,
        "name": "TOWN ACE",
        "makeId": 56
      },
      {
        "id": 998,
        "name": "TOWN ACE NOAH",
        "makeId": 56
      },
      {
        "id": 999,
        "name": "TOWN ACE TRUCK",
        "makeId": 56
      },
      {
        "id": 1000,
        "name": "TOWN ACE VAN",
        "makeId": 56
      },
      {
        "id": 1001,
        "name": "TOYOACE",
        "makeId": 56
      },
      {
        "id": 1002,
        "name": "TUNDRA",
        "makeId": 56
      },
      {
        "id": 1003,
        "name": "URBAN SUPPORTER",
        "makeId": 56
      },
      {
        "id": 1004,
        "name": "VANGUARD",
        "makeId": 56
      },
      {
        "id": 1005,
        "name": "VELLFIRE",
        "makeId": 56
      },
      {
        "id": 1006,
        "name": "VEROSSA",
        "makeId": 56
      },
      {
        "id": 1007,
        "name": "VISTA",
        "makeId": 56
      },
      {
        "id": 1008,
        "name": "VISTA ARDEO",
        "makeId": 56
      },
      {
        "id": 1009,
        "name": "VITZ",
        "makeId": 56
      },
      {
        "id": 1010,
        "name": "VOLTZ",
        "makeId": 56
      },
      {
        "id": 1011,
        "name": "VOXY",
        "makeId": 56
      },
      {
        "id": 1012,
        "name": "WILL",
        "makeId": 56
      },
      {
        "id": 1013,
        "name": "WILL VI",
        "makeId": 56
      },
      {
        "id": 1014,
        "name": "WILL VS",
        "makeId": 56
      },
      {
        "id": 1015,
        "name": "WINDOM",
        "makeId": 56
      },
      {
        "id": 1016,
        "name": "WISH",
        "makeId": 56
      },
      {
        "id": 1017,
        "name": "YARIS",
        "makeId": 56
      },
      {
        "id": 1018,
        "name": "YARIS CROSS",
        "makeId": 56
      },
      {
        "id": 1019,
        "name": "ARTEON",
        "makeId": 57
      },
      {
        "id": 1020,
        "name": "BEETLE",
        "makeId": 57
      },
      {
        "id": 1021,
        "name": "BORA",
        "makeId": 57
      },
      {
        "id": 1022,
        "name": "CC",
        "makeId": 57
      },
      {
        "id": 1023,
        "name": "CROSS POLO",
        "makeId": 57
      },
      {
        "id": 1024,
        "name": "E-GOLF",
        "makeId": 57
      },
      {
        "id": 1025,
        "name": "EOS",
        "makeId": 57
      },
      {
        "id": 1026,
        "name": "GOLF",
        "makeId": 57
      },
      {
        "id": 1027,
        "name": "GOLF  GTI",
        "makeId": 57
      },
      {
        "id": 1028,
        "name": "GOLF ALLTRACK",
        "makeId": 57
      },
      {
        "id": 1029,
        "name": "GOLF CABRIOLET",
        "makeId": 57
      },
      {
        "id": 1030,
        "name": "GOLF GTE",
        "makeId": 57
      },
      {
        "id": 1031,
        "name": "GOLF PLUS",
        "makeId": 57
      },
      {
        "id": 1032,
        "name": "GOLF R",
        "makeId": 57
      },
      {
        "id": 1033,
        "name": "GOLF TOURAN",
        "makeId": 57
      },
      {
        "id": 1034,
        "name": "GOLF VARIANT",
        "makeId": 57
      },
      {
        "id": 1035,
        "name": "GOLF WAGON",
        "makeId": 57
      },
      {
        "id": 1036,
        "name": "JETTA",
        "makeId": 57
      },
      {
        "id": 1037,
        "name": "NEW BEETLE",
        "makeId": 57
      },
      {
        "id": 1038,
        "name": "PASSAT",
        "makeId": 57
      },
      {
        "id": 1039,
        "name": "PASSAT GTE",
        "makeId": 57
      },
      {
        "id": 1040,
        "name": "PASSAT VARIANT",
        "makeId": 57
      },
      {
        "id": 1041,
        "name": "PASSAT WAGON",
        "makeId": 57
      },
      {
        "id": 1042,
        "name": "POLO",
        "makeId": 57
      },
      {
        "id": 1043,
        "name": "POLO GTI",
        "makeId": 57
      },
      {
        "id": 1044,
        "name": "PSSAT ALLTRACK",
        "makeId": 57
      },
      {
        "id": 1045,
        "name": "SCIROCCO",
        "makeId": 57
      },
      {
        "id": 1046,
        "name": "SHARAN",
        "makeId": 57
      },
      {
        "id": 1047,
        "name": "SIROCCO",
        "makeId": 57
      },
      {
        "id": 1048,
        "name": "T-CROSS",
        "makeId": 57
      },
      {
        "id": 1049,
        "name": "T-ROC",
        "makeId": 57
      },
      {
        "id": 1050,
        "name": "THE BEETLE",
        "makeId": 57
      },
      {
        "id": 1051,
        "name": "TIGUAN",
        "makeId": 57
      },
      {
        "id": 1052,
        "name": "TOUAREG",
        "makeId": 57
      },
      {
        "id": 1053,
        "name": "UP",
        "makeId": 57
      },
      {
        "id": 1054,
        "name": "240 WAGON",
        "makeId": 58
      },
      {
        "id": 1055,
        "name": "850 ESTATE",
        "makeId": 58
      },
      {
        "id": 1056,
        "name": "850 WAGON",
        "makeId": 58
      },
      {
        "id": 1057,
        "name": "C30",
        "makeId": 58
      },
      {
        "id": 1058,
        "name": "CROSS COUNTRY",
        "makeId": 58
      },
      {
        "id": 1059,
        "name": "S40",
        "makeId": 58
      },
      {
        "id": 1060,
        "name": "S60",
        "makeId": 58
      },
      {
        "id": 1061,
        "name": "S80",
        "makeId": 58
      },
      {
        "id": 1062,
        "name": "V40",
        "makeId": 58
      },
      {
        "id": 1063,
        "name": "V50",
        "makeId": 58
      },
      {
        "id": 1064,
        "name": "V60",
        "makeId": 58
      },
      {
        "id": 1065,
        "name": "V70",
        "makeId": 58
      },
      {
        "id": 1066,
        "name": "V90",
        "makeId": 58
      },
      {
        "id": 1067,
        "name": "XC40",
        "makeId": 58
      },
      {
        "id": 1068,
        "name": "XC60",
        "makeId": 58
      },
      {
        "id": 1069,
        "name": "XC70",
        "makeId": 58
      },
      {
        "id": 1070,
        "name": "XC90",
        "makeId": 58
      },
      {
        "id": 1071,
        "name": "BELUGA 80",
        "makeId": 59
      },
      {
        "id": 1072,
        "name": "TW200",
        "makeId": 59
      },
      {
        "id": 1073,
        "name": "OTHER",
        "makeId": 60
      },
      {
        "id": 1074,
        "name": "OTHER",
        "makeId": 13
      },
      {
        "id": 1075,
        "name": "OTHER",
        "makeId": 55
      },
      {
        "id": 1076,
        "name": "MDX",
        "makeId": 61
      },
      {
        "id": 1077,
        "name": "NSX",
        "makeId": 61
      },
      {
        "id": 1078,
        "name": "TL",
        "makeId": 61
      },
      {
        "id": 1079,
        "name": "145",
        "makeId": 1
      },
      {
        "id": 1080,
        "name": "155",
        "makeId": 1
      },
      {
        "id": 1081,
        "name": "TONALE",
        "makeId": 1
      },
      {
        "id": 1082,
        "name": "A -STROKE V8VANSOP",
        "makeId": 2
      },
      {
        "id": 1083,
        "name": "DB11",
        "makeId": 2
      },
      {
        "id": 1084,
        "name": "DB9",
        "makeId": 2
      },
      {
        "id": 1085,
        "name": "LAPI-DO",
        "makeId": 2
      },
      {
        "id": 1086,
        "name": "V8VANOP",
        "makeId": 2
      },
      {
        "id": 1087,
        "name": "VAN YELLOW CP",
        "makeId": 2
      },
      {
        "id": 1088,
        "name": "A5 SPOILER B4W",
        "makeId": 3
      },
      {
        "id": 1089,
        "name": "A6 ALLROAD QUATTRO",
        "makeId": 3
      },
      {
        "id": 1090,
        "name": "E-TRON",
        "makeId": 3
      },
      {
        "id": 1091,
        "name": "E-TRON GT QUATTRO",
        "makeId": 3
      },
      {
        "id": 1092,
        "name": "E-TRON S",
        "makeId": 3
      },
      {
        "id": 1093,
        "name": "E-TRON SPORTBACK",
        "makeId": 3
      },
      {
        "id": 1094,
        "name": "MODEL S",
        "makeId": 3
      },
      {
        "id": 1095,
        "name": "Q8",
        "makeId": 3
      },
      {
        "id": 1096,
        "name": "R8",
        "makeId": 3
      },
      {
        "id": 1097,
        "name": "RS 5 SPORTBACK",
        "makeId": 3
      },
      {
        "id": 1098,
        "name": "RS Q8",
        "makeId": 3
      },
      {
        "id": 1099,
        "name": "RS3",
        "makeId": 3
      },
      {
        "id": 1100,
        "name": "RS3 SPORTBACK",
        "makeId": 3
      },
      {
        "id": 1101,
        "name": "RS4",
        "makeId": 3
      },
      {
        "id": 1102,
        "name": "RS4 AVANTE",
        "makeId": 3
      },
      {
        "id": 1103,
        "name": "RS5",
        "makeId": 3
      },
      {
        "id": 1104,
        "name": "RS6 AVANTE",
        "makeId": 3
      },
      {
        "id": 1105,
        "name": "RS7 SPORTBACK",
        "makeId": 3
      },
      {
        "id": 1106,
        "name": "S6 AVANTE",
        "makeId": 3
      },
      {
        "id": 1107,
        "name": "S6 SERIES",
        "makeId": 3
      },
      {
        "id": 1108,
        "name": "S7 SPORTBACK",
        "makeId": 3
      },
      {
        "id": 1109,
        "name": "TT RS COUPE",
        "makeId": 3
      },
      {
        "id": 1110,
        "name": "TTCOUPE",
        "makeId": 3
      },
      {
        "id": 1111,
        "name": "OTHER",
        "makeId": 62
      },
      {
        "id": 1112,
        "name": "BENTAYGA",
        "makeId": 4
      },
      {
        "id": 1113,
        "name": "BENTEI",
        "makeId": 4
      },
      {
        "id": 1114,
        "name": "BENTEI4W",
        "makeId": 4
      },
      {
        "id": 1115,
        "name": "CONTINENTAL",
        "makeId": 4
      },
      {
        "id": 1116,
        "name": "FLAIS4W",
        "makeId": 4
      },
      {
        "id": 1117,
        "name": "NAVY BLUE CHI4D4W",
        "makeId": 4
      },
      {
        "id": 1118,
        "name": "4",
        "makeId": 5
      },
      {
        "id": 1119,
        "name": "7 SERIES 4W",
        "makeId": 5
      },
      {
        "id": 1120,
        "name": "I4",
        "makeId": 5
      },
      {
        "id": 1121,
        "name": "IX3",
        "makeId": 5
      },
      {
        "id": 1122,
        "name": "M ROADSTER",
        "makeId": 5
      },
      {
        "id": 1123,
        "name": "M6",
        "makeId": 5
      },
      {
        "id": 1124,
        "name": "M8",
        "makeId": 5
      },
      {
        "id": 1125,
        "name": "X4 M",
        "makeId": 5
      },
      {
        "id": 1126,
        "name": "X7",
        "makeId": 5
      },
      {
        "id": 1127,
        "name": "Z3 COUPE",
        "makeId": 5
      },
      {
        "id": 1128,
        "name": "Z3 SERIES",
        "makeId": 5
      },
      {
        "id": 1129,
        "name": "B4",
        "makeId": 6
      },
      {
        "id": 1130,
        "name": "B5",
        "makeId": 6
      },
      {
        "id": 1131,
        "name": "B8",
        "makeId": 6
      },
      {
        "id": 1132,
        "name": "D3",
        "makeId": 6
      },
      {
        "id": 1133,
        "name": "D4",
        "makeId": 6
      },
      {
        "id": 1134,
        "name": "D5",
        "makeId": 6
      },
      {
        "id": 1135,
        "name": "XD3",
        "makeId": 6
      },
      {
        "id": 1136,
        "name": "BYUIANK RAY 4W",
        "makeId": 63
      },
      {
        "id": 1137,
        "name": "ATS",
        "makeId": 7
      },
      {
        "id": 1138,
        "name": "CADE ESKARE",
        "makeId": 7
      },
      {
        "id": 1139,
        "name": "CADE ESKARE-4W",
        "makeId": 7
      },
      {
        "id": 1140,
        "name": "CONCOURSE",
        "makeId": 7
      },
      {
        "id": 1141,
        "name": "CT5",
        "makeId": 7
      },
      {
        "id": 1142,
        "name": "CT6",
        "makeId": 7
      },
      {
        "id": 1143,
        "name": "CTS SPORT WAGON",
        "makeId": 7
      },
      {
        "id": 1144,
        "name": "DTS",
        "makeId": 7
      },
      {
        "id": 1145,
        "name": "ESCALADE",
        "makeId": 7
      },
      {
        "id": 1146,
        "name": "FLEETWOOD",
        "makeId": 7
      },
      {
        "id": 1147,
        "name": "OTHER",
        "makeId": 7
      },
      {
        "id": 1148,
        "name": "XT4",
        "makeId": 7
      },
      {
        "id": 1149,
        "name": "XT5 CROSSOVER",
        "makeId": 7
      },
      {
        "id": 1150,
        "name": "XT6",
        "makeId": 7
      },
      {
        "id": 1151,
        "name": "ASTRO",
        "makeId": 9
      },
      {
        "id": 1152,
        "name": "BLAZER SILVERRADO",
        "makeId": 9
      },
      {
        "id": 1153,
        "name": "CAMARO",
        "makeId": 9
      },
      {
        "id": 1154,
        "name": "CVL AVALANCHE",
        "makeId": 9
      },
      {
        "id": 1155,
        "name": "CVL BELL AIR",
        "makeId": 9
      },
      {
        "id": 1156,
        "name": "CVL CAMARO",
        "makeId": 9
      },
      {
        "id": 1157,
        "name": "CVL CORVETTE",
        "makeId": 9
      },
      {
        "id": 1158,
        "name": "CVL CORVETTE OP",
        "makeId": 9
      },
      {
        "id": 1159,
        "name": "CVL EXPRESS 4W",
        "makeId": 9
      },
      {
        "id": 1160,
        "name": "CVL K10",
        "makeId": 9
      },
      {
        "id": 1161,
        "name": "CVL L KAMI-NO",
        "makeId": 9
      },
      {
        "id": 1162,
        "name": "CVL SILVER LA4D4W",
        "makeId": 9
      },
      {
        "id": 1163,
        "name": "CVL SUBURBAN",
        "makeId": 9
      },
      {
        "id": 1164,
        "name": "CVL SUBURBAN 4W",
        "makeId": 9
      },
      {
        "id": 1165,
        "name": "CVL TAHOE",
        "makeId": 9
      },
      {
        "id": 1166,
        "name": "CVL YELLOW .PTEBA4W",
        "makeId": 9
      },
      {
        "id": 1167,
        "name": "CVLA BARA 4D4W",
        "makeId": 9
      },
      {
        "id": 1168,
        "name": "CVLNO-BA",
        "makeId": 9
      },
      {
        "id": 1169,
        "name": "OTHER",
        "makeId": 9
      },
      {
        "id": 1170,
        "name": "SONIC",
        "makeId": 9
      },
      {
        "id": 1171,
        "name": "TRAIL BLAZER",
        "makeId": 9
      },
      {
        "id": 1172,
        "name": "300C TOURING",
        "makeId": 10
      },
      {
        "id": 1173,
        "name": "300M",
        "makeId": 10
      },
      {
        "id": 1174,
        "name": "CROSSFIRE",
        "makeId": 10
      },
      {
        "id": 1175,
        "name": "MAGNUM",
        "makeId": 10
      },
      {
        "id": 1176,
        "name": "2CV6",
        "makeId": 11
      },
      {
        "id": 1177,
        "name": "A CLASS",
        "makeId": 11
      },
      {
        "id": 1178,
        "name": "C6",
        "makeId": 11
      },
      {
        "id": 1179,
        "name": "DS7 CROSSBACK",
        "makeId": 11
      },
      {
        "id": 1180,
        "name": "E-C4",
        "makeId": 11
      },
      {
        "id": 1181,
        "name": "GRAND C4 SPACETOURER",
        "makeId": 11
      },
      {
        "id": 1182,
        "name": "BOON LUMINAS",
        "makeId": 12
      },
      {
        "id": 1183,
        "name": "CAST ACTIVA",
        "makeId": 12
      },
      {
        "id": 1184,
        "name": "DELTA",
        "makeId": 12
      },
      {
        "id": 1185,
        "name": "DELTA TRUCK",
        "makeId": 12
      },
      {
        "id": 1186,
        "name": "ELF",
        "makeId": 12
      },
      {
        "id": 1187,
        "name": "OTHER",
        "makeId": 12
      },
      {
        "id": 1188,
        "name": "STORIA",
        "makeId": 12
      },
      {
        "id": 1189,
        "name": "YRV",
        "makeId": 12
      },
      {
        "id": 1190,
        "name": "DA-TO",
        "makeId": 14
      },
      {
        "id": 1191,
        "name": "DURANGO",
        "makeId": 14
      },
      {
        "id": 1192,
        "name": "RAM",
        "makeId": 14
      },
      {
        "id": 1193,
        "name": "RAM 2D4W",
        "makeId": 14
      },
      {
        "id": 1194,
        "name": "TE. RAN 4W",
        "makeId": 14
      },
      {
        "id": 1195,
        "name": "TEA RENCP",
        "makeId": 14
      },
      {
        "id": 1196,
        "name": "355F1CP",
        "makeId": 15
      },
      {
        "id": 1197,
        "name": "360",
        "makeId": 15
      },
      {
        "id": 1198,
        "name": "458 ITALIA",
        "makeId": 15
      },
      {
        "id": 1199,
        "name": "599",
        "makeId": 15
      },
      {
        "id": 1200,
        "name": "CALIFORNIA",
        "makeId": 15
      },
      {
        "id": 1201,
        "name": "F12 BELL LI",
        "makeId": 15
      },
      {
        "id": 1202,
        "name": "F355OP",
        "makeId": 15
      },
      {
        "id": 1203,
        "name": "F430 SPIDER",
        "makeId": 15
      },
      {
        "id": 1204,
        "name": "F8TRIBUTO",
        "makeId": 15
      },
      {
        "id": 1205,
        "name": "FF",
        "makeId": 15
      },
      {
        "id": 1206,
        "name": "GTC4LUSSO",
        "makeId": 15
      },
      {
        "id": 1207,
        "name": "KALIFORU30",
        "makeId": 15
      },
      {
        "id": 1208,
        "name": "KALIFORUNIT",
        "makeId": 15
      },
      {
        "id": 1209,
        "name": "PORTOFINO",
        "makeId": 15
      },
      {
        "id": 1210,
        "name": "ROMA",
        "makeId": 15
      },
      {
        "id": 1211,
        "name": "SK DER 16",
        "makeId": 15
      },
      {
        "id": 1212,
        "name": "124",
        "makeId": 16
      },
      {
        "id": 1213,
        "name": "500C",
        "makeId": 16
      },
      {
        "id": 1214,
        "name": "500E",
        "makeId": 16
      },
      {
        "id": 1215,
        "name": "500S",
        "makeId": 16
      },
      {
        "id": 1216,
        "name": "ABARTH 124 SPIDER",
        "makeId": 16
      },
      {
        "id": 1217,
        "name": "ABARTH 500",
        "makeId": 16
      },
      {
        "id": 1218,
        "name": "ABARTH 695",
        "makeId": 16
      },
      {
        "id": 1219,
        "name": "DOBLO",
        "makeId": 16
      },
      {
        "id": 1220,
        "name": "DUCATO",
        "makeId": 16
      },
      {
        "id": 1221,
        "name": "MULTIPLA",
        "makeId": 16
      },
      {
        "id": 1222,
        "name": "BRONCO 4W",
        "makeId": 17
      },
      {
        "id": 1223,
        "name": "ECONOLINE",
        "makeId": 17
      },
      {
        "id": 1224,
        "name": "ECOSPORT",
        "makeId": 17
      },
      {
        "id": 1225,
        "name": "F150",
        "makeId": 17
      },
      {
        "id": 1226,
        "name": "FO EXCURSION 4W",
        "makeId": 17
      },
      {
        "id": 1227,
        "name": "FO THUNDER B",
        "makeId": 17
      },
      {
        "id": 1228,
        "name": "FOEK SPEC TISI",
        "makeId": 17
      },
      {
        "id": 1229,
        "name": "FOEKSPSPT4W",
        "makeId": 17
      },
      {
        "id": 1230,
        "name": "FREDA ATF",
        "makeId": 17
      },
      {
        "id": 1231,
        "name": "LINCOLN NAVIGATOR",
        "makeId": 17
      },
      {
        "id": 1232,
        "name": "TOLINO",
        "makeId": 17
      },
      {
        "id": 1233,
        "name": "CADILLAC DEVILLE",
        "makeId": 18
      },
      {
        "id": 1234,
        "name": "CADILLAC ESCALADE",
        "makeId": 18
      },
      {
        "id": 1235,
        "name": "CHEVROLET TRAIL BLAZER",
        "makeId": 18
      },
      {
        "id": 1236,
        "name": "CADILLAC ESCALADE",
        "makeId": 64
      },
      {
        "id": 1237,
        "name": "CHEVROLET AVALANCHE",
        "makeId": 64
      },
      {
        "id": 1238,
        "name": "CHEVROLET CAMARO",
        "makeId": 64
      },
      {
        "id": 1239,
        "name": "EXPRESS",
        "makeId": 64
      },
      {
        "id": 1240,
        "name": "SAVANA",
        "makeId": 64
      },
      {
        "id": 1241,
        "name": "SUBURBAN",
        "makeId": 64
      },
      {
        "id": 1242,
        "name": "TERRAIN",
        "makeId": 64
      },
      {
        "id": 1243,
        "name": "YUKON",
        "makeId": 64
      },
      {
        "id": 1244,
        "name": "BUS",
        "makeId": 19
      },
      {
        "id": 1245,
        "name": "DUTRO HV",
        "makeId": 19
      },
      {
        "id": 1246,
        "name": "LIESSE II",
        "makeId": 19
      },
      {
        "id": 1247,
        "name": "MELPHA",
        "makeId": 19
      },
      {
        "id": 1248,
        "name": "ACCORD HYBRID",
        "makeId": 21
      },
      {
        "id": 1249,
        "name": "AVANCIER",
        "makeId": 21
      },
      {
        "id": 1250,
        "name": "CITY",
        "makeId": 21
      },
      {
        "id": 1251,
        "name": "CIVIC COUPE",
        "makeId": 21
      },
      {
        "id": 1252,
        "name": "DOMANI",
        "makeId": 21
      },
      {
        "id": 1253,
        "name": "HONDA E",
        "makeId": 21
      },
      {
        "id": 1254,
        "name": "LOGO",
        "makeId": 21
      },
      {
        "id": 1255,
        "name": "MINICAB TOLA",
        "makeId": 21
      },
      {
        "id": 1256,
        "name": "ORTHIA",
        "makeId": 21
      },
      {
        "id": 1257,
        "name": "STREET",
        "makeId": 21
      },
      {
        "id": 1258,
        "name": "OTHER",
        "makeId": 22
      },
      {
        "id": 1259,
        "name": "IONIQ 5",
        "makeId": 65
      },
      {
        "id": 1260,
        "name": "SRX",
        "makeId": 65
      },
      {
        "id": 1261,
        "name": "FX35",
        "makeId": 23
      },
      {
        "id": 1262,
        "name": "FX50",
        "makeId": 23
      },
      {
        "id": 1263,
        "name": "G37",
        "makeId": 23
      },
      {
        "id": 1264,
        "name": "IN FINIFX35",
        "makeId": 23
      },
      {
        "id": 1265,
        "name": "QX56",
        "makeId": 23
      },
      {
        "id": 1266,
        "name": "BELLETT",
        "makeId": 25
      },
      {
        "id": 1267,
        "name": "ELF DUMP",
        "makeId": 25
      },
      {
        "id": 1268,
        "name": "ELF NKR",
        "makeId": 25
      },
      {
        "id": 1269,
        "name": "ELF UT",
        "makeId": 25
      },
      {
        "id": 1270,
        "name": "ERGA",
        "makeId": 25
      },
      {
        "id": 1271,
        "name": "FARGO",
        "makeId": 25
      },
      {
        "id": 1272,
        "name": "FARGO TRUCK",
        "makeId": 25
      },
      {
        "id": 1273,
        "name": "FARGO VAN",
        "makeId": 25
      },
      {
        "id": 1274,
        "name": "JOURNEY",
        "makeId": 25
      },
      {
        "id": 1275,
        "name": "MU",
        "makeId": 25
      },
      {
        "id": 1276,
        "name": "JAGUAR E MANY IOP",
        "makeId": 26
      },
      {
        "id": 1277,
        "name": "JAGUAR F",
        "makeId": 26
      },
      {
        "id": 1278,
        "name": "OTHER",
        "makeId": 26
      },
      {
        "id": 1279,
        "name": "SOVEREIGN",
        "makeId": 26
      },
      {
        "id": 1280,
        "name": "COMMANDER",
        "makeId": 27
      },
      {
        "id": 1281,
        "name": "GRAND CHEROKEE L",
        "makeId": 27
      },
      {
        "id": 1282,
        "name": "KLAIJ G CHE RO YELLOW 4W",
        "makeId": 27
      },
      {
        "id": 1283,
        "name": "KLAIJ GWAGONI",
        "makeId": 27
      },
      {
        "id": 1284,
        "name": "KLAIJ GWAGONI4W",
        "makeId": 27
      },
      {
        "id": 1285,
        "name": "KLAIJ JKOMAN MANY 4W",
        "makeId": 27
      },
      {
        "id": 1286,
        "name": "KLAIJ RAN GLAUL",
        "makeId": 27
      },
      {
        "id": 1287,
        "name": "KLAIJGLATE4W",
        "makeId": 27
      },
      {
        "id": 1288,
        "name": "RENEGADE 4XE",
        "makeId": 27
      },
      {
        "id": 1289,
        "name": "AVENTADOR",
        "makeId": 66
      },
      {
        "id": 1290,
        "name": "DIABLO",
        "makeId": 66
      },
      {
        "id": 1291,
        "name": "GALLARDO",
        "makeId": 66
      },
      {
        "id": 1292,
        "name": "HURACAN",
        "makeId": 66
      },
      {
        "id": 1293,
        "name": "ULAKANCP",
        "makeId": 66
      },
      {
        "id": 1294,
        "name": "ULAKANCP4W",
        "makeId": 66
      },
      {
        "id": 1295,
        "name": "URUS",
        "makeId": 66
      },
      {
        "id": 1296,
        "name": "DELTA",
        "makeId": 67
      },
      {
        "id": 1297,
        "name": "THEMA",
        "makeId": 67
      },
      {
        "id": 1298,
        "name": "YPSILON",
        "makeId": 67
      },
      {
        "id": 1299,
        "name": "DISCOVERY 3",
        "makeId": 32
      },
      {
        "id": 1300,
        "name": "FREELANDER 2",
        "makeId": 32
      },
      {
        "id": 1301,
        "name": "LR EVOQUE",
        "makeId": 32
      },
      {
        "id": 1302,
        "name": "LR RANGE RSP4W",
        "makeId": 32
      },
      {
        "id": 1303,
        "name": "LRTIFE2D4W",
        "makeId": 32
      },
      {
        "id": 1304,
        "name": "LRTIFE5D4W",
        "makeId": 32
      },
      {
        "id": 1305,
        "name": "LRVELA-RU",
        "makeId": 32
      },
      {
        "id": 1306,
        "name": "RANGE ROVER VOGUE",
        "makeId": 32
      },
      {
        "id": 1307,
        "name": "LBX",
        "makeId": 33
      },
      {
        "id": 1308,
        "name": "LFA",
        "makeId": 33
      },
      {
        "id": 1309,
        "name": "LM",
        "makeId": 33
      },
      {
        "id": 1310,
        "name": "UREKSALX570",
        "makeId": 33
      },
      {
        "id": 1311,
        "name": "UREKSARX350",
        "makeId": 33
      },
      {
        "id": 1312,
        "name": "AVIATOR",
        "makeId": 34
      },
      {
        "id": 1313,
        "name": "CONTINENTAL",
        "makeId": 34
      },
      {
        "id": 1314,
        "name": "NAVY BLUE CHINENCP",
        "makeId": 34
      },
      {
        "id": 1315,
        "name": "TOWN CAR",
        "makeId": 34
      },
      {
        "id": 1316,
        "name": "ELISE",
        "makeId": 35
      },
      {
        "id": 1317,
        "name": "EMIRA",
        "makeId": 35
      },
      {
        "id": 1318,
        "name": "ESPRIT",
        "makeId": 35
      },
      {
        "id": 1319,
        "name": "OTHER",
        "makeId": 35
      },
      {
        "id": 1320,
        "name": "COUPE",
        "makeId": 36
      },
      {
        "id": 1321,
        "name": "GRANTURISMO",
        "makeId": 36
      },
      {
        "id": 1322,
        "name": "MASE LATTE GRAY CAR RE",
        "makeId": 36
      },
      {
        "id": 1323,
        "name": "MASE LATTE GRAY CAR RE4W",
        "makeId": 36
      },
      {
        "id": 1324,
        "name": "MC20",
        "makeId": 36
      },
      {
        "id": 1325,
        "name": "OTHER",
        "makeId": 36
      },
      {
        "id": 1326,
        "name": "QTORO PORTE",
        "makeId": 36
      },
      {
        "id": 1327,
        "name": "REVAN4W",
        "makeId": 36
      },
      {
        "id": 1328,
        "name": "3FA -STROKE BAK",
        "makeId": 37
      },
      {
        "id": 1329,
        "name": "3FASBA4W",
        "makeId": 37
      },
      {
        "id": 1330,
        "name": "AUTOZAM SCRUM",
        "makeId": 37
      },
      {
        "id": 1331,
        "name": "AXELA HYBRID",
        "makeId": 37
      },
      {
        "id": 1332,
        "name": "AZ-1",
        "makeId": 37
      },
      {
        "id": 1333,
        "name": "BONGO BRAWNY",
        "makeId": 37
      },
      {
        "id": 1334,
        "name": "CAPELLA WAGON",
        "makeId": 37
      },
      {
        "id": 1335,
        "name": "EUNOS PRESSO",
        "makeId": 37
      },
      {
        "id": 1336,
        "name": "FLAIR WAGON CUSTOM S",
        "makeId": 37
      },
      {
        "id": 1337,
        "name": "FLAIR WAGON TOUGH STYLE",
        "makeId": 37
      },
      {
        "id": 1338,
        "name": "LUCE",
        "makeId": 37
      },
      {
        "id": 1339,
        "name": "MX-30 ROTARY EV",
        "makeId": 37
      },
      {
        "id": 1340,
        "name": "OTHER",
        "makeId": 37
      },
      {
        "id": 1341,
        "name": "PROCEED MARVIE",
        "makeId": 37
      },
      {
        "id": 1342,
        "name": "SPEED AXELA",
        "makeId": 37
      },
      {
        "id": 1343,
        "name": "TITAN DASI4D4W",
        "makeId": 37
      },
      {
        "id": 1344,
        "name": "190 CLASS",
        "makeId": 38
      },
      {
        "id": 1345,
        "name": "AMG A CLASS",
        "makeId": 38
      },
      {
        "id": 1346,
        "name": "AMG G CLASS",
        "makeId": 38
      },
      {
        "id": 1347,
        "name": "AMG M CLASS",
        "makeId": 38
      },
      {
        "id": 1348,
        "name": "AMG SL",
        "makeId": 38
      },
      {
        "id": 1349,
        "name": "E-CLASS ALL-TERRAIN",
        "makeId": 38
      },
      {
        "id": 1350,
        "name": "EQC",
        "makeId": 38
      },
      {
        "id": 1351,
        "name": "EQS",
        "makeId": 38
      },
      {
        "id": 1352,
        "name": "GT",
        "makeId": 38
      },
      {
        "id": 1353,
        "name": "GT S",
        "makeId": 38
      },
      {
        "id": 1354,
        "name": "MB CLA CLASS",
        "makeId": 38
      },
      {
        "id": 1355,
        "name": "MB CLS CLASS",
        "makeId": 38
      },
      {
        "id": 1356,
        "name": "MB G CLASS 3D4W",
        "makeId": 38
      },
      {
        "id": 1357,
        "name": "MB G CLASS 5D4W",
        "makeId": 38
      },
      {
        "id": 1358,
        "name": "MB GL CLASS",
        "makeId": 38
      },
      {
        "id": 1359,
        "name": "MB GLC",
        "makeId": 38
      },
      {
        "id": 1360,
        "name": "MB GLE",
        "makeId": 38
      },
      {
        "id": 1361,
        "name": "MB GLS",
        "makeId": 38
      },
      {
        "id": 1362,
        "name": "MB UNIMOG",
        "makeId": 38
      },
      {
        "id": 1363,
        "name": "OTHER",
        "makeId": 38
      },
      {
        "id": 1364,
        "name": "SLC",
        "makeId": 38
      },
      {
        "id": 1365,
        "name": "AERO ACE",
        "makeId": 40
      },
      {
        "id": 1366,
        "name": "AERO MIDI",
        "makeId": 40
      },
      {
        "id": 1367,
        "name": "AERO QUENN",
        "makeId": 40
      },
      {
        "id": 1368,
        "name": "CANTER DUMP",
        "makeId": 40
      },
      {
        "id": 1369,
        "name": "CANTER GUTS 4D4W",
        "makeId": 40
      },
      {
        "id": 1370,
        "name": "CANTER WIDE 2D",
        "makeId": 40
      },
      {
        "id": 1371,
        "name": "DEBONAIR",
        "makeId": 40
      },
      {
        "id": 1372,
        "name": "DELICA D3",
        "makeId": 40
      },
      {
        "id": 1373,
        "name": "DELICA MINI",
        "makeId": 40
      },
      {
        "id": 1374,
        "name": "EK X EV",
        "makeId": 40
      },
      {
        "id": 1375,
        "name": "ETERNA",
        "makeId": 40
      },
      {
        "id": 1376,
        "name": "FUSO BUS",
        "makeId": 40
      },
      {
        "id": 1377,
        "name": "GREAT",
        "makeId": 40
      },
      {
        "id": 1378,
        "name": "LANCER EVOLUTION",
        "makeId": 40
      },
      {
        "id": 1379,
        "name": "LEGNUM",
        "makeId": 40
      },
      {
        "id": 1380,
        "name": "MINICAB MIEV TRUCK",
        "makeId": 40
      },
      {
        "id": 1381,
        "name": "MINIKA TOPPO",
        "makeId": 40
      },
      {
        "id": 1382,
        "name": "MINIYON",
        "makeId": 40
      },
      {
        "id": 1383,
        "name": "MITUBISHI",
        "makeId": 40
      },
      {
        "id": 1384,
        "name": "PAJERO JR",
        "makeId": 40
      },
      {
        "id": 1385,
        "name": "PAJERO JUNIOR",
        "makeId": 40
      },
      {
        "id": 1386,
        "name": "STRADA",
        "makeId": 40
      },
      {
        "id": 1387,
        "name": "GALUE 3",
        "makeId": 41
      },
      {
        "id": 1388,
        "name": "GALUE-3",
        "makeId": 41
      },
      {
        "id": 1389,
        "name": "LE-SEYDE",
        "makeId": 41
      },
      {
        "id": 1390,
        "name": "MC-1",
        "makeId": 41
      },
      {
        "id": 1391,
        "name": "ROCKSTAR",
        "makeId": 41
      },
      {
        "id": 1392,
        "name": "RYUGI",
        "makeId": 41
      },
      {
        "id": 1393,
        "name": "ZERO ONE",
        "makeId": 41
      },
      {
        "id": 1394,
        "name": "4/4",
        "makeId": 68
      },
      {
        "id": 1395,
        "name": "ATLAS DUMP",
        "makeId": 42
      },
      {
        "id": 1396,
        "name": "AVENIR WAGON",
        "makeId": 42
      },
      {
        "id": 1397,
        "name": "BASSARA",
        "makeId": 42
      },
      {
        "id": 1398,
        "name": "BIGTHUMB",
        "makeId": 42
      },
      {
        "id": 1399,
        "name": "CEDRIC WAGON",
        "makeId": 42
      },
      {
        "id": 1400,
        "name": "CIVILIAN B 2D4W",
        "makeId": 42
      },
      {
        "id": 1401,
        "name": "CLIPPER",
        "makeId": 42
      },
      {
        "id": 1402,
        "name": "DATSUN BRUB",
        "makeId": 42
      },
      {
        "id": 1403,
        "name": "ELGRAND 5D4W",
        "makeId": 42
      },
      {
        "id": 1404,
        "name": "EXA",
        "makeId": 42
      },
      {
        "id": 1405,
        "name": "HOMY",
        "makeId": 42
      },
      {
        "id": 1406,
        "name": "HOMY VAN",
        "makeId": 42
      },
      {
        "id": 1407,
        "name": "K ON",
        "makeId": 42
      },
      {
        "id": 1408,
        "name": "KA Z WIDE",
        "makeId": 42
      },
      {
        "id": 1409,
        "name": "LANGLEY",
        "makeId": 42
      },
      {
        "id": 1410,
        "name": "MISTRAL",
        "makeId": 42
      },
      {
        "id": 1411,
        "name": "NT450 ATLAS",
        "makeId": 42
      },
      {
        "id": 1412,
        "name": "NV350 CARAVAN WAGON",
        "makeId": 42
      },
      {
        "id": 1413,
        "name": "PRESEA",
        "makeId": 42
      },
      {
        "id": 1414,
        "name": "QUEST",
        "makeId": 42
      },
      {
        "id": 1415,
        "name": "RNESSA",
        "makeId": 42
      },
      {
        "id": 1416,
        "name": "SPACE RUNNER",
        "makeId": 42
      },
      {
        "id": 1417,
        "name": "TRUCK",
        "makeId": 42
      },
      {
        "id": 1418,
        "name": "OMEGA",
        "makeId": 43
      },
      {
        "id": 1419,
        "name": "540C",
        "makeId": 44
      },
      {
        "id": 1420,
        "name": "570GT",
        "makeId": 44
      },
      {
        "id": 1421,
        "name": "570S",
        "makeId": 44
      },
      {
        "id": 1422,
        "name": "600LT",
        "makeId": 44
      },
      {
        "id": 1423,
        "name": "720S",
        "makeId": 44
      },
      {
        "id": 1424,
        "name": "ANTICO",
        "makeId": 44
      },
      {
        "id": 1425,
        "name": "ARMADA",
        "makeId": 44
      },
      {
        "id": 1426,
        "name": "BYD ATTO3",
        "makeId": 44
      },
      {
        "id": 1427,
        "name": "CATERHAM",
        "makeId": 44
      },
      {
        "id": 1428,
        "name": "COACH MEN",
        "makeId": 44
      },
      {
        "id": 1429,
        "name": "COBRA",
        "makeId": 44
      },
      {
        "id": 1430,
        "name": "DALA-LA",
        "makeId": 44
      },
      {
        "id": 1431,
        "name": "DS DS3 CROSS B",
        "makeId": 44
      },
      {
        "id": 1432,
        "name": "E LEO",
        "makeId": 44
      },
      {
        "id": 1433,
        "name": "EKS YELLOW .LI BAR",
        "makeId": 44
      },
      {
        "id": 1434,
        "name": "HARLEY DABIDOSON",
        "makeId": 44
      },
      {
        "id": 1435,
        "name": "HYONTE SONATA",
        "makeId": 44
      },
      {
        "id": 1436,
        "name": "HYONTESRX",
        "makeId": 44
      },
      {
        "id": 1437,
        "name": "KA Z",
        "makeId": 44
      },
      {
        "id": 1438,
        "name": "KAI GAI NISSAN ARMADA",
        "makeId": 44
      },
      {
        "id": 1439,
        "name": "M MAYBACH GLS4W",
        "makeId": 44
      },
      {
        "id": 1440,
        "name": "M MAYBACH S",
        "makeId": 44
      },
      {
        "id": 1441,
        "name": "M MAYBACH S CLASS",
        "makeId": 44
      },
      {
        "id": 1442,
        "name": "MAKLAMP412CSPA",
        "makeId": 44
      },
      {
        "id": 1443,
        "name": "MAKLAREN650SSPA",
        "makeId": 44
      },
      {
        "id": 1444,
        "name": "MAKLAREN720S",
        "makeId": 44
      },
      {
        "id": 1445,
        "name": "MEAMG C",
        "makeId": 44
      },
      {
        "id": 1446,
        "name": "MEAMG E",
        "makeId": 44
      },
      {
        "id": 1447,
        "name": "MEAMG EQE",
        "makeId": 44
      },
      {
        "id": 1448,
        "name": "MEAMG EQS",
        "makeId": 44
      },
      {
        "id": 1449,
        "name": "MEAMG GLA",
        "makeId": 44
      },
      {
        "id": 1450,
        "name": "MEAMG GLC",
        "makeId": 44
      },
      {
        "id": 1451,
        "name": "MEAMG GLE",
        "makeId": 44
      },
      {
        "id": 1452,
        "name": "MEAMG GLS",
        "makeId": 44
      },
      {
        "id": 1453,
        "name": "MEAMG S",
        "makeId": 44
      },
      {
        "id": 1454,
        "name": "MEAMG S CLASS",
        "makeId": 44
      },
      {
        "id": 1455,
        "name": "MEAMGCLASHUB4W",
        "makeId": 44
      },
      {
        "id": 1456,
        "name": "MEAMGEQESUV4W",
        "makeId": 44
      },
      {
        "id": 1457,
        "name": "MEIJI",
        "makeId": 44
      },
      {
        "id": 1458,
        "name": "MODEL Y",
        "makeId": 44
      },
      {
        "id": 1459,
        "name": "NANSEI",
        "makeId": 44
      },
      {
        "id": 1460,
        "name": "OTHER IMPORTED",
        "makeId": 44
      },
      {
        "id": 1461,
        "name": "OTHER YELLOW KAI",
        "makeId": 44
      },
      {
        "id": 1462,
        "name": "RRGO- -STROKE",
        "makeId": 44
      },
      {
        "id": 1463,
        "name": "RRKALI NUM",
        "makeId": 44
      },
      {
        "id": 1464,
        "name": "SHINMAYWA",
        "makeId": 44
      },
      {
        "id": 1465,
        "name": "SIN DAIWA",
        "makeId": 44
      },
      {
        "id": 1466,
        "name": "SKANIA",
        "makeId": 44
      },
      {
        "id": 1467,
        "name": "TRIUMPH",
        "makeId": 44
      },
      {
        "id": 1468,
        "name": "UNIC",
        "makeId": 44
      },
      {
        "id": 1469,
        "name": "UNIC CRANE",
        "makeId": 44
      },
      {
        "id": 1470,
        "name": "UNISA TITAN",
        "makeId": 44
      },
      {
        "id": 1471,
        "name": "UNISA TITAN 4D4W",
        "makeId": 44
      },
      {
        "id": 1472,
        "name": "US ACCORD WAGON",
        "makeId": 44
      },
      {
        "id": 1473,
        "name": "UTOYOFJ CRUISER 4W",
        "makeId": 44
      },
      {
        "id": 1474,
        "name": "WESTFIELD OP",
        "makeId": 44
      },
      {
        "id": 1475,
        "name": "ZX4R",
        "makeId": 44
      },
      {
        "id": 1476,
        "name": "ZZ",
        "makeId": 44
      },
      {
        "id": 1477,
        "name": "1007",
        "makeId": 45
      },
      {
        "id": 1478,
        "name": "106",
        "makeId": 45
      },
      {
        "id": 1479,
        "name": "407",
        "makeId": 45
      },
      {
        "id": 1480,
        "name": "BOKSA",
        "makeId": 45
      },
      {
        "id": 1481,
        "name": "E-208",
        "makeId": 45
      },
      {
        "id": 1482,
        "name": "OTHER",
        "makeId": 45
      },
      {
        "id": 1483,
        "name": "RIFTER",
        "makeId": 45
      },
      {
        "id": 1484,
        "name": "PONTE FIRE B",
        "makeId": 69
      },
      {
        "id": 1485,
        "name": "356",
        "makeId": 46
      },
      {
        "id": 1486,
        "name": "718 BOXSTER",
        "makeId": 46
      },
      {
        "id": 1487,
        "name": "718 CAYMAN",
        "makeId": 46
      },
      {
        "id": 1488,
        "name": "718 SPY",
        "makeId": 46
      },
      {
        "id": 1489,
        "name": "911CP4W",
        "makeId": 46
      },
      {
        "id": 1490,
        "name": "911OP4W",
        "makeId": 46
      },
      {
        "id": 1491,
        "name": "914",
        "makeId": 46
      },
      {
        "id": 1492,
        "name": "BOXSTER",
        "makeId": 46
      },
      {
        "id": 1493,
        "name": "KAI KPE",
        "makeId": 46
      },
      {
        "id": 1494,
        "name": "OTHER",
        "makeId": 46
      },
      {
        "id": 1495,
        "name": "PANASONIC ME",
        "makeId": 46
      },
      {
        "id": 1496,
        "name": "PORU MANY IKAN4D4W",
        "makeId": 46
      },
      {
        "id": 1497,
        "name": "PORU PANASONIC ME",
        "makeId": 46
      },
      {
        "id": 1498,
        "name": "PORU718BOKS",
        "makeId": 46
      },
      {
        "id": 1499,
        "name": "TAYCAN",
        "makeId": 46
      },
      {
        "id": 1500,
        "name": "ALPINE",
        "makeId": 47
      },
      {
        "id": 1501,
        "name": "ALPINE A110",
        "makeId": 47
      },
      {
        "id": 1502,
        "name": "ARCANA",
        "makeId": 47
      },
      {
        "id": 1503,
        "name": "CAPTUR",
        "makeId": 47
      },
      {
        "id": 1504,
        "name": "KOLEOS",
        "makeId": 47
      },
      {
        "id": 1505,
        "name": "PHANTOM",
        "makeId": 70
      },
      {
        "id": 1506,
        "name": "9-5X",
        "makeId": 49
      },
      {
        "id": 1507,
        "name": "900 SERIES",
        "makeId": 49
      },
      {
        "id": 1508,
        "name": "9000",
        "makeId": 49
      },
      {
        "id": 1509,
        "name": "COUPE",
        "makeId": 50
      },
      {
        "id": 1510,
        "name": "FORTWO COUPE",
        "makeId": 50
      },
      {
        "id": 1511,
        "name": "FORTWO ELECTRIC DRIVE",
        "makeId": 50
      },
      {
        "id": 1512,
        "name": "K",
        "makeId": 50
      },
      {
        "id": 1513,
        "name": "360",
        "makeId": 51
      },
      {
        "id": 1514,
        "name": "ALCYONE SVX",
        "makeId": 51
      },
      {
        "id": 1515,
        "name": "IMPREZA 5HB",
        "makeId": 51
      },
      {
        "id": 1516,
        "name": "IMPREZA SPORT HYBRID",
        "makeId": 51
      },
      {
        "id": 1517,
        "name": "LEGACY TWO LIWG4W",
        "makeId": 51
      },
      {
        "id": 1518,
        "name": "PLEO CUSTOM",
        "makeId": 51
      },
      {
        "id": 1519,
        "name": "AERIO",
        "makeId": 53
      },
      {
        "id": 1520,
        "name": "CARA",
        "makeId": 53
      },
      {
        "id": 1521,
        "name": "CRUZE",
        "makeId": 53
      },
      {
        "id": 1522,
        "name": "CULTUS",
        "makeId": 53
      },
      {
        "id": 1523,
        "name": "CULTUS CRESCENT",
        "makeId": 53
      },
      {
        "id": 1524,
        "name": "CULTUS WAGON",
        "makeId": 53
      },
      {
        "id": 1525,
        "name": "EVERY LANDY",
        "makeId": 53
      },
      {
        "id": 1526,
        "name": "CRANE",
        "makeId": 71
      },
      {
        "id": 1527,
        "name": "5D4W",
        "makeId": 56
      },
      {
        "id": 1528,
        "name": "AVALON",
        "makeId": 56
      },
      {
        "id": 1529,
        "name": "AVENSIS",
        "makeId": 56
      },
      {
        "id": 1530,
        "name": "CAMROAD 2D",
        "makeId": 56
      },
      {
        "id": 1531,
        "name": "CAVALIER",
        "makeId": 56
      },
      {
        "id": 1532,
        "name": "CELICA LIFT BACK",
        "makeId": 56
      },
      {
        "id": 1533,
        "name": "COMFORT",
        "makeId": 56
      },
      {
        "id": 1534,
        "name": "COROLLA II",
        "makeId": 56
      },
      {
        "id": 1535,
        "name": "CROEN CROSSOVER",
        "makeId": 56
      },
      {
        "id": 1536,
        "name": "CROWN ATHLETE",
        "makeId": 56
      },
      {
        "id": 1537,
        "name": "CROWN VAN",
        "makeId": 56
      },
      {
        "id": 1538,
        "name": "CURREN",
        "makeId": 56
      },
      {
        "id": 1539,
        "name": "DYNA TRUCK 4D4W",
        "makeId": 56
      },
      {
        "id": 1540,
        "name": "DYNA VAN",
        "makeId": 56
      },
      {
        "id": 1541,
        "name": "GR COROLLA",
        "makeId": 56
      },
      {
        "id": 1542,
        "name": "HIACE KOMYU4D4W",
        "makeId": 56
      },
      {
        "id": 1543,
        "name": "HILUX SAF5D4W",
        "makeId": 56
      },
      {
        "id": 1544,
        "name": "KLUGER HYBRID",
        "makeId": 56
      },
      {
        "id": 1545,
        "name": "KO-S MANY BIG VAN 2D",
        "makeId": 56
      },
      {
        "id": 1546,
        "name": "LITE ACE WAGON",
        "makeId": 56
      },
      {
        "id": 1547,
        "name": "MARK II QUALIS",
        "makeId": 56
      },
      {
        "id": 1548,
        "name": "MEGA CRUISER",
        "makeId": 56
      },
      {
        "id": 1549,
        "name": "NADIA",
        "makeId": 56
      },
      {
        "id": 1550,
        "name": "QUICK DELIVERY",
        "makeId": 56
      },
      {
        "id": 1551,
        "name": "REGIUS ACE",
        "makeId": 56
      },
      {
        "id": 1552,
        "name": "SEQUOIA",
        "makeId": 56
      },
      {
        "id": 1553,
        "name": "SEQUOIA 5D4W",
        "makeId": 56
      },
      {
        "id": 1554,
        "name": "SERA",
        "makeId": 56
      },
      {
        "id": 1555,
        "name": "SI-PODO",
        "makeId": 56
      },
      {
        "id": 1556,
        "name": "SUN GYO DETACHED KAI",
        "makeId": 56
      },
      {
        "id": 1557,
        "name": "TACOMA 4D4W",
        "makeId": 56
      },
      {
        "id": 1558,
        "name": "TOWN ACE TRUCK 4W",
        "makeId": 56
      },
      {
        "id": 1559,
        "name": "TOWN ACE WAGON",
        "makeId": 56
      },
      {
        "id": 1560,
        "name": "TOYOACE TRUCK",
        "makeId": 56
      },
      {
        "id": 1561,
        "name": "TOYOACE VAN",
        "makeId": 56
      },
      {
        "id": 1562,
        "name": "TRUCK",
        "makeId": 56
      },
      {
        "id": 1563,
        "name": "VELLFIRE HYBRID",
        "makeId": 56
      },
      {
        "id": 1564,
        "name": "VENZA",
        "makeId": 56
      },
      {
        "id": 1565,
        "name": "CHIMAERA",
        "makeId": 72
      },
      {
        "id": 1566,
        "name": "GRIFFITH",
        "makeId": 72
      },
      {
        "id": 1567,
        "name": "TUSCAN",
        "makeId": 72
      },
      {
        "id": 1568,
        "name": "2D",
        "makeId": 57
      },
      {
        "id": 1569,
        "name": "CORRADO",
        "makeId": 57
      },
      {
        "id": 1570,
        "name": "CROSS GOLF",
        "makeId": 57
      },
      {
        "id": 1571,
        "name": "EUROPEAN VAN",
        "makeId": 57
      },
      {
        "id": 1572,
        "name": "GOLF 5D4W",
        "makeId": 57
      },
      {
        "id": 1573,
        "name": "GOLF GTI",
        "makeId": 57
      },
      {
        "id": 1574,
        "name": "GOLF R VALIANT",
        "makeId": 57
      },
      {
        "id": 1575,
        "name": "ID.4",
        "makeId": 57
      },
      {
        "id": 1576,
        "name": "KARMANN-GHIA CP",
        "makeId": 57
      },
      {
        "id": 1577,
        "name": "LUPO",
        "makeId": 57
      },
      {
        "id": 1578,
        "name": "NEW BEETLE CABRIOLET",
        "makeId": 57
      },
      {
        "id": 1579,
        "name": "OTHER",
        "makeId": 57
      },
      {
        "id": 1580,
        "name": "PASSAT ALLTRACK",
        "makeId": 57
      },
      {
        "id": 1581,
        "name": "PASSAT CC",
        "makeId": 57
      },
      {
        "id": 1582,
        "name": "RUPO",
        "makeId": 57
      },
      {
        "id": 1583,
        "name": "THE BEETLE CABRIOLET",
        "makeId": 57
      },
      {
        "id": 1584,
        "name": "TYPE 1",
        "makeId": 57
      },
      {
        "id": 1585,
        "name": "TYPE 2",
        "makeId": 57
      },
      {
        "id": 1586,
        "name": "VANAGON",
        "makeId": 57
      },
      {
        "id": 1587,
        "name": "850 SERIES",
        "makeId": 58
      },
      {
        "id": 1588,
        "name": "940 ESTATE",
        "makeId": 58
      },
      {
        "id": 1589,
        "name": "940 SERIES",
        "makeId": 58
      },
      {
        "id": 1590,
        "name": "960 SERIES",
        "makeId": 58
      },
      {
        "id": 1591,
        "name": "C70 SERIES",
        "makeId": 58
      },
      {
        "id": 1592,
        "name": "OTHER",
        "makeId": 58
      },
      {
        "id": 1593,
        "name": "S70",
        "makeId": 58
      },
      {
        "id": 1594,
        "name": "S90",
        "makeId": 58
      },
      {
        "id": 1595,
        "name": "OTHER",
        "makeId": 59
      },
      {
        "id": 1596,
        "name": "S8 PLUS",
        "makeId": 3
      },
      {
        "id": 1597,
        "name": "X5 M",
        "makeId": 5
      },
      {
        "id": 1598,
        "name": "XTS",
        "makeId": 7
      },
      {
        "id": 1599,
        "name": "575",
        "makeId": 15
      },
      {
        "id": 1600,
        "name": "FO F250",
        "makeId": 17
      },
      {
        "id": 1601,
        "name": "BUS 1D",
        "makeId": 19
      },
      {
        "id": 1602,
        "name": "RAINBOW",
        "makeId": 19
      },
      {
        "id": 1603,
        "name": "I30",
        "makeId": 65
      },
      {
        "id": 1604,
        "name": "BUS",
        "makeId": 25
      },
      {
        "id": 1605,
        "name": "KLAIJ RAN GLA",
        "makeId": 27
      },
      {
        "id": 1606,
        "name": "EXIGE",
        "makeId": 35
      },
      {
        "id": 1607,
        "name": "EFINI MS-8",
        "makeId": 37
      },
      {
        "id": 1608,
        "name": "SCRUM (AUTOZAM)",
        "makeId": 37
      },
      {
        "id": 1609,
        "name": "TITAN TRUCK 2D4W",
        "makeId": 37
      },
      {
        "id": 1610,
        "name": "AMG GL",
        "makeId": 38
      },
      {
        "id": 1611,
        "name": "MB EQS SUV4W",
        "makeId": 38
      },
      {
        "id": 1612,
        "name": "CAB TRUCK 4W",
        "makeId": 40
      },
      {
        "id": 1613,
        "name": "FUSO AERO MIDI",
        "makeId": 40
      },
      {
        "id": 1614,
        "name": "ATLAS TRUCK 2D4W",
        "makeId": 42
      },
      {
        "id": 1615,
        "name": "STANZA",
        "makeId": 42
      },
      {
        "id": 1616,
        "name": "BLIJI",
        "makeId": 44
      },
      {
        "id": 1617,
        "name": "BREAKER",
        "makeId": 44
      },
      {
        "id": 1618,
        "name": "CRANE",
        "makeId": 44
      },
      {
        "id": 1619,
        "name": "DUNLOP",
        "makeId": 44
      },
      {
        "id": 1620,
        "name": "ESMAK",
        "makeId": 44
      },
      {
        "id": 1621,
        "name": "FJI",
        "makeId": 44
      },
      {
        "id": 1622,
        "name": "FJII",
        "makeId": 44
      },
      {
        "id": 1623,
        "name": "HO KETSU",
        "makeId": 44
      },
      {
        "id": 1624,
        "name": "I-EN SYSTEM",
        "makeId": 44
      },
      {
        "id": 1625,
        "name": "JOHN TIA",
        "makeId": 44
      },
      {
        "id": 1626,
        "name": "MANY I YELLOW .K",
        "makeId": 44
      },
      {
        "id": 1627,
        "name": "MARU YAMA",
        "makeId": 44
      },
      {
        "id": 1628,
        "name": "MATSU UNDER",
        "makeId": 44
      },
      {
        "id": 1629,
        "name": "MEYER",
        "makeId": 44
      },
      {
        "id": 1630,
        "name": "MITSUI ERA",
        "makeId": 44
      },
      {
        "id": 1631,
        "name": "MOTO",
        "makeId": 44
      },
      {
        "id": 1632,
        "name": "NI HO N CAR RYOU",
        "makeId": 44
      },
      {
        "id": 1633,
        "name": "NI HO NTEN YELLOW",
        "makeId": 44
      },
      {
        "id": 1634,
        "name": "NI KEI",
        "makeId": 44
      },
      {
        "id": 1635,
        "name": "NITSUU",
        "makeId": 44
      },
      {
        "id": 1636,
        "name": "ONAN",
        "makeId": 44
      },
      {
        "id": 1637,
        "name": "OO MACHI",
        "makeId": 44
      },
      {
        "id": 1638,
        "name": "PRO TON",
        "makeId": 44
      },
      {
        "id": 1639,
        "name": "SAKATO",
        "makeId": 44
      },
      {
        "id": 1640,
        "name": "SHIKOKU",
        "makeId": 44
      },
      {
        "id": 1641,
        "name": "SMITHCO",
        "makeId": 44
      },
      {
        "id": 1642,
        "name": "STARCRAFT STARCRAFT",
        "makeId": 44
      },
      {
        "id": 1643,
        "name": "TENGEN",
        "makeId": 44
      },
      {
        "id": 1644,
        "name": "VAN ZAI",
        "makeId": 44
      },
      {
        "id": 1645,
        "name": "YAMAGCHI",
        "makeId": 44
      },
      {
        "id": 1646,
        "name": "YELLOW .TO",
        "makeId": 44
      },
      {
        "id": 1647,
        "name": "YUKEN",
        "makeId": 44
      },
      {
        "id": 1648,
        "name": "LI COVER",
        "makeId": 45
      },
      {
        "id": 1649,
        "name": "PORU MANY IKANWG4W",
        "makeId": 46
      },
      {
        "id": 1650,
        "name": "SPACIA CUSTOM 4W",
        "makeId": 53
      },
      {
        "id": 1651,
        "name": "BUS",
        "makeId": 56
      },
      {
        "id": 1652,
        "name": "SCION XB",
        "makeId": 56
      },
      {
        "id": 1653,
        "name": "OTHER",
        "makeId": 10
      },
      {
        "id": 1654,
        "name": "STS",
        "makeId": 7
      },
      {
        "id": 1655,
        "name": "GRAN MAX",
        "makeId": 12
      },
      {
        "id": 1656,
        "name": "NAVIGATOR",
        "makeId": 17
      },
      {
        "id": 1657,
        "name": "BLUE RIBBON",
        "makeId": 19
      },
      {
        "id": 1658,
        "name": "ENGINE",
        "makeId": 19
      },
      {
        "id": 1659,
        "name": "SELEGA",
        "makeId": 19
      },
      {
        "id": 1660,
        "name": "TODAY VAN",
        "makeId": 21
      },
      {
        "id": 1661,
        "name": "OO RATTLING OTHER",
        "makeId": 25
      },
      {
        "id": 1662,
        "name": "COUNTACH",
        "makeId": 66
      },
      {
        "id": 1663,
        "name": "AMG E CLASS",
        "makeId": 38
      },
      {
        "id": 1664,
        "name": "CLS SHOOTING BRAKE",
        "makeId": 38
      },
      {
        "id": 1665,
        "name": "MERCEDES -AMG",
        "makeId": 38
      },
      {
        "id": 1666,
        "name": "MERCEDES AMG",
        "makeId": 38
      },
      {
        "id": 1667,
        "name": "ENGINE",
        "makeId": 40
      },
      {
        "id": 1668,
        "name": "CRUE",
        "makeId": 42
      },
      {
        "id": 1669,
        "name": "ENGINE",
        "makeId": 42
      },
      {
        "id": 1670,
        "name": "ASTRA",
        "makeId": 43
      },
      {
        "id": 1671,
        "name": "BODY",
        "makeId": 44
      },
      {
        "id": 1672,
        "name": "ENGINE",
        "makeId": 44
      },
      {
        "id": 1673,
        "name": "GEAR",
        "makeId": 44
      },
      {
        "id": 1674,
        "name": "SEMI TRAILLER",
        "makeId": 44
      },
      {
        "id": 1675,
        "name": "TIRE SET",
        "makeId": 44
      },
      {
        "id": 1676,
        "name": "TRANSPORT SEMI TRAILLER",
        "makeId": 44
      },
      {
        "id": 1677,
        "name": "TRUCK BIHIN",
        "makeId": 44
      },
      {
        "id": 1678,
        "name": "YBR125",
        "makeId": 44
      },
      {
        "id": 1679,
        "name": "YUMBO",
        "makeId": 44
      },
      {
        "id": 1680,
        "name": "YUMBO B27",
        "makeId": 44
      },
      {
        "id": 1681,
        "name": "ENGINE",
        "makeId": 56
      },
      {
        "id": 1682,
        "name": "QUICKDELIVERY VAN",
        "makeId": 56
      },
      {
        "id": 1683,
        "name": "SPRINTER",
        "makeId": 38
      },
      {
        "id": 1684,
        "name": "OTHER",
        "makeId": 3
      },
      {
        "id": 1685,
        "name": "OTHER",
        "makeId": 2
      },
      {
        "id": 1686,
        "name": "CADE . RACK",
        "makeId": 64
      },
      {
        "id": 1687,
        "name": "CHEVROLET CAPTIVA",
        "makeId": 64
      },
      {
        "id": 1688,
        "name": "OTHER",
        "makeId": 64
      },
      {
        "id": 1689,
        "name": "VAMOS 0D",
        "makeId": 21
      },
      {
        "id": 1690,
        "name": "ENGINE",
        "makeId": 25
      },
      {
        "id": 1691,
        "name": "YUMBO",
        "makeId": 30
      },
      {
        "id": 1692,
        "name": "ECLIPSE CROSS PHEV",
        "makeId": 40
      },
      {
        "id": 1693,
        "name": "BUDDY",
        "makeId": 41
      },
      {
        "id": 1694,
        "name": "MAEDA CRANE",
        "makeId": 44
      },
      {
        "id": 1695,
        "name": "4",
        "makeId": 47
      },
      {
        "id": 1696,
        "name": "KANG",
        "makeId": 47
      },
      {
        "id": 1697,
        "name": "ROADSTER",
        "makeId": 50
      },
      {
        "id": 1698,
        "name": "VENT",
        "makeId": 57
      },
      {
        "id": 1699,
        "name": "C40",
        "makeId": 58
      },
      {
        "id": 1700,
        "name": "DBS",
        "makeId": 2
      },
      {
        "id": 1701,
        "name": "SQ2",
        "makeId": 3
      },
      {
        "id": 1702,
        "name": "CVL C1500",
        "makeId": 9
      },
      {
        "id": 1703,
        "name": "VOYAGE",
        "makeId": 10
      },
      {
        "id": 1704,
        "name": "ASH ZETOKAGO4D4W",
        "makeId": 12
      },
      {
        "id": 1705,
        "name": "DELTA VAN",
        "makeId": 12
      },
      {
        "id": 1706,
        "name": "MIRA WORKS RU-V2D",
        "makeId": 12
      },
      {
        "id": 1707,
        "name": "ABARTH PUNTO EVO",
        "makeId": 16
      },
      {
        "id": 1708,
        "name": "LINCOLN CONTINENTAL",
        "makeId": 17
      },
      {
        "id": 1709,
        "name": "SPECTRON",
        "makeId": 17
      },
      {
        "id": 1710,
        "name": "CADILLAC STS",
        "makeId": 18
      },
      {
        "id": 1711,
        "name": "CHEVROLET CORVETTE",
        "makeId": 18
      },
      {
        "id": 1712,
        "name": "BLUERIBBON",
        "makeId": 19
      },
      {
        "id": 1713,
        "name": "DUTRO 2D",
        "makeId": 19
      },
      {
        "id": 1714,
        "name": "CIVIC SHUTTLE",
        "makeId": 21
      },
      {
        "id": 1715,
        "name": "N360",
        "makeId": 21
      },
      {
        "id": 1716,
        "name": "TN360",
        "makeId": 21
      },
      {
        "id": 1717,
        "name": "BI SILVER",
        "makeId": 25
      },
      {
        "id": 1718,
        "name": "JOURNEY BUS",
        "makeId": 25
      },
      {
        "id": 1719,
        "name": "PIAZZA",
        "makeId": 25
      },
      {
        "id": 1720,
        "name": "KLAIJ RAN GUL4W",
        "makeId": 27
      },
      {
        "id": 1721,
        "name": "LR DISCOVER 4W",
        "makeId": 32
      },
      {
        "id": 1722,
        "name": "BONGO WAGON",
        "makeId": 37
      },
      {
        "id": 1723,
        "name": "COSMO",
        "makeId": 37
      },
      {
        "id": 1724,
        "name": "EUNOS 800",
        "makeId": 37
      },
      {
        "id": 1725,
        "name": "LANTIS",
        "makeId": 37
      },
      {
        "id": 1726,
        "name": "MX-6",
        "makeId": 37
      },
      {
        "id": 1727,
        "name": "SCRUM TRUCK 2D4W",
        "makeId": 37
      },
      {
        "id": 1728,
        "name": "TITAN DASI2D4W",
        "makeId": 37
      },
      {
        "id": 1729,
        "name": "C CLASS SPORTS COUPE",
        "makeId": 38
      },
      {
        "id": 1730,
        "name": "MB TRANCE P",
        "makeId": 38
      },
      {
        "id": 1731,
        "name": "AERO E-S",
        "makeId": 40
      },
      {
        "id": 1732,
        "name": "CHALLENGER",
        "makeId": 40
      },
      {
        "id": 1733,
        "name": "TOPPO BJ VAN",
        "makeId": 40
      },
      {
        "id": 1734,
        "name": "OTHER",
        "makeId": 41
      },
      {
        "id": 1735,
        "name": "RYUGI WAGON",
        "makeId": 41
      },
      {
        "id": 1736,
        "name": "CEDRIC VAN",
        "makeId": 42
      },
      {
        "id": 1737,
        "name": "FRONTIER",
        "makeId": 42
      },
      {
        "id": 1738,
        "name": "LUCINO",
        "makeId": 42
      },
      {
        "id": 1739,
        "name": "SKYLINE STAY SI",
        "makeId": 42
      },
      {
        "id": 1740,
        "name": "BLAZE SMART EV",
        "makeId": 44
      },
      {
        "id": 1741,
        "name": "BLUE BA",
        "makeId": 44
      },
      {
        "id": 1742,
        "name": "CANTER",
        "makeId": 44
      },
      {
        "id": 1743,
        "name": "DS DS3 BLACK BE TON",
        "makeId": 44
      },
      {
        "id": 1744,
        "name": "ELF",
        "makeId": 44
      },
      {
        "id": 1745,
        "name": "HARLEY DAVIDSON",
        "makeId": 44
      },
      {
        "id": 1746,
        "name": "HIGH PLACE",
        "makeId": 44
      },
      {
        "id": 1747,
        "name": "HOWE EISAIZOU",
        "makeId": 44
      },
      {
        "id": 1748,
        "name": "KE- MANY IS M SUPER 7",
        "makeId": 44
      },
      {
        "id": 1749,
        "name": "LF",
        "makeId": 44
      },
      {
        "id": 1750,
        "name": "NANSEI CRANE",
        "makeId": 44
      },
      {
        "id": 1751,
        "name": "SEADOO",
        "makeId": 44
      },
      {
        "id": 1752,
        "name": "SHINMAYWA CRANE",
        "makeId": 44
      },
      {
        "id": 1753,
        "name": "UNISA FRONTIER 4W",
        "makeId": 44
      },
      {
        "id": 1754,
        "name": "RANGER",
        "makeId": 73
      },
      {
        "id": 1755,
        "name": "KANGOO BE BOP",
        "makeId": 47
      },
      {
        "id": 1756,
        "name": "DEFENDER",
        "makeId": 48
      },
      {
        "id": 1757,
        "name": "FREELANDER",
        "makeId": 48
      },
      {
        "id": 1758,
        "name": "MGF",
        "makeId": 48
      },
      {
        "id": 1759,
        "name": "OTHER",
        "makeId": 48
      },
      {
        "id": 1760,
        "name": "TI FENDER",
        "makeId": 48
      },
      {
        "id": 1761,
        "name": "CABRIO",
        "makeId": 50
      },
      {
        "id": 1762,
        "name": "REX",
        "makeId": 51
      },
      {
        "id": 1763,
        "name": "SOLTERRA",
        "makeId": 51
      },
      {
        "id": 1764,
        "name": "CARAVAN",
        "makeId": 53
      },
      {
        "id": 1765,
        "name": "OTHER",
        "makeId": 71
      },
      {
        "id": 1766,
        "name": "CARINA ED",
        "makeId": 56
      },
      {
        "id": 1767,
        "name": "CARINA VAN",
        "makeId": 56
      },
      {
        "id": 1768,
        "name": "CLASSIC",
        "makeId": 56
      },
      {
        "id": 1769,
        "name": "HILUX 4W",
        "makeId": 56
      },
      {
        "id": 1770,
        "name": "HIMEDIC",
        "makeId": 56
      },
      {
        "id": 1771,
        "name": "REGIUS A COMMUTER",
        "makeId": 56
      },
      {
        "id": 1772,
        "name": "TRUCK 4D4W",
        "makeId": 56
      },
      {
        "id": 1773,
        "name": "240 SERIES",
        "makeId": 58
      },
      {
        "id": 1774,
        "name": "OTHER",
        "makeId": 73
      },
      {
        "id": 1775,
        "name": "GTV",
        "makeId": 1
      },
      {
        "id": 1776,
        "name": "Q4 SPORTBACK E-TRON",
        "makeId": 3
      },
      {
        "id": 1777,
        "name": "SRX",
        "makeId": 7
      },
      {
        "id": 1778,
        "name": "CVL SILVER LADO2D",
        "makeId": 9
      },
      {
        "id": 1779,
        "name": "PT CRUISER CABRIO",
        "makeId": 10
      },
      {
        "id": 1780,
        "name": "MIRA TR-XX",
        "makeId": 12
      },
      {
        "id": 1781,
        "name": "BARCHETTA",
        "makeId": 16
      },
      {
        "id": 1782,
        "name": "FOCUS",
        "makeId": 17
      },
      {
        "id": 1783,
        "name": "J100 TRUCK",
        "makeId": 17
      },
      {
        "id": 1784,
        "name": "CADILLAC CTS",
        "makeId": 18
      },
      {
        "id": 1785,
        "name": "CHEVROLET CAMARO",
        "makeId": 18
      },
      {
        "id": 1786,
        "name": "CLARITY",
        "makeId": 21
      },
      {
        "id": 1787,
        "name": "WR-V",
        "makeId": 21
      },
      {
        "id": 1788,
        "name": "VEHICROSS",
        "makeId": 25
      },
      {
        "id": 1789,
        "name": "DIGNITY",
        "makeId": 40
      },
      {
        "id": 1790,
        "name": "LAMBDA",
        "makeId": 40
      },
      {
        "id": 1791,
        "name": "DATSUN SUNNY",
        "makeId": 42
      },
      {
        "id": 1792,
        "name": "HOMY CORCH",
        "makeId": 42
      },
      {
        "id": 1793,
        "name": "SKYLINE WAGON",
        "makeId": 42
      },
      {
        "id": 1794,
        "name": "650S SPY DA",
        "makeId": 44
      },
      {
        "id": 1795,
        "name": "SI- MARINE TRAILER",
        "makeId": 44
      },
      {
        "id": 1796,
        "name": "SUPER CUB",
        "makeId": 44
      },
      {
        "id": 1797,
        "name": "TUKTUK",
        "makeId": 44
      },
      {
        "id": 1798,
        "name": "Z750FX",
        "makeId": 44
      },
      {
        "id": 1799,
        "name": "WINDOW",
        "makeId": 47
      },
      {
        "id": 1800,
        "name": "LEVORG LAYBACK",
        "makeId": 51
      },
      {
        "id": 1801,
        "name": "CYNOS",
        "makeId": 56
      },
      {
        "id": 1802,
        "name": "TOYOPE CORONA MARK 2",
        "makeId": 56
      },
      {
        "id": 1803,
        "name": "CARAVELLE",
        "makeId": 57
      }
    ]

    await modelRepository.save(models);
    console.log('Models have been seeded successfully.');
  }
}
