import { AppDataSource } from "../../data-source";
import { CreateCountry } from "./country.seed";
import { CreateAuctionSeed } from "./auction.seed";
import { CreateMakesSeed } from "./make.seed";
import { CreateModelsSeed } from "./model.seed";


async function runSeeders() {

   await AppDataSource.initialize();

  console.log('Database connection initialized.');

  // Запуск нескольких сидеров
  await new CreateCountry().run();
  console.log('Country have been seeded.');

  await new CreateAuctionSeed().run()
  console.log('Auction have been seeded.');

  await new CreateMakesSeed().run()
  console.log('Make have been seeded.');

  await new CreateModelsSeed().run()
  console.log('Model have been seeded.');

  await AppDataSource.destroy();
  console.log('Database connection closed.');


}
runSeeders().catch((error) => {
  console.error('Error while seeding:', error);
});