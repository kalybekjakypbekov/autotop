import { Seeder } from 'typeorm-seeding';

import { AppDataSource } from '../../data-source';
import { MakeEntity } from "../car/entity/make.entity";  // Подключение к источнику данных

export class CreateMakesSeed implements Seeder {
  public async run(): Promise<void> {
    const makeRepository = AppDataSource.getRepository(MakeEntity);

    const getMake =  await makeRepository.find()
    if(getMake.length){
      return undefined
    }

    const makes = [
      { id: 1, name: 'ALFAROMEO', carvector_make_id: 12 },
      { id: 2, name: 'ASTON MARTIN', carvector_make_id: 112 },
      { id: 3, name: 'AUDI', carvector_make_id: 13 },
      { id: 4, name: 'BENTLEY', carvector_make_id: 0 },
      { id: 5, name: 'BMW', carvector_make_id: 14 },
      { id: 6, name: 'BMW ALPINA', carvector_make_id: 0 },
      { id: 7, name: 'CADILLAC', carvector_make_id: 119 },
      { id: 8, name: 'CATERPILLAR', carvector_make_id: 0 },
      { id: 9, name: 'CHEVROLET', carvector_make_id: 0 },
      { id: 10, name: 'CHRYSLER', carvector_make_id: 15 },
      { id: 11, name: 'CITROEN', carvector_make_id: 16 },
      { id: 12, name: 'DAIHATSU', carvector_make_id: 9 },
      { id: 13, name: 'DAIMLER', carvector_make_id: 0 },
      { id: 14, name: 'DODGE', carvector_make_id: 124 },
      { id: 15, name: 'FERRARI', carvector_make_id: 126 },
      { id: 16, name: 'FIAT', carvector_make_id: 18 },
      { id: 17, name: 'FORD', carvector_make_id: 19 },
      { id: 18, name: 'GM', carvector_make_id: 20 },
      { id: 19, name: 'HINO', carvector_make_id: 21 },
      { id: 20, name: 'HITACHI', carvector_make_id: 340 },
      { id: 21, name: 'HONDA', carvector_make_id: 5 },
      { id: 22, name: 'HUMMER', carvector_make_id: 129 },
      { id: 23, name: 'INFINITI', carvector_make_id: 0 },
      { id: 24, name: 'ISEKI', carvector_make_id: 341 },
      { id: 25, name: 'ISUZU', carvector_make_id: 8 },
      { id: 26, name: 'JAGUAR', carvector_make_id: 132 },
      { id: 27, name: 'JEEP', carvector_make_id: 0 },
      { id: 28, name: 'KAWASAKI', carvector_make_id: 0 },
      { id: 29, name: 'KOBELCO', carvector_make_id: 342 },
      { id: 30, name: 'KOMATSU', carvector_make_id: 343 },
      { id: 31, name: 'KUBOTA', carvector_make_id: 344 },
      { id: 32, name: 'LAND ROVER', carvector_make_id: 0 },
      { id: 33, name: 'LEXUS', carvector_make_id: 23 },
      { id: 34, name: 'LINCOLN', carvector_make_id: 0 },
      { id: 35, name: 'LOTUS', carvector_make_id: 0 },
      { id: 36, name: 'MASERATI', carvector_make_id: 137 },
      { id: 37, name: 'MAZDA', carvector_make_id: 3 },
      { id: 38, name: 'MERCEDES BENZ', carvector_make_id: 24 },
      { id: 39, name: 'MINI', carvector_make_id: 0 },
      { id: 40, name: 'MITSUBISHI', carvector_make_id: 4 },
      { id: 41, name: 'MITSUOKA', carvector_make_id: 10 },
      { id: 42, name: 'NISSAN', carvector_make_id: 2 },
      { id: 43, name: 'OPEL', carvector_make_id: 25 },
      { id: 44, name: 'OTHERS', carvector_make_id: 98 },
      { id: 45, name: 'PEUGEOT', carvector_make_id: 26 },
      { id: 46, name: 'PORSCHE', carvector_make_id: 148 },
      { id: 47, name: 'RENAULT', carvector_make_id: 27 },
      { id: 48, name: 'ROVER', carvector_make_id: 28 },
      { id: 49, name: 'SAAB', carvector_make_id: 150 },
      { id: 50, name: 'SMART', carvector_make_id: 291 },
      { id: 51, name: 'SUBARU', carvector_make_id: 7 },
      { id: 52, name: 'SUMITOMO', carvector_make_id: 345 },
      { id: 53, name: 'SUZUKI', carvector_make_id: 6 },
      { id: 54, name: 'TCM', carvector_make_id: 30 },
      { id: 55, name: 'TESLA', carvector_make_id: 348 },
      { id: 56, name: 'TOYOTA', carvector_make_id: 1 },
      { id: 57, name: 'VOLKSWAGEN', carvector_make_id: 31 },
      { id: 58, name: 'VOLVO', carvector_make_id: 32 },
      { id: 59, name: 'YAMAHA', carvector_make_id: 326 },
      { id: 60, name: 'YANMAR', carvector_make_id: 347 },
      { id: 61, name: 'ACURA', carvector_make_id: 0 },
      { id: 62, name: 'AUSTIN', carvector_make_id: 0 },
      { id: 63, name: 'BUICK', carvector_make_id: 0 },
      { id: 64, name: 'GMC', carvector_make_id: 0 },
      { id: 65, name: 'HYUNDAI', carvector_make_id: 130 },
      { id: 66, name: 'LAMBORGHINI', carvector_make_id: 134 },
      { id: 67, name: 'LANCIA', carvector_make_id: 0 },
      { id: 68, name: 'MORGAN', carvector_make_id: 0 },
      { id: 69, name: 'PONTIAC', carvector_make_id: 0 },
      { id: 70, name: 'ROLLS ROYCE', carvector_make_id: 0 },
      { id: 71, name: 'TADANO', carvector_make_id: 346 },
      { id: 72, name: 'TVR', carvector_make_id: 0 },
      { id: 73, name: 'RANGER', carvector_make_id: 0 }
    ];

    // Вставляем данные в таблицу `makes`
    await makeRepository.save(makes);

    console.log('Makes have been seeded successfully.');
  }
}
