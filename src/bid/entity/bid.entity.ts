import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn, UpdateDateColumn
} from "typeorm";
import {UserEntity} from "../../user/entity/user.entity";
import {LotEntity} from "../../auction/entity/lot.entity";


enum group {
    A,
    B,
    C,
    D,
}

@Entity({name:"bids"})
export  class BidEntity extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    bid_price: number

    @Column()
    userId: number

    @Column()
    lotId: number

    @Column({nullable:true})
    auction_date: Date

    @Column({
        type:"enum",
        enum:group,
        default: group.A
    })
    group: group

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @ManyToOne(() => UserEntity,(user) => user.bids)
    @JoinColumn({name:"userId", referencedColumnName:"id"})
    user: UserEntity

    @ManyToOne(()=>LotEntity, (auction) => auction.bids)
    // @JoinColumn({name:"lotId", referencedColumnName:"id"})
    lot: LotEntity

}