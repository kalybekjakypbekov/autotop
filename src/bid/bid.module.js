"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.BidModule = void 0;
var common_1 = require("@nestjs/common");
var bid_service_1 = require("./bid.service");
var bid_controller_1 = require("./bid.controller");
var aution_module_1 = require("../auction/aution.module");
var typeorm_1 = require("@nestjs/typeorm");
var bid_entity_1 = require("./entity/bid.entity");
var user_module_1 = require("../user/user.module");
var BidModule = /** @class */ (function () {
    function BidModule() {
    }
    BidModule = __decorate([
        (0, common_1.Module)({
            imports: [aution_module_1.AutionModule, typeorm_1.TypeOrmModule.forFeature([bid_entity_1.BidEntity]), user_module_1.UserModule],
            providers: [bid_service_1.BidService],
            controllers: [bid_controller_1.BidController]
        })
    ], BidModule);
    return BidModule;
}());
exports.BidModule = BidModule;
