import {IsNotEmpty} from "class-validator";


export  class CreateBidDto {

    @IsNotEmpty()
    bid_price : number

    @IsNotEmpty()
    lotId: string

}