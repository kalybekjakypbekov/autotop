"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.BidService = void 0;
var common_1 = require("@nestjs/common");
var typeorm_1 = require("@nestjs/typeorm");
var bid_entity_1 = require("./entity/bid.entity");
var BidService = /** @class */ (function () {
    function BidService(bidEntity, auctionService, userService) {
        this.bidEntity = bidEntity;
        this.auctionService = auctionService;
        this.userService = userService;
    }
    BidService.prototype.createBids = function (bid, currentUser) {
        return __awaiter(this, void 0, void 0, function () {
            var lotId, checkingLot, balance, maxBid, createBid;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        lotId = bid.lotId;
                        return [4 /*yield*/, this.auctionService.getById(lotId, currentUser)];
                    case 1:
                        checkingLot = _a.sent();
                        if (!checkingLot) {
                            throw new common_1.NotFoundException("Lot with ID ".concat(lotId, " not found"));
                        }
                        if (checkingLot.end_time < new Date()) {
                            throw new common_1.ConflictException('The auction has already ended, you cannot place a bid');
                        }
                        return [4 /*yield*/, this.userService.findOne(currentUser.username)];
                    case 2:
                        balance = _a.sent();
                        if (balance.balance <= 0) {
                            throw new common_1.ConflictException('You don\'t have enough deposit to participate in the auction.');
                        }
                        maxBid = {
                            bid_price: 0
                        };
                        if (checkingLot.bids.length) {
                            maxBid = checkingLot.bids.reduce(function (prev, current) {
                                return (prev.bid_price > current.bid_price) ? prev : current;
                            });
                        }
                        if (checkingLot.start_price >= bid.bid_price || maxBid.bid_price >= bid.bid_price) {
                            throw new common_1.ConflictException('The bid must be higher than the starting bid or higher than the previous one.');
                        }
                        return [4 /*yield*/, this.bidEntity.save({
                                bid_price: bid.bid_price,
                                userId: currentUser.userId,
                                lotId: checkingLot.id,
                                auction_date: checkingLot.end_time
                            })];
                    case 3:
                        createBid = _a.sent();
                        return [2 /*return*/, createBid];
                }
            });
        });
    };
    BidService.prototype.myBids = function (params, currentUser) {
        return __awaiter(this, void 0, void 0, function () {
            var bids;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.auctionService.search(params, currentUser)];
                    case 1:
                        bids = _a.sent();
                        return [2 /*return*/, bids];
                }
            });
        });
    };
    BidService = __decorate([
        (0, common_1.Injectable)(),
        __param(0, (0, typeorm_1.InjectRepository)(bid_entity_1.BidEntity))
    ], BidService);
    return BidService;
}());
exports.BidService = BidService;
