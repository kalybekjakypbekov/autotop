import {ConflictException, Injectable, NotFoundException} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {BidEntity} from "./entity/bid.entity";
import {Repository} from "typeorm";
import {AutionService} from "../auction/aution.service";
import {CreateBidDto} from "./dto/create-bid.dto";
import {SearchVehiclesDto} from "../auction/dto/search-vehicles.dto";
import {UserService} from "../user/user.service";
import { VehicleService } from "../vehicle/vehicle.service";
import { CarService } from "../car/car.service";
import { createJestPreset } from "ts-jest";
import { FilterOperator, paginate, Paginated, PaginateQuery } from "nestjs-paginate";
import { OrderEntity } from "../order/entity/order.entity";

@Injectable()
export class BidService {

    constructor(@InjectRepository(BidEntity) private  readonly  bidEntity: Repository<BidEntity>,
                private readonly  auctionService:AutionService,
                private readonly  userService: UserService,
                private readonly vehicleService: VehicleService,
                private readonly carService: CarService
                ) {
    }

   async  createBids (bid: CreateBidDto, currentUser){

        const dataString = bid.lotId.split(":")
        const aj_id = dataString[0]
        const auction = dataString[1]
        const lot_no  = dataString[2]
        const dataSearchCR = {
          auction: auction,
          lot_no: lot_no
        }


        const checkingLot = await this.auctionService.getById(aj_id, currentUser,dataSearchCR);
        const makeAndModel = await this.carService.getByModel(checkingLot.vehicle.model.name)

        checkingLot.vehicle.model = makeAndModel
        checkingLot.vehicle.year = new Date(checkingLot.vehicle.year)
        checkingLot.vehicle.makeId = makeAndModel.makeId
        const createAndUpdateVehicle = await this.vehicleService.save(checkingLot.vehicle);

        checkingLot.vehicleId = createAndUpdateVehicle.id
        checkingLot.vehicle = createAndUpdateVehicle

        const createAndUpdateLot = await this.auctionService.createOrUpdateLot(checkingLot)


        if(!createAndUpdateLot) {
            throw new NotFoundException(`Lot with ID ${lot_no} not found`);
        }

       if(createAndUpdateLot.end_time < new Date()){
           throw new ConflictException('The auction has already ended, you cannot place a bid');
       }


       const balance = await this.userService.findOne(currentUser.username)

       if(balance.balance <= 0) {
           throw new ConflictException('You don\'t have enough deposit to participate in the auction.');
       }

       let maxBid = {
           bid_price: 0
       };

       if (createAndUpdateLot.bids.length){
           maxBid = createAndUpdateLot.bids.reduce((prev, current) => {
               return (prev.bid_price > current.bid_price) ? prev : current;
           });
       }

        if(checkingLot.start_price >= bid.bid_price || maxBid.bid_price >= bid.bid_price ) {
            throw new ConflictException('The bid must be higher than the starting bid or higher than the previous one.');
        }


        const createBid = await this.bidEntity.save({
            bid_price: bid.bid_price,
            userId: currentUser.userId,
            lotId: createAndUpdateLot.id,
            auction_date: checkingLot.end_time
        })

        return createBid
   }


   async myBids (params: SearchVehiclesDto,currentUser){
        const bids  = await this.auctionService.search(params,currentUser)
        return bids
   }

   async  test(){
      const queryBuilder = await this.bidEntity.createQueryBuilder('bid')
        .innerJoinAndSelect('bid.lot' , 'lot')

      return queryBuilder;
   }


  async pagination(query: PaginateQuery): Promise<Paginated<BidEntity>> {

    const paginatedResult = await paginate(query, this.bidEntity, {
      relations:['user', 'lot','lot.vehicle', 'lot.vehicle.model', 'lot.vehicle.model.make', 'lot.auction'],
      sortableColumns: ['id','bid_price','auction_date','user.last_name','user.first_name', 'lot.aj_id', 'lot.vehicle.id'],
      searchableColumns: ['id','bid_price','auction_date','user.last_name','user.first_name', 'lot.aj_id', 'lot.vehicle.id'],
      maxLimit: 20,
      defaultLimit: 20,
      filterableColumns: {
        id: true,
        bid_price: true,
        'lot.aj_id': true,
        'lot.vehicle.id': true,
        "user.last_name": [FilterOperator.ILIKE],
        "user.first_name": [FilterOperator.ILIKE],
        auction_date: [FilterOperator.BTW]
      }
    })
    return paginatedResult;

  }


}
