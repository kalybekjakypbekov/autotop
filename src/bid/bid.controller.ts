import {Body, Controller, Get, Post, Query, UseGuards} from '@nestjs/common';
import {BidService} from "./bid.service";
import {CreateBidDto} from "./dto/create-bid.dto";
import {JwtAuthGuard} from "../auth/guard/jwt-auth.guard";
import {CurrentUser} from "../auth/decorators/current-user.decorator";
import {SearchVehiclesDto} from "../auction/dto/search-vehicles.dto";
import { Paginate, Paginated, PaginateQuery } from "nestjs-paginate";
import { OrderEntity } from "../order/entity/order.entity";
import { BidEntity } from "./entity/bid.entity";

@UseGuards(JwtAuthGuard)
@Controller('bid')
export class BidController {

    constructor(private  readonly  bidService:BidService) {
    }

    @Post('/')
    async createBid(@Body() createBidDto: CreateBidDto, @CurrentUser() currentUser){
            return await this.bidService.createBids(createBidDto, currentUser)
    }

    @Get('/mybids')
    async myBids(@Query() params: SearchVehiclesDto ,@CurrentUser() currentUser){
            return await this.bidService.myBids(params,currentUser)
    }


  @Get('/test')
  async test(@Query() params: SearchVehiclesDto ,@CurrentUser() currentUser){
    return await this.bidService.test()
  }

  @Get('/list')
  public async list(@Paginate() query: PaginateQuery): Promise<Paginated<BidEntity>> {
    return this.bidService.pagination(query)
  }

}
