import { Module } from '@nestjs/common';
import { BidService } from './bid.service';
import { BidController } from './bid.controller';
import {AutionModule} from "../auction/aution.module";
import {TypeOrmModule} from "@nestjs/typeorm";
import {BidEntity} from "./entity/bid.entity";
import {UserModule} from "../user/user.module";
import { VehicleModule } from "../vehicle/vehicle.module";
import { VehicleService } from "../vehicle/vehicle.service";
import { CarModule } from "../car/car.module";

@Module({
  imports:[AutionModule,TypeOrmModule.forFeature([BidEntity]),UserModule,VehicleModule,CarModule],
  providers: [BidService],
  controllers: [BidController],

})
export class BidModule {}
