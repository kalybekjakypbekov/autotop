import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn, UpdateDateColumn
} from "typeorm";
import {VehicleEntity} from "../../vehicle/entity/vehicle.entity";
import {UserEntity} from "../../user/entity/user.entity";
import {ShipEntity} from "../../ship/entity/ship.entity";
import {ConsigneeEntity} from "../../consignee/entity/consignee.entity";
import {DocumentEntity} from "../../document/entity/document.entity";

@Entity({name:"purchases"})
export  class PurchasesEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    price: number

    @Column()
    vehicle_id: number

    @Column()
    user_id: number

    @Column({nullable:true})
    ship_id: number

    @Column({nullable:true})
    consignee_id: number

    @Column({nullable:true})
    document_id: number

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @ManyToOne(()=>UserEntity, (user)=>user.purchases)
    @JoinColumn({name:"user_id", referencedColumnName:"id"})
    user: UserEntity

    @OneToOne(()=> VehicleEntity, vehicle => vehicle.purchases)
    @JoinColumn({name:"vehicle_id", referencedColumnName:"id"})
    vehicle: VehicleEntity

    @OneToOne(() => ShipEntity, (ship) => ship.purchase)
    @JoinColumn({name:"ship_id", referencedColumnName:"id"})
    ship: ShipEntity

    @OneToOne(() => ConsigneeEntity, (consignee) => consignee.consignee)
    @JoinColumn({name:"consignee_id", referencedColumnName:"id"})
    consignee: ConsigneeEntity

    @OneToOne(() => DocumentEntity, (document) => document.purchase)
    @JoinColumn({name:"document_id", referencedColumnName:"id"})
    document: DocumentEntity
}