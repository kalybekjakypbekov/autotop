"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.PurchasesService = void 0;
var common_1 = require("@nestjs/common");
var typeorm_1 = require("@nestjs/typeorm");
var purchases_entity_1 = require("./entity/purchases.entity");
var PurchasesService = /** @class */ (function () {
    function PurchasesService(purchasesEntity) {
        this.purchasesEntity = purchasesEntity;
    }
    PurchasesService.prototype.list = function (params, currentUser) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, page, _b, limit, _c, sort, _d, order, skip, queryBuilder, start_year, end_year, start_year, end_year, _e, results, total, totalPages;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        _a = params.page, page = _a === void 0 ? 1 : _a, _b = params.limit, limit = _b === void 0 ? 10 : _b, _c = params.sort, sort = _c === void 0 ? 'created_at' : _c, _d = params.order, order = _d === void 0 ? 'ASC' : _d;
                        skip = (page - 1) * limit;
                        queryBuilder = this.purchasesEntity
                            .createQueryBuilder('purchase')
                            .leftJoinAndSelect('purchase.vehicle', 'vehicle')
                            .leftJoinAndSelect('vehicle.media', 'media')
                            .leftJoinAndSelect('vehicle.make', 'make')
                            .leftJoinAndSelect('vehicle.model', 'model')
                            .where('purchase.user_id = :userId', { userId: currentUser.userId });
                        if (params.make) {
                            queryBuilder.andWhere('make.name = :makeName', { makeName: params.make });
                        }
                        if (params.model) {
                            queryBuilder.andWhere('model.name = :modelName', { modelName: params.model });
                        }
                        if (params.chassis) {
                            queryBuilder.andWhere('vehicle.chassis = :chassis', { chassis: params.chassis });
                        }
                        if (params.ref) {
                            queryBuilder.andWhere('vehicle.ref_number = :ref_number', { ref_number: params.ref });
                        }
                        if (params.start_year && params.end_year) {
                            start_year = params.start_year;
                            end_year = params.end_year;
                            queryBuilder.andWhere("YEAR(vehicle.year) >= :start_year AND YEAR(vehicle.year) <= :end_year ", { start_year: start_year, end_year: end_year });
                        }
                        else if (params.start_year) {
                            start_year = params.start_year;
                            queryBuilder.andWhere("YEAR(vehicle.year) >= :start_year ", { start_year: start_year });
                        }
                        else if (params.end_year) {
                            end_year = params.end_year;
                            queryBuilder.andWhere("YEAR(vehicle.year) <= :end_year ", { end_year: end_year });
                        }
                        return [4 /*yield*/, queryBuilder
                                .orderBy("purchase.".concat(sort), order)
                                .skip(skip)
                                .take(limit)
                                .getManyAndCount()];
                    case 1:
                        _e = _f.sent(), results = _e[0], total = _e[1];
                        totalPages = Math.ceil(total / limit);
                        return [2 /*return*/, {
                                data: results,
                                page: page,
                                limit: limit,
                                total: total,
                                totalPages: totalPages
                            }];
                }
            });
        });
    };
    PurchasesService.prototype.getById = function (currentUser, id) {
        return __awaiter(this, void 0, void 0, function () {
            var purchase;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.purchasesEntity.findOne({
                            where: { id: id },
                            relations: ["vehicle", "vehicle.make", "vehicle.media", "vehicle.model", "ship", "document", "consignee"]
                        })];
                    case 1:
                        purchase = _a.sent();
                        if (purchase.user_id !== currentUser.userId) {
                            throw new common_1.NotFoundException("Purchases with id ".concat(id, " not found"));
                        }
                        return [2 /*return*/, purchase];
                }
            });
        });
    };
    PurchasesService.prototype.create = function (vehicle_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    PurchasesService = __decorate([
        (0, common_1.Injectable)(),
        __param(0, (0, typeorm_1.InjectRepository)(purchases_entity_1.PurchasesEntity))
    ], PurchasesService);
    return PurchasesService;
}());
exports.PurchasesService = PurchasesService;
