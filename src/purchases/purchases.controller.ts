import {Controller, Get, Param, Query, UseGuards} from '@nestjs/common';
import {JwtAuthGuard} from "../auth/guard/jwt-auth.guard";
import {CurrentUser} from "../auth/decorators/current-user.decorator";
import {PurchasesService} from "./purchases.service";
import {SearchVehiclesDto} from "../auction/dto/search-vehicles.dto";


@UseGuards(JwtAuthGuard)
@Controller('purchases')
export class PurchasesController {

    constructor(private  readonly  purchasesService: PurchasesService) {
    }
    @Get('/list')
    async list(@CurrentUser() currentUser, @Query() params: SearchVehiclesDto){

        return await this.purchasesService.list(params,currentUser)
    }

    @Get("/:id")
    async getById(@CurrentUser() currentUser, @Param("id") id){
        return await this.purchasesService.getById(currentUser, id)
    }



}
