import { Module } from '@nestjs/common';
import { PurchasesController } from './purchases.controller';
import { PurchasesService } from './purchases.service';
import {AutionModule} from "../auction/aution.module";
import {TypeOrmModule} from "@nestjs/typeorm";
import {PurchasesEntity} from "./entity/purchases.entity";


@Module({
  imports:[AutionModule, TypeOrmModule.forFeature([PurchasesEntity])],
  controllers: [PurchasesController],
  providers: [PurchasesService]
})
export class PurchasesModule {}
