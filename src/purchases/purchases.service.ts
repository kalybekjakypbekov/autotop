import {Injectable, NotFoundException} from '@nestjs/common';
import {AutionService} from "../auction/aution.service";
import {InjectRepository} from "@nestjs/typeorm";
import {PurchasesEntity} from "./entity/purchases.entity";
import {getRepository, Repository, SelectQueryBuilder} from "typeorm";

@Injectable()
export class PurchasesService {

    constructor(@InjectRepository(PurchasesEntity) private  readonly purchasesEntity: Repository<PurchasesEntity>) {
    }



   async list(params, currentUser) {
    const { page = 1, limit = 10, sort = 'created_at', order = 'ASC' } = params;
    const skip = (page - 1) * limit;

    const queryBuilder = this.purchasesEntity
        .createQueryBuilder('purchase')
        .leftJoinAndSelect('purchase.vehicle', 'vehicle')
        .leftJoinAndSelect('vehicle.media', 'media')
        .leftJoinAndSelect('vehicle.make', 'make')
        .leftJoinAndSelect('vehicle.model', 'model')
        .where('purchase.user_id = :userId', { userId: currentUser.userId });

    if (params.make) {
        queryBuilder.andWhere('make.name = :makeName', { makeName: params.make });
    }

    if (params.model) {
        queryBuilder.andWhere('model.name = :modelName', { modelName: params.model });
    }

    if (params.chassis) {
        queryBuilder.andWhere('vehicle.chassis = :chassis', { chassis: params.chassis });
    }

    if (params.ref) {
        queryBuilder.andWhere('vehicle.ref_number = :ref_number', { ref_number: params.ref });
    }


    if(params.start_year && params.end_year){
        let start_year =  params.start_year
        let end_year = params.end_year
        queryBuilder.andWhere("YEAR(vehicle.year) >= :start_year AND YEAR(vehicle.year) <= :end_year ",{start_year,end_year})
    }else if(params.start_year){
        let start_year =  params.start_year
        queryBuilder.andWhere("YEAR(vehicle.year) >= :start_year ",{start_year})
    }else if(params.end_year) {
        let end_year = params.end_year
        queryBuilder.andWhere("YEAR(vehicle.year) <= :end_year ",{end_year})
    }

    const [results, total] = await queryBuilder
        .orderBy(`purchase.${sort}`, order as 'ASC' | 'DESC')
        .skip(skip)
        .take(limit)
        .getManyAndCount();

    const totalPages = Math.ceil(total / limit);

    return {
        data: results,
        page: page,
        limit: limit,
        total: total,
        totalPages: totalPages,
    };
}

    async getById(currentUser, id: number){

        const purchase  =  await this.purchasesEntity.findOne({
            where: {id: id },
            relations:["vehicle", "vehicle.make","vehicle.media", "vehicle.model", "ship", "document", "consignee"]
        })

        if(purchase.user_id !== currentUser.userId){
            throw new NotFoundException(`Purchases with id ${id} not found`);
        }

        return purchase;
    }

    async create(vehicle_id: number){

    }

}
