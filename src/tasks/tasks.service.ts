import { Injectable } from '@nestjs/common';
import {Cron} from "@nestjs/schedule";
import {AutionService} from "../auction/aution.service";
import {VehicleService} from "../vehicle/vehicle.service";

@Injectable()
export class TasksService {

    constructor(private readonly auctionService: AutionService, private vehicleService: VehicleService) {}


     @Cron('*/1 * * * *') // Запускается каждую минуту. Измените расписание по необходимости.
    async handleAuctionEndings() {

         // const lot = await this.auctionService.inAuctionsVehicles()
         // console.log (lot)
    }




}
