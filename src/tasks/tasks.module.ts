import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import {ScheduleModule} from "@nestjs/schedule";
import {AutionModule} from "../auction/aution.module";
import {VehicleModule} from "../vehicle/vehicle.module";

@Module({
  imports: [ScheduleModule.forRoot(), AutionModule, VehicleModule],
  providers: [TasksService]
})
export class TasksModule {}
