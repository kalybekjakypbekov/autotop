import { Module } from '@nestjs/common';
import { PaymentController } from "./payment.controller";
import { PaymentService } from "./payment.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { PaymentEntity } from "./entity/payment.entity";

@Module({
  imports:[TypeOrmModule.forFeature([PaymentEntity])],
  providers: [PaymentService],
  controllers:[PaymentController],
  exports:[PaymentService]
})

export class PaymentModule {}
