import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { OrderEntity } from "../../order/entity/order.entity";

@Entity({name: "payment"})
export class PaymentEntity{

  @PrimaryGeneratedColumn('uuid')
  transactionId: number

  @Column()
  amount: number

  @Column()
  method: string

  @Column()
  order_id: number;

  @Column({type:'datetime'})
  date: Date

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(()=>OrderEntity, (order) => order.payment)
  @JoinColumn({name:"order_id", referencedColumnName:"id"})
  order: OrderEntity

}