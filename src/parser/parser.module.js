"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ParserModule = void 0;
var common_1 = require("@nestjs/common");
var parser_service_1 = require("./parser.service");
var parser_controller_1 = require("./parser.controller");
var typeorm_1 = require("@nestjs/typeorm");
var aj_auc_entity_1 = require("./entity/aj_auc.entity");
var aution_module_1 = require("../auction/aution.module");
var vehicle_module_1 = require("../vehicle/vehicle.module");
var car_module_1 = require("../car/car.module");
var media_module_1 = require("../media/media.module");
var ParserModule = /** @class */ (function () {
    function ParserModule() {
    }
    ParserModule = __decorate([
        (0, common_1.Module)({
            imports: [typeorm_1.TypeOrmModule.forFeature([aj_auc_entity_1.Aj_aucEntity]), aution_module_1.AutionModule, vehicle_module_1.VehicleModule, car_module_1.CarModule, media_module_1.MediaModule],
            providers: [parser_service_1.ParserService],
            controllers: [parser_controller_1.ParserController]
        })
    ], ParserModule);
    return ParserModule;
}());
exports.ParserModule = ParserModule;
