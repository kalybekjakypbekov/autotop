import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity('aj_auc')
export class Aj_aucEntity {
    @PrimaryColumn({ type: 'varchar', length: 20, collation: 'latin1_bin' })
    id: string;

    @Column({ type: 'varchar', length: 32, nullable: true, collation: 'latin1_bin' })
    auct_name: string;

    @Column({ type: 'int', width: 6, default: 0 })
    lot_num: number;

    @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
    auct_date: Date;

    @Column({ type: 'varchar', length: 13, nullable: false, default: '', collation: 'latin1_bin' })
    manuf_name: string;

    @Column({ type: 'int', width: 2, default: 0 })
    manuf_id: number;

    @Column({ type: 'varchar', length: 29, nullable: false, default: '', collation: 'latin1_bin' })
    model_name: string;

    @Column({ type: 'int', width: 4, default: 0 })
    model_id: number;

    @Column({ type: 'int', width: 4, default: 0 })
    year: number;

    @Column({ type: 'varchar', length: 150, nullable: false, default: '', collation: 'latin1_bin' })
    grade: string;

    @Column({ type: 'varchar', length: 128, nullable: false, default: '', collation: 'latin1_bin' })
    body: string;

    @Column({ type: 'int', width: 11, nullable: true })
    Vcc: number;

    @Column({ type: 'varchar', length: 30, nullable: false, default: '', collation: 'latin1_bin' })
    PW: string;

    @Column({ type: 'varchar', length: 20, nullable: false, default: '', collation: 'latin1_bin' })
    PRIV: string;

    @Column({ type: 'int', width: 6, default: 0 })
    mileage: number;

    @Column({ type: 'varchar', length: 100, nullable: false, collation: 'latin1_bin' })
    colour: string;

    @Column({ type: 'varchar', length: 120, nullable: false, default: '', collation: 'latin1_bin' })
    equipment: string;

    @Column({ type: 'varchar', length: 20, nullable: false, default: '', collation: 'latin1_bin' })
    transmission: string;

    @Column({ type: 'char', length: 1, nullable: false, default: '', collation: 'latin1_bin' })
    engine: string;

    @Column({ type: 'varchar', length: 16, nullable: false, default: '', collation: 'latin1_bin' })
    rate: string;

    @Column({ type: 'varchar', length: 80, nullable: false, default: '', collation: 'latin1_bin' })
    status: string;

    @Column({ type: 'int', width: 8, default: 0 })
    price_start: number;

    @Column({ type: 'int', width: 8, default: 0 })
    price_finish: number;

    @Column({ type: 'int', width: 6, default: 0 })
    price_average: number;

    @Column({ type: 'text', nullable: false, collation: 'latin1_bin' })
    images: string;

    @Column({ type: 'varchar', length: 255, nullable: false, default: '', collation: 'latin1_bin' })
    auction_list: string;

    @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
    datetime: Date;

    @Column({ type: 'varchar', length: 1500, nullable: false, default: '', collation: 'latin1_bin' })
    info: string;

    @Column({ type: 'varchar', length: 50, nullable: false, default: '', collation: 'latin1_bin' })
    price_average_list: string;

    @Column({ type: 'int', width: 2, default: 0 })
    transmission_type: number;
}
