import {Controller, Get} from '@nestjs/common';
import {ParserService} from "./parser.service";

@Controller('parser')
export class ParserController {

    constructor(private readonly  parserService: ParserService) {
    }

    @Get('/')
    async parser(){
        return await this.parserService.parser()
    }


    @Get('/test')
    async test(){
        return await this.parserService.test()
    }





}
