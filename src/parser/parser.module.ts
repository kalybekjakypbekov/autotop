import { Module } from '@nestjs/common';
import { ParserService } from './parser.service';
import { ParserController } from './parser.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Aj_aucEntity} from "./entity/aj_auc.entity";
import {AutionModule} from "../auction/aution.module";
import {VehicleModule} from "../vehicle/vehicle.module";
import {CarModule} from "../car/car.module";
import {MediaModule} from "../media/media.module";


@Module({
  imports:[TypeOrmModule.forFeature([Aj_aucEntity]), AutionModule, VehicleModule,CarModule,MediaModule],
  providers: [ParserService],
  controllers: [ParserController]
})
export class ParserModule {}
