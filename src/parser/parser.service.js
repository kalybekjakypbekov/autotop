"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.ParserService = void 0;
var common_1 = require("@nestjs/common");
var path_1 = require("path");
var fs_1 = require("fs");
var he = require("he");
var typeorm_1 = require("@nestjs/typeorm");
var aj_auc_entity_1 = require("./entity/aj_auc.entity");
var path = require("path");
var axios = require('axios');
var fs = require('fs');
var ParserService = /** @class */ (function () {
    function ParserService(aj_aucEntity, connection, auctionService, vehicleService, carService, mediaService) {
        this.aj_aucEntity = aj_aucEntity;
        this.connection = connection;
        this.auctionService = auctionService;
        this.vehicleService = vehicleService;
        this.carService = carService;
        this.mediaService = mediaService;
    }
    ParserService.prototype.downloadDump = function () {
        return __awaiter(this, void 0, void 0, function () {
            var url, dumpDirectory, response, originalFileName, filePath_1, writer_1, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        url = 'http://144.76.203.145/dump/pro_PJPfh5jd9sU.sql';
                        dumpDirectory = (0, path_1.join)(__dirname, '../../src/parser/dump');
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, axios.get(url, {
                                responseType: 'stream'
                            })];
                    case 2:
                        response = _a.sent();
                        originalFileName = path.basename(url);
                        filePath_1 = "".concat(dumpDirectory, "/").concat(originalFileName);
                        writer_1 = fs.createWriteStream(filePath_1);
                        response.data.pipe(writer_1);
                        return [2 /*return*/, new Promise(function (resolve, reject) {
                                writer_1.on('finish', function () { return resolve(filePath_1); });
                                writer_1.on('error', reject);
                            })];
                    case 3:
                        error_1 = _a.sent();
                        console.error('An error occurred:', error_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    // @Cron('* 30 * * * *')
    ParserService.prototype.parser = function () {
        return __awaiter(this, void 0, void 0, function () {
            var dumpDirectory, inputFilePath, outPutFilePath, dump, filteredContent, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.downloadDump()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 7, , 8]);
                        dumpDirectory = (0, path_1.join)(__dirname, '../../src/parser/dump');
                        inputFilePath = dumpDirectory + '/pro_PJPfh5jd9sU.sql';
                        outPutFilePath = dumpDirectory + '/outputFilePath.sql';
                        return [4 /*yield*/, fs_1.promises.readFile(inputFilePath, 'utf-8')];
                    case 3:
                        dump = _a.sent();
                        filteredContent = dump.replace(/CREATE TABLE[^;]*;/gi, '').replace(/DROP TABLE IF EXISTS[^;]*;/gi, '');
                        // Записываем отфильтрованное содержимое в выходной файл асинхронно
                        return [4 /*yield*/, fs_1.promises.writeFile(outPutFilePath, filteredContent)];
                    case 4:
                        // Записываем отфильтрованное содержимое в выходной файл асинхронно
                        _a.sent();
                        console.log('Создан отфильтрованный файл дампа.');
                        return [4 /*yield*/, this.importDump()];
                    case 5:
                        _a.sent();
                        return [4 /*yield*/, this.dumpTransferDb()];
                    case 6:
                        _a.sent();
                        return [2 /*return*/, 0];
                    case 7:
                        err_1 = _a.sent();
                        console.error('Ошибка:', err_1);
                        throw new common_1.HttpException('Это сообщение об ошибке', common_1.HttpStatus.BAD_REQUEST);
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    ParserService.prototype.importDump = function () {
        return __awaiter(this, void 0, void 0, function () {
            var dumpDirectory, outPutFilePath, dumpContent, decodedContent, queries, _i, queries_1, query, trimmedQuery, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.connection.query("DELETE from aj_auc;")];
                    case 1:
                        _a.sent();
                        console.log("3. importDump");
                        dumpDirectory = (0, path_1.join)(__dirname, '../../src/parser/dump');
                        outPutFilePath = dumpDirectory + '/outputFilePath.sql';
                        return [4 /*yield*/, fs_1.promises.readFile(outPutFilePath, 'utf-8')];
                    case 2:
                        dumpContent = _a.sent();
                        decodedContent = he.decode(dumpContent);
                        queries = decodedContent.split(';');
                        _i = 0, queries_1 = queries;
                        _a.label = 3;
                    case 3:
                        if (!(_i < queries_1.length)) return [3 /*break*/, 8];
                        query = queries_1[_i];
                        trimmedQuery = query.trim();
                        if (!trimmedQuery) return [3 /*break*/, 7];
                        _a.label = 4;
                    case 4:
                        _a.trys.push([4, 6, , 7]);
                        return [4 /*yield*/, this.connection.query(trimmedQuery)];
                    case 5:
                        _a.sent();
                        return [3 /*break*/, 7];
                    case 6:
                        e_1 = _a.sent();
                        if (e_1.code == "ER_DUP_ENTRY") {
                            console.log(e_1);
                        }
                        return [3 /*break*/, 7];
                    case 7:
                        _i++;
                        return [3 /*break*/, 3];
                    case 8: return [2 /*return*/, true];
                }
            });
        });
    };
    ParserService.prototype.dumpTransferDb = function () {
        return __awaiter(this, void 0, void 0, function () {
            var auction_list, _i, auction_list_1, auction, checkAuction, makes, _a, makes_1, make, checkMake, models, _b, models_1, model, modelCheck, createModel, vehicles, count, _c, vehicles_1, vehicle, checkVehicle, inputString, regex, matches, rateExt, rateInt, vehicleData, makeId, modelId, auction, lotData, findLot;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        console.log("4. dumpTransferDb");
                        return [4 /*yield*/, this.connection.query("select auct_name from aj_auc group by auct_name;")];
                    case 1:
                        auction_list = _d.sent();
                        _i = 0, auction_list_1 = auction_list;
                        _d.label = 2;
                    case 2:
                        if (!(_i < auction_list_1.length)) return [3 /*break*/, 6];
                        auction = auction_list_1[_i];
                        return [4 /*yield*/, this.auctionService.findAuction(auction.auct_name)];
                    case 3:
                        checkAuction = _d.sent();
                        if (!!checkAuction) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.auctionService.createAuction(auction.auct_name)];
                    case 4:
                        _d.sent();
                        _d.label = 5;
                    case 5:
                        _i++;
                        return [3 /*break*/, 2];
                    case 6: return [4 /*yield*/, this.connection.query("select manuf_name, manuf_id from aj_auc group by manuf_name,manuf_id;")];
                    case 7:
                        makes = _d.sent();
                        _a = 0, makes_1 = makes;
                        _d.label = 8;
                    case 8:
                        if (!(_a < makes_1.length)) return [3 /*break*/, 18];
                        make = makes_1[_a];
                        return [4 /*yield*/, this.carService.getByMakeName(make.manuf_name)];
                    case 9:
                        checkMake = _d.sent();
                        if (!!checkMake) return [3 /*break*/, 11];
                        return [4 /*yield*/, this.carService.createMake(make.manuf_name)];
                    case 10:
                        checkMake = _d.sent();
                        _d.label = 11;
                    case 11: return [4 /*yield*/, this.connection.query("select manuf_name ,model_name,model_id from aj_auc WHERE manuf_name = '".concat(checkMake.name, "' GROUP by model_name,model_id,manuf_name;"))];
                    case 12:
                        models = _d.sent();
                        _b = 0, models_1 = models;
                        _d.label = 13;
                    case 13:
                        if (!(_b < models_1.length)) return [3 /*break*/, 17];
                        model = models_1[_b];
                        return [4 /*yield*/, this.carService.getByModelName(model.model_name, checkMake.id)];
                    case 14:
                        modelCheck = _d.sent();
                        if (!!modelCheck) return [3 /*break*/, 16];
                        return [4 /*yield*/, this.carService.createModel(model.model_name, checkMake.id)];
                    case 15:
                        createModel = _d.sent();
                        _d.label = 16;
                    case 16:
                        _b++;
                        return [3 /*break*/, 13];
                    case 17:
                        _a++;
                        return [3 /*break*/, 8];
                    case 18: return [4 /*yield*/, this.aj_aucEntity.find()];
                    case 19:
                        vehicles = _d.sent();
                        count = 1;
                        _c = 0, vehicles_1 = vehicles;
                        _d.label = 20;
                    case 20:
                        if (!(_c < vehicles_1.length)) return [3 /*break*/, 29];
                        vehicle = vehicles_1[_c];
                        return [4 /*yield*/, this.vehicleService.getByAjId(vehicle.id)];
                    case 21:
                        checkVehicle = _d.sent();
                        inputString = vehicle.info;
                        regex = /Rate ext: (\w), Rate int: (\w)/;
                        matches = inputString.match(regex);
                        rateExt = void 0;
                        rateInt = void 0;
                        if (matches) {
                            rateExt = matches[1]; // Буква A
                            rateInt = matches[2]; // Буква C
                        }
                        else {
                            rateExt = "-";
                            rateInt = "-";
                        }
                        vehicleData = {
                            aj_id: vehicle.id,
                            year: new Date(vehicle.year.toString()),
                            chassis: vehicle.body,
                            grade: vehicle.grade,
                            color: vehicle.colour,
                            mileage: vehicle.mileage,
                            displace: vehicle.Vcc,
                            trans: vehicle.transmission,
                            equipment: vehicle.equipment,
                            condition_ext: rateExt,
                            condition_int: rateInt
                        };
                        if (!!checkVehicle) return [3 /*break*/, 25];
                        return [4 /*yield*/, this.carService.getByMakeName(vehicle.manuf_name)];
                    case 22:
                        makeId = _d.sent();
                        return [4 /*yield*/, this.carService.getByModelName(vehicle.model_name, makeId.id)];
                    case 23:
                        modelId = _d.sent();
                        vehicleData.makeId = makeId.id;
                        vehicleData.modelId = modelId.id;
                        return [4 /*yield*/, this.vehicleService.save(vehicleData)];
                    case 24:
                        checkVehicle = _d.sent();
                        return [3 /*break*/, 25];
                    case 25: return [4 /*yield*/, this.auctionService.findAuction(vehicle.auct_name)];
                    case 26:
                        auction = _d.sent();
                        lotData = {
                            aj_id: vehicle.id,
                            start_time: vehicle.auct_date,
                            end_time: vehicle.datetime,
                            lot_no: vehicle.lot_num,
                            start_price: vehicle.price_start,
                            finish_price: vehicle.price_finish,
                            price_average: vehicle.price_average,
                            average_price_list: vehicle.price_average_list,
                            rate: vehicle.rate,
                            vehicleId: checkVehicle.id,
                            auctionId: auction.id,
                            auction_list: vehicle.auction_list,
                            images: vehicle.images
                        };
                        return [4 /*yield*/, this.auctionService.findLotAjId(vehicle.id, checkVehicle.id, vehicle.auct_date)];
                    case 27:
                        findLot = _d.sent();
                        if (findLot) {
                            this.auctionService.updateLot(vehicle.id, lotData);
                        }
                        else {
                            this.auctionService.createLot(lotData);
                        }
                        console.log(count + "/" + vehicles.length);
                        count++;
                        _d.label = 28;
                    case 28:
                        _c++;
                        return [3 /*break*/, 20];
                    case 29: 
                    //   await this.connection.query("DELETE from aj_auc;")
                    return [2 /*return*/, true];
                }
            });
        });
    };
    ParserService.prototype.test = function () {
        return __awaiter(this, void 0, void 0, function () {
            var vehicles, count, _i, vehicles_2, vehicle, checkVehicle, inputString, regex, matches, rateExt, rateInt, vehicleData, makeId, modelId, auction, lotData, findLot;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.aj_aucEntity.find()];
                    case 1:
                        vehicles = _a.sent();
                        count = 1;
                        _i = 0, vehicles_2 = vehicles;
                        _a.label = 2;
                    case 2:
                        if (!(_i < vehicles_2.length)) return [3 /*break*/, 15];
                        vehicle = vehicles_2[_i];
                        console.time('Execution Time');
                        return [4 /*yield*/, this.vehicleService.getByAjId(vehicle.id)];
                    case 3:
                        checkVehicle = _a.sent();
                        inputString = vehicle.info;
                        regex = /Rate ext: (\w), Rate int: (\w)/;
                        matches = inputString.match(regex);
                        rateExt = void 0;
                        rateInt = void 0;
                        if (matches) {
                            rateExt = matches[1]; // Буква A
                            rateInt = matches[2]; // Буква C
                        }
                        else {
                            rateExt = "-";
                            rateInt = "-";
                        }
                        vehicleData = {
                            aj_id: vehicle.id,
                            year: new Date(vehicle.year.toString()),
                            chassis: vehicle.body,
                            grade: vehicle.grade,
                            color: vehicle.colour,
                            mileage: vehicle.mileage,
                            displace: vehicle.Vcc,
                            trans: vehicle.transmission,
                            equipment: vehicle.equipment,
                            condition_ext: rateExt,
                            condition_int: rateInt
                        };
                        if (!!checkVehicle) return [3 /*break*/, 7];
                        return [4 /*yield*/, this.carService.getByMakeName(vehicle.manuf_name)];
                    case 4:
                        makeId = _a.sent();
                        return [4 /*yield*/, this.carService.getByModelName(vehicle.model_name, makeId.id)];
                    case 5:
                        modelId = _a.sent();
                        vehicleData.makeId = makeId.id;
                        vehicleData.modelId = modelId.id;
                        return [4 /*yield*/, this.vehicleService.save(vehicleData)];
                    case 6:
                        checkVehicle = _a.sent();
                        return [3 /*break*/, 7];
                    case 7: return [4 /*yield*/, this.auctionService.findAuction(vehicle.auct_name)];
                    case 8:
                        auction = _a.sent();
                        lotData = {
                            aj_id: vehicle.id,
                            start_time: vehicle.auct_date,
                            end_time: vehicle.datetime,
                            lot_no: vehicle.lot_num,
                            start_price: vehicle.price_start,
                            finish_price: vehicle.price_finish,
                            price_average: vehicle.price_average,
                            average_price_list: vehicle.price_average_list,
                            rate: vehicle.rate,
                            vehicleId: checkVehicle.id,
                            auctionId: auction.id,
                            auction_list: vehicle.auction_list,
                            images: vehicle.images
                        };
                        return [4 /*yield*/, this.auctionService.findLotAjId(vehicle.id, checkVehicle.id, vehicle.auct_date)];
                    case 9:
                        findLot = _a.sent();
                        if (!findLot) return [3 /*break*/, 11];
                        return [4 /*yield*/, this.auctionService.updateLot(vehicle.id, lotData)];
                    case 10:
                        _a.sent();
                        return [3 /*break*/, 13];
                    case 11: return [4 /*yield*/, this.auctionService.createLot(lotData)];
                    case 12:
                        _a.sent();
                        _a.label = 13;
                    case 13:
                        console.log(count + "/" + vehicles.length);
                        count++;
                        console.timeEnd('Execution Time');
                        _a.label = 14;
                    case 14:
                        _i++;
                        return [3 /*break*/, 2];
                    case 15: return [2 /*return*/];
                }
            });
        });
    };
    ParserService = __decorate([
        (0, common_1.Injectable)(),
        __param(0, (0, typeorm_1.InjectRepository)(aj_auc_entity_1.Aj_aucEntity))
    ], ParserService);
    return ParserService;
}());
exports.ParserService = ParserService;
