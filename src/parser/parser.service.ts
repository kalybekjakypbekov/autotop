import {HttpException, HttpService, HttpStatus, Injectable} from '@nestjs/common';
import { join } from 'path';
import { promises as fsPromises } from 'fs';
import {Connection, Repository} from "typeorm";
import * as he from 'he';
import {InjectRepository} from "@nestjs/typeorm";
import {Aj_aucEntity} from "./entity/aj_auc.entity";
import {AutionService} from "../auction/aution.service";
import {VehicleService} from "../vehicle/vehicle.service";
import {CarService} from "../car/car.service";
import {CreateVehicleDto} from "../vehicle/dto/create-vehicle.dto";
import {MediaService} from "../media/media.service";
import * as path from "path";
import { Cron } from "@nestjs/schedule";
const axios = require('axios');
const fs = require('fs');

@Injectable()
export class ParserService {
    constructor(
        @InjectRepository(Aj_aucEntity) private  readonly aj_aucEntity: Repository<Aj_aucEntity>,
        private readonly connection: Connection,
        private readonly auctionService: AutionService,
        private  readonly vehicleService: VehicleService,
        private  readonly carService: CarService,
        private  readonly mediaService: MediaService,

        ) {
    }


    async downloadDump(){
        const url = 'http://144.76.203.145/dump/pro_PJPfh5jd9sU.sql';
        const dumpDirectory = join(__dirname, '../../src/parser/dump');

        try {
            const response = await axios.get(url, {
                responseType: 'stream', // Получаем потоковые данные
            });

            const originalFileName = path.basename(url);  // Извлекаем оригинальное имя файла из заголовка 'content-disposition'

            const filePath = `${dumpDirectory}/${originalFileName}`; // Путь для сохранения файла

            const writer = fs.createWriteStream(filePath);
            response.data.pipe(writer);

            return new Promise((resolve, reject) => {
                writer.on('finish', () => resolve(filePath));
                writer.on('error', reject);
            });
        } catch (error) {
            console.error('An error occurred:', error);
        }
    }

    // @Cron('* 30 * * * *')
    async parser() {
             await this.downloadDump();
        try {
            const dumpDirectory = join(__dirname, '../../src/parser/dump');
            const inputFilePath = dumpDirectory + '/pro_PJPfh5jd9sU.sql';
            const outPutFilePath = dumpDirectory + '/outputFilePath.sql';

            // Считываем входной файл асинхронно
            const dump = await fsPromises.readFile(inputFilePath, 'utf-8');

            // Используем регулярные выражения для удаления операторов CREATE TABLE и DROP TABLE
            const filteredContent = dump.replace(/CREATE TABLE[^;]*;/gi, '').replace(/DROP TABLE IF EXISTS[^;]*;/gi, '');

            // Записываем отфильтрованное содержимое в выходной файл асинхронно
            await fsPromises.writeFile(outPutFilePath, filteredContent);

            console.log('Создан отфильтрованный файл дампа.');
               await this.importDump()
             await this.dumpTransferDb()
            return 0;
        } catch (err) {
            console.error('Ошибка:', err);
            throw new HttpException('Это сообщение об ошибке', HttpStatus.BAD_REQUEST);
        }
    }


    async importDump (){
        await this.connection.query("DELETE from aj_auc;")
        console.log("3. importDump")
        const dumpDirectory = join(__dirname, '../../src/parser/dump');
        const outPutFilePath = dumpDirectory + '/outputFilePath.sql';
        const dumpContent = await fsPromises.readFile(outPutFilePath, 'utf-8');
        const decodedContent = he.decode(dumpContent);
        let queries = decodedContent.split(';')

        for(let query of queries){
            const trimmedQuery = query.trim();
            if (trimmedQuery) {
                try {
                    await this.connection.query(trimmedQuery);
                   // console.log('Запрос успешно выполнен:', trimmedQuery);
                }catch (e){
                    if(e.code == "ER_DUP_ENTRY"){
                        console.log(e)
                    }
                }

            }
        }
        return true;
    }

    async dumpTransferDb (){
        console.log("4. dumpTransferDb")

        const auction_list = await this.connection.query("select auct_name from aj_auc group by auct_name;")

        for(let auction of auction_list){
            const checkAuction = await  this.auctionService.findAuction(auction.auct_name)
            if(!checkAuction){
                await this.auctionService.createAuction(auction.auct_name)
            }
        }

        const makes = await this.connection.query("select manuf_name, manuf_id from aj_auc group by manuf_name,manuf_id;")

        for (let make of makes){

            let checkMake = await this.carService.getByMakeName(make.manuf_name)
            if(!checkMake) {
                checkMake = await this.carService.createMake(make.manuf_name)
            }

            const models = await this.connection.query(`select manuf_name ,model_name,model_id from aj_auc WHERE manuf_name = '${checkMake.name}' GROUP by model_name,model_id,manuf_name;`)

            for(let model of models) {
                const modelCheck = await this.carService.getByModelName(model.model_name, checkMake.id)

                if(!modelCheck){
                    const createModel = await  this.carService.createModel(model.model_name ,checkMake.id)
                }

            }
        }



        const vehicles = await this.aj_aucEntity.find()
        let count = 1;

        for (let vehicle of vehicles ){

            let  checkVehicle = await this.vehicleService.getByAjId(vehicle.id)

            const inputString = vehicle.info;
            const regex = /Rate ext: (\w), Rate int: (\w)/;
            const matches = inputString.match(regex);
            let rateExt;
            let rateInt;
            if (matches) {
                 rateExt = matches[1]; // Буква A
                 rateInt = matches[2]; // Буква C
            } else {
                rateExt = "-"
                rateInt = "-"
            }

            let vehicleData: CreateVehicleDto = {
                aj_id: vehicle.id,
                year: new Date(vehicle.year.toString()),
                chassis: vehicle.body,
                grade: vehicle.grade,
                color: vehicle.colour,
                mileage: vehicle.mileage,
                displace: vehicle.Vcc,
                trans: vehicle.transmission,
                equipment: vehicle.equipment,
                condition_ext: rateExt,
                condition_int: rateInt,
            }

            if (!checkVehicle) {
                const makeId = await this.carService.getByMakeName(vehicle.manuf_name)
                const modelId  = await this.carService.getByModelName(vehicle.model_name, makeId.id)
                vehicleData.makeId = makeId.id
                vehicleData.modelId = modelId.id
                checkVehicle = await this.vehicleService.save(vehicleData);
            } else {
               // await this.vehicleService.updateVehicle(checkVehicle.aj_id, vehicleData);
            }

            let auction = await this.auctionService.findAuction(vehicle.auct_name)

            let lotData = {
                aj_id: vehicle.id,
                start_time: vehicle.auct_date,
                end_time: vehicle.datetime,
                lot_no: vehicle.lot_num,
                start_price: vehicle.price_start,
                finish_price: vehicle.price_finish,
                price_average: vehicle.price_average,
                average_price_list: vehicle.price_average_list,
                rate: vehicle.rate,
                vehicleId: checkVehicle.id,
                auctionId:auction.id,
                auction_list: vehicle.auction_list,
                images: vehicle.images
            }

            let findLot = await this.auctionService.findLotAjId(vehicle.id, checkVehicle.id,vehicle.auct_date)

            if(findLot) {
                  this.auctionService.updateLot(vehicle.id, lotData )
            }else {
                 // this.auctionService.createLot(lotData);
            }

            console.log(count+"/"+vehicles.length)
            count++;
        }


     //   await this.connection.query("DELETE from aj_auc;")

        return true
    }


    async  test(){

        const vehicles = await this.aj_aucEntity.find()
        let count = 1;

        for (let vehicle of vehicles ){
            console.time('Execution Time');
            let  checkVehicle = await this.vehicleService.getByAjId(vehicle.id)
            const inputString = vehicle.info;
            const regex = /Rate ext: (\w), Rate int: (\w)/;
            const matches = inputString.match(regex);
            let rateExt;
            let rateInt;
            if (matches) {
                rateExt = matches[1]; // Буква A
                rateInt = matches[2]; // Буква C
            } else {
                rateExt = "-"
                rateInt = "-"
            }

            let vehicleData: CreateVehicleDto = {
                aj_id: vehicle.id,
                year: new Date(vehicle.year.toString()),
                chassis: vehicle.body,
                grade: vehicle.grade,
                color: vehicle.colour,
                mileage: vehicle.mileage,
                displace: vehicle.Vcc,
                trans: vehicle.transmission,
                equipment: vehicle.equipment,
                condition_ext: rateExt,
                condition_int: rateInt,
            }

            if (!checkVehicle) {
                const makeId = await this.carService.getByMakeName(vehicle.manuf_name)
                const modelId  = await this.carService.getByModelName(vehicle.model_name, makeId.id)
                vehicleData.makeId = makeId.id
                vehicleData.modelId = modelId.id
                checkVehicle = await this.vehicleService.save(vehicleData);
            } else {
                // await this.vehicleService.updateVehicle(checkVehicle.aj_id, vehicleData);
            }

            let auction = await this.auctionService.findAuction(vehicle.auct_name)

            let lotData = {
                aj_id: vehicle.id,
                start_time: vehicle.auct_date,
                end_time: vehicle.datetime,
                lot_no: vehicle.lot_num,
                start_price: vehicle.price_start,
                finish_price: vehicle.price_finish,
                price_average: vehicle.price_average,
                average_price_list: vehicle.price_average_list,
                rate: vehicle.rate,
                vehicleId: checkVehicle.id,
                auctionId:auction.id,
                auction_list: vehicle.auction_list,
                images: vehicle.images
            }

            let findLot = await this.auctionService.findLotAjId(vehicle.id, checkVehicle.id,vehicle.auct_date)

            if(findLot) {
                 await this.auctionService.updateLot(vehicle.id, lotData )
            }else {
                // await this.auctionService.createLot(lotData);
            }

            console.log(count+"/"+vehicles.length)
            count++;
            console.timeEnd('Execution Time');
        }



    }


}
