import {BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {PurchasesEntity} from "../../purchases/entity/purchases.entity";
import { OrderEntity } from "../../order/entity/order.entity";

@Entity({name:"consignee"})
export  class ConsigneeEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column({nullable:true})
    consignee: string

    @Column({nullable:true})
    address: string

    @Column({nullable:true})
    telephone: string

    @Column({nullable:true})
    notify: string

    @Column({nullable:true})
    address_2: string

    @Column({nullable:true})
    purchase_id: number

    @OneToOne(()=>PurchasesEntity, (purchase) => purchase.consignee)
    @JoinColumn({name:"purchase_id",referencedColumnName:"id"})
    purchase: PurchasesEntity

    @OneToOne(()=>OrderEntity, (order)=> order.consignee)
    order: OrderEntity

}