import { Module } from '@nestjs/common';
import { ConsigneeService } from './consignee.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {ConsigneeEntity} from "./entity/consignee.entity";
import { ConsigneeController } from "./consignee.controller";

@Module({
  controllers:[ConsigneeController],
  imports: [TypeOrmModule.forFeature([ConsigneeEntity])],
  providers: [ConsigneeService]
})
export class ConsigneeModule {}
