import { MigrationInterface, QueryRunner } from "typeorm";

export class AddFieldsActiveNatif1727649064447 implements MigrationInterface {
    name = 'AddFieldsActiveNatif1727649064447'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`notification\` ADD \`active\` varchar(255) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`notification\` DROP COLUMN \`active\``);
    }

}
