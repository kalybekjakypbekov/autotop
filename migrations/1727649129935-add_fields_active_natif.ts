import { MigrationInterface, QueryRunner } from "typeorm";

export class AddFieldsActiveNatif1727649129935 implements MigrationInterface {
    name = 'AddFieldsActiveNatif1727649129935'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`notification\` CHANGE \`active\` \`active\` varchar(255) NOT NULL DEFAULT 'Active'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`notification\` CHANGE \`active\` \`active\` varchar(255) NOT NULL`);
    }

}
