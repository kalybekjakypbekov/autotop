import { MigrationInterface, QueryRunner } from "typeorm";

export class RemoveFieldsActiveNatif1727649159067 implements MigrationInterface {
    name = 'RemoveFieldsActiveNatif1727649159067'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`notification\` DROP COLUMN \`active\``);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`notification\` ADD \`active\` varchar(255) NOT NULL DEFAULT 'Active'`);
    }

}
