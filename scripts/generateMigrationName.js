const fs = require('fs');

// Генерация названия миграции с текущей датой и временем
let date = new Date();
const migrationName = `auto-migration-${date.getFullYear()}${(date.getMonth() + 1).toString().padStart(2, '0')}${date.getDate().toString().padStart(2, '0')}${date.getHours().toString().padStart(2, '0')}${date.getMinutes().toString().padStart(2, '0')}${date.getSeconds().toString().padStart(2, '0')}`;

// Запись названия в файл
fs.writeFileSync('migrations-name.txt', migrationName, 'utf8');

console.log(`Migration name generated: ${migrationName}`);
