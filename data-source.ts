import { DataSource } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { ConfigModule } from '@nestjs/config';
import { join } from 'path';

// Инициализируем ConfigModule для использования process.env
ConfigModule.forRoot();

const configService = new ConfigService();

export const AppDataSource = new DataSource({
  type: 'mysql',
  host: configService.get<string>('DB_HOST'),
  port: configService.get<number>('DB_PORT'),
  username: configService.get<string>('DB_USERNAME'),
  password: configService.get<string>('DB_PASSWORD'),
  database: configService.get<string>('DB_DATABASE'),
  entities: [join(__dirname, './src/**/*.entity{.ts,.js}')], // Путь к сущностям
  migrations: [join(__dirname, '/migrations/*.ts')], // Путь к миграциям
  synchronize: false,
});
