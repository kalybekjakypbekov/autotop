import { ConfigService } from '@nestjs/config';
import { DataSource } from 'typeorm';
import { ConfigModule } from '@nestjs/config';

// Инициализируем ConfigModule для использования process.env
ConfigModule.forRoot();

const configService = new ConfigService();

export const dataSource = new DataSource({
  type: 'mysql',
  host: configService.get<string>('DB_HOST'),
  port: configService.get<number>('DB_PORT'),
  username: configService.get<string>('DB_USERNAME'),
  password: configService.get<string>('DB_PASSWORD'),
  database: configService.get<string>('DB_DATABASE'),
  entities: [__dirname + '/../**/*.entity{.ts,.js}'],
  migrations: [__dirname + './migrations/*.ts'],
  synchronize: false,
});
